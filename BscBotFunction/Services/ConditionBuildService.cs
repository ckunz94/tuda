﻿using System.Collections.Generic;
using System.Text;
using BscBotFunction.Models;

namespace BscBotFunction.Services
{
    public class ConditionBuildService
    {
        public ConditionBuildService()
        {

        }


        public ParameterModel makeParameterModel(string key, string value, string syntax)
        {
            ParameterModel model = new ParameterModel();
            model.key = key;
            model.value = value;
            model.syntax = syntax;
            return model;
        }

        public string buildCondition(List<ParameterModel> parameters)
        {
            StringBuilder sb = new StringBuilder();

            foreach (ParameterModel parameter in parameters)
            {

                if (parameter.value != "" && parameter.value != null && parameter.value != " ")
                {
                    if (parameter.syntax == "<" || parameter.syntax == ">" || parameter.syntax == "<=" || parameter.syntax == ">=")
                        sb.Append(parameter.key + " " + parameter.syntax + " " + parameter.value + " AND ");
                    else
                        sb.Append(parameter.key + " " + parameter.syntax + " '" + parameter.value + "' AND ");
                }


            }
            sb.Append("1 = 1");
            return sb.ToString();
        }
    }
}
