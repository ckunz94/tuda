﻿using System.Collections.Generic;
using System.Text;
using BscBotFunction.Models;
using BscBotFunction.Models.DialogFlow;
using Google.Cloud.Dialogflow.V2;

namespace BscBotFunction.Services
{
    public class DialogflowService
    {
        public DialogflowService()
        {
        }

        public string listToString(string[] input)
        {
            StringBuilder sb = new StringBuilder();

            int dbCount = input.Length;
            if (dbCount == 1)
                return input[0];


            foreach (string element in input)
            {

                if (dbCount > 2)
                    sb.Append(element + ", ");
                else if (dbCount > 1)
                    sb.Append(element);
                else
                    sb.Append(" und " + element);
                dbCount -= 1;
            }
            return sb.ToString();
        }
        public ParameterModel makeParameter(string key, string value)
        {
            ParameterModel PM = new ParameterModel();
            PM.key = key;
            PM.value = value;
            return PM;

        }
        public string parameterListToString(List<ParameterModel> inputList)
        {
            StringBuilder sb = new StringBuilder();
            List<ParameterModel> workList = inputList;

            for (int i = inputList.Count - 1; i >= 0; i--)
            {
                if (workList[i].value == "")
                    workList.RemoveAt(i);
            }



            ParameterModel[] input = inputList.ToArray();
            int dbCount = input.Length;
            if (dbCount == 1)
                return input[0].key + " " + input[0].value;

            foreach (ParameterModel element in input)
            {

                if (dbCount > 2)
                    sb.Append(element.key + " " + element.value + ", ");
                else if (dbCount > 1)
                    sb.Append(element.key + " " + element.value);
                else
                    sb.Append(" und " + element.key + " " + element.value);

                dbCount -= 1;
            }
            return sb.ToString();
        }
        public parameters outputContextToParameters(WebhookRequest request)
        {
            parameters parms = new parameters();

            foreach (Context element in request.QueryResult.OutputContexts)
            {


                if (element.Parameters.Fields.GetValueOrDefault("email") != null)
                    parms.email = element.Parameters.Fields.GetValueOrDefault("email").StringValue;
                if (element.Parameters.Fields.GetValueOrDefault("address") != null)
                    parms.address = element.Parameters.Fields.GetValueOrDefault("address").StringValue;
                if (element.Parameters.Fields.GetValueOrDefault("AskiPhone") != null)
                    parms.AskiPhone = element.Parameters.Fields.GetValueOrDefault("AskiPhone").StringValue;
                if (element.Parameters.Fields.GetValueOrDefault("AskiPhoneComponent") != null)
                    parms.AskiPhoneComponent = element.Parameters.Fields.GetValueOrDefault("AskiPhoneComponent").StringValue;
                if (element.Parameters.Fields.GetValueOrDefault("DeviceCapacity") != null)
                    parms.DeviceCapacity = element.Parameters.Fields.GetValueOrDefault("DeviceCapacity").StringValue;
                if (element.Parameters.Fields.GetValueOrDefault("DeviceColor") != null)
                    parms.DeviceColor = element.Parameters.Fields.GetValueOrDefault("DeviceColor").StringValue;
                if (element.Parameters.Fields.GetValueOrDefault("DeviceDisplaySize") != null)
                    parms.DeviceDisplaySize = element.Parameters.Fields.GetValueOrDefault("DeviceDisplaySize").StringValue;
                if (element.Parameters.Fields.GetValueOrDefault("DeviceName") != null)
                    parms.DeviceName = element.Parameters.Fields.GetValueOrDefault("DeviceName").StringValue;

                if (element.Parameters.Fields.GetValueOrDefault("DeviceProcessorName") != null)
                    parms.DeviceProcessorName = element.Parameters.Fields.GetValueOrDefault("DeviceProcessorName").StringValue;
                if (element.Parameters.Fields.GetValueOrDefault("DeviceProcessorGHZ") != null)
                    parms.DeviceProcessorGHz = element.Parameters.Fields.GetValueOrDefault("DeviceProcessorGHZ").StringValue;
                if (element.Parameters.Fields.GetValueOrDefault("DeviceProcessorCore") != null)
                    parms.DeviceProcessorCore = element.Parameters.Fields.GetValueOrDefault("DeviceProcessorCore").StringValue;
                if (element.Parameters.Fields.GetValueOrDefault("DeviceMemory") != null)
                    parms.DeviceMemory = element.Parameters.Fields.GetValueOrDefault("DeviceMemory").StringValue;
                if (element.Parameters.Fields.GetValueOrDefault("DeviceGraphicCard") != null)
                    parms.DeviceGraphik = element.Parameters.Fields.GetValueOrDefault("DeviceGraphicCard").StringValue;


                if (element.Parameters.Fields.GetValueOrDefault("DeviceType") != null)
                    parms.DeviceType = element.Parameters.Fields.GetValueOrDefault("DeviceType").StringValue;
                if (element.Parameters.Fields.GetValueOrDefault("geoCity") != null)
                    parms.geoCity = element.Parameters.Fields.GetValueOrDefault("geoCity").StringValue;
                if (element.Parameters.Fields.GetValueOrDefault("givenName") != null)
                    parms.givenName = element.Parameters.Fields.GetValueOrDefault("givenName").StringValue;
                if (element.Parameters.Fields.GetValueOrDefault("lastName") != null)
                    parms.lastName = element.Parameters.Fields.GetValueOrDefault("lastName").StringValue;
                if (element.Parameters.Fields.GetValueOrDefault("zipCode") != null)
                    parms.zipCode = element.Parameters.Fields.GetValueOrDefault("zipCode").StringValue;

                if (element.Parameters.Fields.GetValueOrDefault("AddonSell") != null)
                {
                    List<string> addonList = new List<string>();
                    for (int i = 0; i < element.Parameters.Fields.GetValueOrDefault("AddonSell").ListValue.Values.Count; i++)
                    {
                        addonList.Add(element.Parameters.Fields.GetValueOrDefault("AddonSell").ListValue.Values[i].StringValue);
                    }
                    List<addonSell> addonSellList = new List<addonSell>();
                    foreach (string shortname in addonList)
                    {
                        addonSell addon = new addonSell();
                        addon.shortName = shortname;
                        addonSellList.Add(addon);
                    }
                    parms.AddonSells = addonSellList.ToArray();
                }

                if (element.Parameters.Fields.GetValueOrDefault("UpSell") != null)
                    parms.UpSell = element.Parameters.Fields.GetValueOrDefault("UpSell").StringValue;
                if (element.Parameters.Fields.GetValueOrDefault("DeviceConnectivity") != null)
                    parms.DeviceConnectivity = element.Parameters.Fields.GetValueOrDefault("DeviceConnectivity").StringValue;

            }

            return parms;
        }
        public Google.Protobuf.Collections.RepeatedField<Context> addToContext(string parameter, string value, WebhookRequest request)
        {
            Google.Protobuf.Collections.RepeatedField<Context> contexts = new Google.Protobuf.Collections.RepeatedField<Context>();

            Google.Protobuf.WellKnownTypes.Value value1 = new Google.Protobuf.WellKnownTypes.Value();
            value1.StringValue = value;
            Google.Protobuf.Collections.MapField<string, Google.Protobuf.WellKnownTypes.Value> map = new Google.Protobuf.Collections.MapField<string, Google.Protobuf.WellKnownTypes.Value>();
            map.Add(parameter, value1);

            string sessionID = request.Session;
            string[] sessionIDsplit = sessionID.Split('/');
            sessionID = sessionIDsplit[4];

            Google.Protobuf.WellKnownTypes.Struct parms2 = new Google.Protobuf.WellKnownTypes.Struct();
            parms2.Fields.Add(map);

            string contextname = "";
            string contextname4 = "";

            if (request.QueryResult.Intent.DisplayName == "buy.Mac")
            {
                contextname = "buy_mac_dialog_context";
                contextname4 = "59d492fd-054a-4fea-ad9a-c5349791c3df_id_dialog_context";
            }
            else if (request.QueryResult.Intent.DisplayName == "buy.iPad")
            {
                contextname = "buy_ipad_dialog_context";
                contextname4 = "8e9abc2b-b1a6-40eb-bd30-c510853f411e_id_dialog_context";
            }
            else if (request.QueryResult.Intent.DisplayName == "Consult")
            {
                contextname = "consult_dialog_context";
                contextname4 = "e5895eb3-a4fc-4f3d-b70b-bba5447136b7_id_dialog_context";
            }
            else if (request.QueryResult.Intent.DisplayName == "buy.iPad - yes")
            {
                string contextname1 = "";
                contextname1 = "buyipad-followup";
                Context context1 = new Context();
                context1.LifespanCount = 5;
                context1.Name = request.Session + "/contexts/" + contextname1;
                context1.Parameters = parms2;
                contexts.Add(context1);


                contextname = "buy_ipad_-_yes_dialog_context";
                contextname4 = "78ddda15-120b-49d1-b40f-21867b64c5f3_id_dialog_context";
            }


            Context context = new Context();
            context.LifespanCount = 5;
            context.Name = request.Session + "/contexts/" + contextname;
            context.Parameters = parms2;

            Context context4 = new Context();
            context4.LifespanCount = 5;
            context4.Name = request.Session + "/contexts/" + contextname4;
            context4.Parameters = parms2;

            contexts.Add(context);
            contexts.Add(context4);

            return contexts;
        }

    }
}
