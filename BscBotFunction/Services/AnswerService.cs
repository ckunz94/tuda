﻿using BscBotFunction.Services;
using Google.Cloud.Dialogflow.V2;

namespace BscBotFunction.Services
{
    public class AnswerService
    {
        public AnswerService()
        {

        }

        public async System.Threading.Tasks.Task<WebhookResponse> responseAsync(WebhookRequest request)
        {
            WebhookResponse response = new WebhookResponse();
            iPhoneService iPhone = new iPhoneService();
            iPadService iPad = new iPadService();
            MacService mac = new MacService();
            ConsulService consult = new ConsulService();

            if (request.QueryResult.Intent.DisplayName == "buy.iphone")
                return (iPhone.buyiphone(request));
            else if (request.QueryResult.Intent.DisplayName == "ask.iphone")
                return (iPhone.askiphone(request));
            else if (request.QueryResult.Intent.DisplayName == "buy.iphone - yes - nameAddress")
                return (await iPhone.buyiphone_finishAsync(request));
            else if (request.QueryResult.Intent.DisplayName == "buy.Mac")
                return (mac.buyMacbook(request));
            else if (request.QueryResult.Intent.DisplayName == "buy.Mac - yes")
                return (mac.buyMacbookAddon(request));
            else if (request.QueryResult.Intent.DisplayName == "buy.Mac - yes - nameAddress")
                return await mac.buyMacbook_finishAsync(request);
            else if (request.QueryResult.Intent.DisplayName == "buy.iPad")
                return (iPad.buyipad(request));
            else if (request.QueryResult.Intent.DisplayName == "buy.iPad - yes")
                return (iPad.buyipadUp(request));
            else if (request.QueryResult.Intent.DisplayName == "buy.iPad - yes - nameAddress")
                return await iPad.buyiPad_finishAsync(request);
            else if (request.QueryResult.Intent.DisplayName == "Consult")
                return consult.consult(request);

            return response;

        }
    }
}
