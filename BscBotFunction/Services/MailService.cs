﻿using System;
using System.Net.Mail;
using BscBotFunction.Models;
using BscBotFunction.Models.DialogFlow;

namespace BscBotFunction.Services
{
    public class MailService
    {
        public MailService()
        {
        }
        public void sendMail(parameters parms)
        {



            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            mail.To.Add(parms.email);
            mail.From = new MailAddress("chatbotprototyp@gmail.com", "Chatbot Prototyp", System.Text.Encoding.UTF8);
            mail.Subject = "Auftragsbestätigung für den kauf deines " + parms.DeviceName;
            mail.SubjectEncoding = System.Text.Encoding.UTF8;
            mail.Body = "Hallo " + parms.givenName + ", </br></br> Hiermit bestätigen wir den Kauf deines " + parms.DeviceName +
            ". </br> Der Betrag ist erfolgreich auf unserem Konto eingegangen. Das Paket wird in kürze versendet." +
              "</br> </br>Vielen Dank für den Einkauf, </br> Dein TUDA Team";
            mail.BodyEncoding = System.Text.Encoding.UTF8;
            mail.IsBodyHtml = true;
            mail.Priority = MailPriority.High;
            SmtpClient client = new SmtpClient();
            client.Credentials = new System.Net.NetworkCredential("chatbotprototyp@gmail.com", "****");
            client.Port = 587;
            client.Host = "smtp.gmail.com";
            client.EnableSsl = true;
            try
            {
                client.Send(mail);
            }
            catch (Exception ex)
            {
                Exception ex2 = ex;
                string errorMessage = string.Empty;
                while (ex2 != null)
                {
                    errorMessage += ex2.ToString();
                    ex2 = ex2.InnerException;
                }
            }
        }
    }
}
