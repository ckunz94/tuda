﻿using System;
using System.Collections.Generic;
using System.Text;
using BscBotFunction.Services;
using BscBotFunction.Models;
using BscBotFunction.Models.DialogFlow;

using Google.Cloud.Dialogflow.V2;
using Newtonsoft.Json.Linq;
namespace BscBotFunction.Services
{
    public class MacService
    {
        DialogflowService DFS;
        public MacService()
        {
            DFS = new DialogflowService();
        }
        public WebhookResponse buyMacbook(WebhookRequest request)
        {
            WebhookResponse response = new WebhookResponse();
            DatabaseService DbS = new DatabaseService();

            if (request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceCapacity").StringValue != "")
            {
                int DeviceCapacity = Int32.Parse(request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceCapacity").StringValue);
                if (DeviceCapacity < 7)
                    request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceCapacity").StringValue = request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceCapacity").StringValue + " TB";
                else
                    request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceCapacity").StringValue = request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceCapacity").StringValue + " GB";

            }



            if (request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceCapacity").StringValue != "" &&
                request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceName").StringValue != "" &&
                request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceColor").StringValue != "" &&
                request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceDisplaySize").StringValue != "" &&
                request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceProcessorName").StringValue != "" &&
                request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceProcessorGHZ").StringValue != "" &&
                request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceProcessorCore").StringValue != "" &&
                request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceMemory").StringValue != "")
            {
                // buy specific MacBook 


                string deviceName = request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceName").StringValue;
                string deviceColor = request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceColor").StringValue;
                string deviceCapacity = request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceCapacity").StringValue;
                string deviceDisplaySize = request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceDisplaySize").StringValue;
                string deviceProcessorName = request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceProcessorName").StringValue;
                string deviceProcessorGHz = request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceProcessorGHZ").StringValue;
                string deviceProcessorCore = request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceProcessorCore").StringValue;
                string deviceMemory = request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceMemory").StringValue;
                string query1 = "SELECT DISTINCT Price, imageURL FROM Mac, Mac_Display, Mac_Processor WHERE Mac.Display = Mac_Display.id AND Mac.Processor = Mac_Processor.id AND Mac.Name = '" + deviceName + "' AND Color = '" + deviceColor + "' AND Capacity = '" + deviceCapacity + "' AND Mac_Display.inch = '" + deviceDisplaySize + "' AND RAM = '" + deviceMemory + "' AND Mac_Processor.name = '" + deviceProcessorName + "' AND Mac_Processor.GHZ = '" + deviceProcessorGHz + "' AND Mac_Processor.cores = '" + deviceProcessorCore + "'";
                string[] DBresponse = DbS.readDatabase(query1);
                if (DBresponse.Length == 0 || DBresponse == null)
                {
                    response.FulfillmentText = "Für die gewählte Konfiguration konnte ich leider kein Gerät finden.";
                    return response;
                }
                string devicePrice = DBresponse[0];
                string imageURL = DBresponse[1];

                if (deviceDisplaySize == "no")
                    deviceDisplaySize = "";
                else
                    deviceDisplaySize = deviceDisplaySize + " Zoll";
                response.FulfillmentText = "Möchtest du den " + deviceName + " " + deviceDisplaySize + " in " + deviceColor + " mit " + deviceCapacity + " Speicherplatz, " + deviceMemory + " RAM und " + deviceProcessorGHz + " " + deviceProcessorName + " Prozessor mit " + deviceProcessorCore + " Kernen für " + devicePrice + "€ kaufen?";

                Intent.Types.Message.Types.Card responseCard = new Intent.Types.Message.Types.Card();
                responseCard.Buttons.Add(new Intent.Types.Message.Types.Card.Types.Button() { Text = "Ja" });
                responseCard.Buttons.Add(new Intent.Types.Message.Types.Card.Types.Button() { Text = "Nein" });
                responseCard.Title = "Konfiguration Kaufen";
                responseCard.Subtitle = response.FulfillmentText;
                responseCard.ImageUri = imageURL;




                Intent.Types.Message responseMessageCard = new Intent.Types.Message();
                responseMessageCard.Card = responseCard;
                response.FulfillmentMessages.Add(responseMessageCard);


                return response;
            }



            ConditionBuildService CBS = new ConditionBuildService();
            List<ParameterModel> parameterList = new List<ParameterModel>();
            parameterList.Add(CBS.makeParameterModel("Type", request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceType").StringValue, "="));
            parameterList.Add(CBS.makeParameterModel("Color", request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceColor").StringValue, "="));
            parameterList.Add(CBS.makeParameterModel("Capacity", request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceCapacity").StringValue, "="));
            parameterList.Add(CBS.makeParameterModel("Mac.Name", request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceName").StringValue, "="));
            parameterList.Add(CBS.makeParameterModel("Mac_Display.inch", request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceDisplaySize").StringValue, "="));
            parameterList.Add(CBS.makeParameterModel("RAM", request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceMemory").StringValue, "="));
            parameterList.Add(CBS.makeParameterModel("Mac_Processor.name", request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceProcessorName").StringValue, "="));
            parameterList.Add(CBS.makeParameterModel("Mac_Processor.GHZ", request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceProcessorGHZ").StringValue, "="));
            parameterList.Add(CBS.makeParameterModel("Mac_Processor.cores", request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceProcessorCore").StringValue, "="));


            if (request.QueryResult.Parameters.Fields.GetValueOrDefault("UnitCurrency").StructValue != null)
            {
                Google.Protobuf.WellKnownTypes.Value priceCurrencyValue = new Google.Protobuf.WellKnownTypes.Value();
                priceCurrencyValue = request.QueryResult.Parameters.Fields.GetValueOrDefault("UnitCurrency");
                string priceStruct = priceCurrencyValue.StructValue.ToString();
                JToken UnitCurrencyJson = JToken.Parse(priceStruct);
                string price = UnitCurrencyJson["amount"].ToString();
                parameterList.Add(CBS.makeParameterModel("Price", price, "<"));
            }




            string condition = CBS.buildCondition(parameterList);
            string query = "";
            if (request.QueryResult.FulfillmentText == "Welches Gerät möchtest du haben?")
                query = "SELECT DISTINCT Mac.Name FROM Mac, Mac_Display, Mac_Processor WHERE Mac.Display = Mac_Display.id AND Mac.Processor = Mac_Processor.id AND " + condition;
            else if (request.QueryResult.FulfillmentText == "Wie groß soll das Display denn sein?")
                query = "SELECT DISTINCT Mac_Display.inch FROM Mac, Mac_Display, Mac_Processor WHERE Mac.Display = Mac_Display.id AND Mac.Processor = Mac_Processor.id AND " + condition + "ORDER BY Mac_Display.inch ASC";
            else if (request.QueryResult.FulfillmentText == "Welche Farbe soll das Gerät haben?")
                query = "SELECT DISTINCT Color FROM Mac, Mac_Display, Mac_Processor WHERE Mac.Display = Mac_Display.id AND Mac.Processor = Mac_Processor.id AND " + condition;
            else if (request.QueryResult.FulfillmentText == "Wie groß soll die Festplatte sein?")
                query = "SELECT DISTINCT Capacity FROM Mac, Mac_Display, Mac_Processor WHERE Mac.Display = Mac_Display.id AND Mac.Processor = Mac_Processor.id AND " + condition + "ORDER BY Capacity ASC";
            else if (request.QueryResult.FulfillmentText == "Wie viel Arbeitsspeicher soll das Gerät haben?")
                query = "SELECT DISTINCT RAM FROM Mac, Mac_Display, Mac_Processor WHERE Mac.Display = Mac_Display.id AND Mac.Processor = Mac_Processor.id AND " + condition + "ORDER BY RAM ASC";
            else if (request.QueryResult.FulfillmentText == "Welchen Prozessor möchtest du haben?")
                query = "SELECT DISTINCT Mac_Processor.name, Mac_Processor.cores, Mac_Processor.GHZ FROM Mac, Mac_Display, Mac_Processor WHERE Mac.Display = Mac_Display.id AND Mac.Processor = Mac_Processor.id AND " + condition;
            else if (request.QueryResult.FulfillmentText == "Welchen Gerätetyp möchtest du haben?")
                query = "SELECT DISTINCT Type FROM Mac, Mac_Display, Mac_Processor WHERE Mac.Display = Mac_Display.id AND Mac.Processor = Mac_Processor.id AND " + condition;
            else if (request.QueryResult.FulfillmentText == "Welche Grafikkarte möchtest du in deinem Gerät haben?")
                query = "SELECT DISTINCT Graphik FROM Mac, Mac_Display, Mac_Processor WHERE Mac.Display = Mac_Display.id AND Mac.Processor = Mac_Processor.id AND " + condition;

            string[] dbResult = DbS.readDatabase(query);
            if (dbResult.Length == 0 || dbResult == null)
            {
                response.FulfillmentText = "Für die gewählte Konfiguration konnte ich leider kein Gerät finden.";
                return response;
            }

            if (request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceType").StringValue == "Desktop")
            {
                response.OutputContexts.Add(DFS.addToContext("DeviceDisplaySize", "no", request));

            }
            if (request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceType").StringValue == "Desktop" ||
                request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceType").StringValue == "All-in-one")
            {
                string skipQuery = "SELECT DISTINCT Color FROM Mac, Mac_Display, Mac_Processor WHERE Mac.Display = Mac_Display.id AND Mac.Processor = Mac_Processor.id AND " + condition;
                string[] skipdbResult = DbS.readDatabase(skipQuery);
                if (skipdbResult.Length == 1)
                    response.OutputContexts.Add(DFS.addToContext("DeviceColor", skipdbResult[0], request));

            }
            if (request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceType").StringValue == "Laptop" ||
                request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceName").StringValue == "Mac Mini")
            {
                string skipQuery = "SELECT DISTINCT Graphik FROM Mac, Mac_Display, Mac_Processor WHERE Mac.Display = Mac_Display.id AND Mac.Processor = Mac_Processor.id AND " + condition;
                string[] skipdbResult = DbS.readDatabase(skipQuery);
                if (skipdbResult.Length == 1)
                    response.OutputContexts.Add(DFS.addToContext("DeviceGraphicCard", skipdbResult[0], request));
            }




            if (request.QueryResult.FulfillmentText == "Welchen Prozessor möchtest du haben?")
            {

                List<string> newDBresultlist = new List<string>();
                for (int i = 0; i < dbResult.Length; i += 3)
                {
                    newDBresultlist.Add(dbResult[i + 2] + " " + dbResult[i] + " Prozessor mit " + dbResult[i + 1] + " Kernen");
                }
                dbResult = newDBresultlist.ToArray();
            }
            else if (request.QueryResult.FulfillmentText == "Wie viel Arbeitsspeicher soll das Gerät haben?")
            {
                for (int i = 0; i < dbResult.Length; i++)
                {
                    dbResult[i] = dbResult[i] + " RAM";
                }
            }
            else if (request.QueryResult.FulfillmentText == "Wie groß soll das Display denn sein?")
            {
                for (int i = 0; i < dbResult.Length; i++)
                {
                    dbResult[i] = dbResult[i] + " Zoll";
                }
            }





            Intent.Types.Message.Types.Card card = new Intent.Types.Message.Types.Card();

            foreach (string option in dbResult)
            {
                card.Buttons.Add(new Intent.Types.Message.Types.Card.Types.Button() { Text = option });
            }
            card.Title = request.QueryResult.FulfillmentText;
            Intent.Types.Message messageCard = new Intent.Types.Message();
            messageCard.Card = card;
            response.FulfillmentMessages.Add(messageCard);


            StringBuilder sb = new StringBuilder();
            sb.Append(request.QueryResult.FulfillmentText);
            sb.Append(" Zur Auswahl stehen: ");
            sb.Append(DFS.listToString(dbResult));
            if (request.QueryResult.FulfillmentText == "Wie groß soll das Display denn sein?")
                sb.Append(" Zoll.");
            else
                sb.Append(".");
            response.FulfillmentText = sb.ToString();

            return response;

        }
        public WebhookResponse buyMacbookAddon(WebhookRequest request)
        {


            WebhookResponse response = new WebhookResponse();
            bool parmsPresent = false;
            try
            {
                parmsPresent = request.QueryResult.AllRequiredParamsPresent;
            }
            catch { }

            if (parmsPresent)
            {
                response.FulfillmentText = "Alles klar. Dann benötige ich jetzt deine Lieferdaten. Fangen wir mit deinem Namen an: Wie heißt du?";
                return response;
            }

            DatabaseService DbS = new DatabaseService();
            string query1 = "SELECT DISTINCT Name, shortName, Price FROM Addon WHERE deviceType = 'Mac'";
            string[] DBresponse = DbS.readDatabase(query1);



            Intent.Types.Message.Types.Card responseCard = new Intent.Types.Message.Types.Card();
            for (int i = 0; i < DBresponse.Length; i = i + 3)
            {
                responseCard.Buttons.Add(new Intent.Types.Message.Types.Card.Types.Button() { Text = DBresponse[i] + " ( " + DBresponse[i + 2] + " € )" }); //Postback = DBresponse[i +1]
            }
            responseCard.Buttons.Add(new Intent.Types.Message.Types.Card.Types.Button() { Text = DBresponse[0] + " ( " + DBresponse[2] + " € )" + " und " + DBresponse[3] + " ( " + DBresponse[5] + " € )" }); //Postback = DBresponse[1] + " und " + DBresponse[4]
            responseCard.Buttons.Add(new Intent.Types.Message.Types.Card.Types.Button() { Text = "keine" });
            responseCard.Title = "Zusätzliche Software";
            responseCard.Subtitle = "Sehr gute Wahl! Welche Software möchtest du zusätzlich installiert haben?";





            Intent.Types.Message responseMessageCard = new Intent.Types.Message();
            responseMessageCard.Card = responseCard;
            response.FulfillmentMessages.Add(responseMessageCard);


            return response;

        }
        public async System.Threading.Tasks.Task<WebhookResponse> buyMacbook_finishAsync(WebhookRequest request)
        {
            WebhookResponse response = new WebhookResponse();
            parameters parms = DFS.outputContextToParameters(request);
            DatabaseService DbS = new DatabaseService();
            if (parms.DeviceCapacity != "")
            {
                int DeviceCapacity = Int32.Parse(parms.DeviceCapacity);
                if (DeviceCapacity < 7)
                    parms.DeviceCapacity = parms.DeviceCapacity + " TB";
                else
                    parms.DeviceCapacity = parms.DeviceCapacity + " GB";
            }


            if (parms.DeviceDisplaySize != "" && parms.DeviceDisplaySize != "no")
                parms.DeviceDisplaySize = parms.DeviceDisplaySize.Remove(parms.DeviceDisplaySize.Length - 5);


            try
            {
                for (int i = 0; i < parms.AddonSells.Length; i++)
                {
                    string queryAddon = "SELECT Name, Price FROM Addon Where shortName = '" + parms.AddonSells[i].shortName + "'";
                    parms.AddonSells[i].name = DbS.readDatabase(queryAddon)[0];
                    parms.AddonSells[i].price = Int32.Parse(DbS.readDatabase(queryAddon)[1]);
                }
            }
            catch { }





            string deviceName = parms.DeviceName;
            string deviceColor = parms.DeviceColor;
            string deviceCapacity = parms.DeviceCapacity;
            string deviceDisplaySize = parms.DeviceDisplaySize;
            string deviceProcessorName = parms.DeviceProcessorName;
            string deviceProcessorGHz = parms.DeviceProcessorGHz;
            string deviceProcessorCore = parms.DeviceProcessorCore;
            string deviceMemory = parms.DeviceMemory;
            string deviceGraphik = parms.DeviceGraphik;



            string query1 = "SELECT DISTINCT Price FROM Mac, Mac_Display, Mac_Processor WHERE Mac.Display = Mac_Display.id AND Mac.Processor = Mac_Processor.id AND Mac.Name = '" + deviceName + "' AND Color = '" + deviceColor + "' AND Capacity = '" + deviceCapacity + "' AND Mac_Display.inch = '" + deviceDisplaySize + "' AND RAM = '" + deviceMemory + "' AND Mac_Processor.name = '" + deviceProcessorName + "' AND Mac_Processor.GHZ = '" + deviceProcessorGHz + "' AND Mac_Processor.cores = '" + deviceProcessorCore + "' AND Graphik = '" + deviceGraphik + "'";

            string devicePrice = DbS.readDatabase(query1)[0];
            unitCurrency unitCurrency = new unitCurrency();
            unitCurrency.amount = Int32.Parse(devicePrice);
            parms.UnitCurrency = unitCurrency;

            PayPalService PPS = new PayPalService();
            string paypalURL = await PPS.PayAsync(parms, request.OriginalDetectIntentRequest.Source);

            response.FulfillmentText = "Vielen Dank! Bitte folge dem folgendem link um die Zahlung durchzuführen und die Bestellung abzuschließen: "
                + paypalURL + " Nach erfolgreichem Abschluss der Bezahlung erhälst du eine E-Mail als Bestätigung.";



            Intent.Types.Message.Types.Card.Types.Button buttonBuy = new Intent.Types.Message.Types.Card.Types.Button();
            buttonBuy.Text = "jetzt kaufen";
            buttonBuy.Postback = paypalURL;
            Intent.Types.Message.Types.Card.Types.Button buttonCancle = new Intent.Types.Message.Types.Card.Types.Button();
            buttonCancle.Text = "abbrechen";


            Intent.Types.Message.Types.Card card = new Intent.Types.Message.Types.Card();
            card.Buttons.Add(buttonBuy);
            card.Buttons.Add(buttonCancle);
            card.Title = "Mit Paypal Zahlen";
            card.Subtitle = "Vielen Dank! Nach erfolgreicher Bezahlung erhälst du eine E-Mail als Bestätigung. Klicke dazu auf 'Jetzt Kaufen'";



            Intent.Types.Message messageCard = new Intent.Types.Message();
            messageCard.Card = card;
            response.FulfillmentMessages.Add(messageCard);



            return response;
        }
    }
}
