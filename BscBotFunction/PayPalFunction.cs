using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using BscBotFunction.Services;
using Google.Cloud.Dialogflow.V2;
using Google.Protobuf;

namespace BscBotFunction
{
    /// <summary>
    /// Represents the PayPal Function Class.
    /// </summary>
    public static class PayPalFunction
    {

        /// <summary>
        /// Gets called by PayPal if Payment is successfully finished.
        /// </summary>
        /// <param name=""PayPal/{FirstName}/{LastName}/{EMail}/{Adress}/{Zip}/{City}/{Item}/{Source}""></param>
        /// <returns>RedirectResult with URL to origin of user request</returns>
        [FunctionName("PayPalFunction")]
        public static async Task<RedirectResult> Run([HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "{FirstName}/{LastName}/{EMail}/{Adress}/{Zip}/{City}/{Item}/{Source}")]HttpRequest req, string FirstName, string LastName, string EMail, string Adress, string Zip, string City, string Item, string Source, ILogger log)
        {
            MailService mailSerive = new MailService();
            //var context = HttpContext;
            Models.DialogFlow.parameters parameters = new Models.DialogFlow.parameters();
            StringEditService SED = new StringEditService();



            parameters.givenName = SED.reloadSpaceDotAt(FirstName);
            parameters.lastName = SED.reloadSpaceDotAt(LastName);
            parameters.DeviceName = SED.reloadSpaceDotAt(Item);
            parameters.email = SED.reloadSpaceDotAt(EMail);
            parameters.address = SED.reloadSpaceDotAt(Adress);
            parameters.zipCode = SED.reloadSpaceDotAt(Zip);
            parameters.geoCity = SED.reloadSpaceDotAt(City);
            mailSerive.sendMail(parameters);

            //slack_testbot -> https://slack.com/intl/de/
            //telegram
            //kik
            //google - https://bot.dialogflow.com/9b2201c5-2f8f-4222-a0ff-a185e600ed18

            RedirectResult Redirect_slack = new RedirectResult("https://slack.com");
            RedirectResult Redirect_close = new RedirectResult("https://bscchatbotblob.blob.core.windows.net/html/close.html");
            RedirectResult Redirect_dialogflow = new RedirectResult("https://bot.dialogflow.com/9b2201c5-2f8f-4222-a0ff-a185e600ed18");


            if (Source == "slack_testbot")
                return Redirect_slack;
            else if (Source == "telegram")
                return Redirect_close;
            else if (Source == "kik")
                return Redirect_close;
            else
                return Redirect_dialogflow; 
        }
    }
}
