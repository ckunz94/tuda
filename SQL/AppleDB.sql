/*
 Navicat Premium Data Transfer

 Source Server         : bsc-chatbot
 Source Server Type    : SQL Server
 Source Server Version : 12001300
 Source Host           : bsc-chatbot.database.windows.net
 Source Database       : apple
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 12001300
 File Encoding         : utf-8

 Date: 04/12/2019 00:58:18 AM
*/

-- ----------------------------
--  Table structure for [dbo].[iPhone_copy]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[iPhone_copy]') AND type IN ('U'))
	DROP TABLE [dbo].[iPhone_copy]
GO
CREATE TABLE [dbo].[iPhone_copy] (
	[ID] int NOT NULL,
	[Name] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Color] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Capacity] int NULL,
	[Size] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Weight] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Display] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IPProtection] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Processor] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Camera] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Videorecording] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FrontCamera] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Biometrie] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ApplePay] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Wireless] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Locating] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PowerSupply] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Sensors] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SimCard] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Price] int NULL,
	[test] varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__iPhone_c__3214EC274748C7B0] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[iPhone_copy]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[iPhone_copy] VALUES ('1', 'iPhone 7', 'Rosegold', '32', 'iPh7', '138', 'iPh7', 'IP67', 'A10', '1', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7', '1', '1', '519', 'test');
INSERT INTO [dbo].[iPhone_copy] VALUES ('2', 'iPhone 7', 'Gold', '32', 'iPh7', '138', 'iPh7', 'IP67', 'A10', '1', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7', '1', '1', '519', 'ztrertz');
INSERT INTO [dbo].[iPhone_copy] VALUES ('3', 'iPhone 7', 'Silber', '32', 'iPh7', '138', 'iPh7', 'IP67', 'A10', '1', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7', '1', '1', '519', 'dfg');
INSERT INTO [dbo].[iPhone_copy] VALUES ('4', 'iPhone 7', 'Schwarz', '32', 'iPh7', '138', 'iPh7', 'IP67', 'A10', '1', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7', '1', '1', '519', 'dgf');
INSERT INTO [dbo].[iPhone_copy] VALUES ('5', 'iPhone 7', 'Rosegold', '128', 'iPh7', '138', 'iPh7', 'IP67', 'A10', '1', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7', '1', '1', '629', null);
INSERT INTO [dbo].[iPhone_copy] VALUES ('6', 'iPhone 7', 'Gold', '128', 'iPh7', '138', 'iPh7', 'IP67', 'A10', '1', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7', '1', '1', '629', null);
INSERT INTO [dbo].[iPhone_copy] VALUES ('7', 'iPhone 7', 'Silber', '128', 'iPh7', '138', 'iPh7', 'IP67', 'A10', '1', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7', '1', '1', '629', null);
INSERT INTO [dbo].[iPhone_copy] VALUES ('8', 'iPhone 7', 'Schwarz', '128', 'iPh7', '138', 'iPh7', 'IP67', 'A10', '1', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7', '1', '1', '629', null);
INSERT INTO [dbo].[iPhone_copy] VALUES ('9', 'iPhone 7 Plus', 'Rosegold', '32', 'iPh7Plus', '188', 'iPh7Plus', 'IP67', 'A10', '2', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7Plus', '1', '1', '649', null);
INSERT INTO [dbo].[iPhone_copy] VALUES ('10', 'iPhone 7 Plus', 'Gold', '32', 'iPh7Plus', '188', 'iPh7Plus', 'IP67', 'A10', '2', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7Plus', '1', '1', '649', null);
INSERT INTO [dbo].[iPhone_copy] VALUES ('11', 'iPhone 7 Plus', 'Schwarz', '32', 'iPh7Plus', '188', 'iPh7Plus', 'IP67', 'A10', '2', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7Plus', '1', '1', '649', null);
INSERT INTO [dbo].[iPhone_copy] VALUES ('12', 'iPhone 7 Plus', 'Silber', '32', 'iPh7Plus', '188', 'iPh7Plus', 'IP67', 'A10', '2', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7Plus', '1', '1', '649', null);
INSERT INTO [dbo].[iPhone_copy] VALUES ('13', 'iPhone 7 Plus', 'Rosegold', '128', 'iPh7Plus', '188', 'iPh7Plus', 'IP67', 'A10', '2', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7Plus', '1', '1', '759', null);
INSERT INTO [dbo].[iPhone_copy] VALUES ('14', 'iPhone 7 Plus', 'Gold', '128', 'iPh7Plus', '188', 'iPh7Plus', 'IP67', 'A10', '2', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7Plus', '1', '1', '759', null);
INSERT INTO [dbo].[iPhone_copy] VALUES ('15', 'iPhone 7 Plus', 'Schwarz', '128', 'iPh7Plus', '188', 'iPh7Plus', 'IP67', 'A10', '2', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7Plus', '1', '1', '759', null);
INSERT INTO [dbo].[iPhone_copy] VALUES ('16', 'iPhone 7 Plus', 'Silber', '128', 'iPh7Plus', '188', 'iPh7Plus', 'IP67', 'A10', '2', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7Plus', '1', '1', '759', null);
INSERT INTO [dbo].[iPhone_copy] VALUES ('17', 'iPhone 8', 'Silber', '64', 'iPh8', '148', 'iPh8', 'IP67', 'A11', '1', '2', 'FT', 'Touch ID', 'ja', 'iPh8', '1', 'iPh8', '1', '1', '679', null);
INSERT INTO [dbo].[iPhone_copy] VALUES ('18', 'iPhone 8', 'Space Grau', '64', 'iPh8', '148', 'iPh8', 'IP67', 'A11', '1', '2', 'FT', 'Touch ID', 'ja', 'iPh8', '1', 'iPh8', '1', '1', '679', null);
INSERT INTO [dbo].[iPhone_copy] VALUES ('19', 'iPhone 8', 'Gold', '64', 'iPh8', '148', 'iPh8', 'IP67', 'A11', '1', '2', 'FT', 'Touch ID', 'ja', 'iPh8', '1', 'iPh8', '1', '1', '679', null);
INSERT INTO [dbo].[iPhone_copy] VALUES ('20', 'iPhone 8', 'Silber', '256', 'iPh8', '148', 'iPh8', 'IP67', 'A11', '1', '2', 'FT', 'Touch ID', 'ja', 'iPh8', '1', 'iPh8', '1', '1', '849', null);
INSERT INTO [dbo].[iPhone_copy] VALUES ('21', 'iPhone 8', 'Space Grau', '256', 'iPh8', '148', 'iPh8', 'IP67', 'A11', '1', '2', 'FT', 'Touch ID', 'ja', 'iPh8', '1', 'iPh8', '1', '1', '849', null);
INSERT INTO [dbo].[iPhone_copy] VALUES ('22', 'iPhone 8', 'Gold', '256', 'iPh8', '148', 'iPh8', 'IP67', 'A11', '1', '2', 'FT', 'Touch ID', 'ja', 'iPh8', '1', 'iPh8', '1', '1', '849', null);
INSERT INTO [dbo].[iPhone_copy] VALUES ('23', 'iPhone 8 Plus', 'Silber', '64', 'iPh8Plus', '202', 'iPh8Plus', 'IP67', 'A11', '2', '2', 'FT', 'Touch ID', 'ja', 'iPh8', '1', 'iPh8', '1', '1', '789', null);
INSERT INTO [dbo].[iPhone_copy] VALUES ('24', 'iPhone 8 Plus', 'Space Grau', '64', 'iPh8Plus', '202', 'iPh8Plus', 'IP67', 'A11', '2', '2', 'FT', 'Touch ID', 'ja', 'iPh8', '1', 'iPh8', '1', '1', '789', null);
INSERT INTO [dbo].[iPhone_copy] VALUES ('25', 'iPhone 8 Plus', 'Gold', '64', 'iPh8Plus', '202', 'iPh8Plus', 'IP67', 'A11', '2', '2', 'FT', 'Touch ID', 'ja', 'iPh8', '1', 'iPh8', '1', '1', '789', null);
INSERT INTO [dbo].[iPhone_copy] VALUES ('26', 'iPhone 8 Plus', 'Silber', '256', 'iPh8Plus', '202', 'iPh8Plus', 'IP67', 'A11', '2', '2', 'FT', 'Touch ID', 'ja', 'iPh8', '1', 'iPh8', '1', '1', '959', null);
INSERT INTO [dbo].[iPhone_copy] VALUES ('27', 'iPhone 8 Plus', 'Space Grau', '256', 'iPh8Plus', '202', 'iPh8Plus', 'IP67', 'A11', '2', '2', 'FT', 'Touch ID', 'ja', 'iPh8', '1', 'iPh8', '1', '1', '959', null);
INSERT INTO [dbo].[iPhone_copy] VALUES ('28', 'iPhone 8 Plus', 'Gold', '256', 'iPh8Plus', '202', 'iPh8Plus', 'IP67', 'A11', '2', '2', 'FT', 'Touch ID', 'ja', 'iPh8', '1', 'iPh8', '1', '1', '959', null);
INSERT INTO [dbo].[iPhone_copy] VALUES ('29', 'iPhone XR', 'Blau', '64', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[Mac]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[Mac]') AND type IN ('U'))
	DROP TABLE [dbo].[Mac]
GO
CREATE TABLE [dbo].[Mac] (
	[ID] int NOT NULL,
	[Name] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Color] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Display] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Capacity] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Size] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Weight] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Processor] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[RAM] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Graphik] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Ports] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[KeyboardTrackpad] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Wireless] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PowerSupply] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Price] int NULL,
	[Type] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[imageURL] varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__Mac__3214EC27FCBA66EA] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[Mac]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[Mac] VALUES ('1', 'MacBook Pro', 'Space Grau', 'MBP13', '128 GB', 'MBP13', '1,37 KG', 'MBP13_1', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '1499', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('2', 'MacBook Pro', 'Space Grau', 'MBP13', '256 GB', 'MBP13', '1.37 KG', 'MBP13_1', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '1749', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('3', 'MacBook Pro', 'Space Grau', 'MBP13', '512 GB', 'MBP13', '1.37 KG', 'MBP13_1', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '1999', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('4', 'MacBook Pro', 'Space Grau', 'MBP13', '1 TB', 'MBP13', '1.37 KG', 'MBP13_1', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2499', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('5', 'MacBook Pro', 'Space Grau', 'MBP13', '128 GB', 'MBP13', '1.37 KG', 'MBP13_1', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '1739', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('6', 'MacBook Pro', 'Space Grau', 'MBP13', '256 GB', 'MBP13', '1.37 KG', 'MBP13_1', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '1989', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('7', 'MacBook Pro', 'Space Grau', 'MBP13', '512 GB', 'MBP13', '1.37 KG', 'MBP13_1', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2239', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('8', 'MacBook Pro', 'Space Grau', 'MBP13', '1 TB', 'MBP13', '1.37 KG', 'MBP13_1', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2739', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('9', 'MacBook Pro', 'Space Grau', 'MBP13', '128 GB', 'MBP13', '1.37 KG', 'MBP13_2', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '1859', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('10', 'MacBook Pro', 'Space Grau', 'MBP13', '256 GB', 'MBP13', '1.37 KG', 'MBP13_2', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2109', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('11', 'MacBook Pro', 'Space Grau', 'MBP13', '512 GB', 'MBP13', '1.37 KG', 'MBP13_2', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2359', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('12', 'MacBook Pro', 'Space Grau', 'MBP13', '1 TB', 'MBP13', '1.37 KG', 'MBP13_2', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2859', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('13', 'MacBook Pro', 'Space Grau', 'MBP13', '128 GB', 'MBP13', '1.37 KG', 'MBP13_2', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2099', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('14', 'MacBook Pro', 'Space Grau', 'MBP13', '256 GB', 'MBP13', '1.37 KG', 'MBP13_2', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2349', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('15', 'MacBook Pro', 'Space Grau', 'MBP13', '512 GB', 'MBP13', '1.37 KG', 'MBP13_2', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2599', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('16', 'MacBook Pro', 'Space Grau', 'MBP13', '1 TB', 'MBP13', '1.37 KG', 'MBP13_2', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '3099', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('17', 'MacBook Pro', 'Silber', 'MBP13', '128 GB', 'MBP13', '1.37 KG', 'MBP13_1', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '1499', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('18', 'MacBook Pro', 'Silber', 'MBP13', '256 GB', 'MBP13', '1.37 KG', 'MBP13_1', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '1749', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('19', 'MacBook Pro', 'Silber', 'MBP13', '512 GB', 'MBP13', '1.37 KG', 'MBP13_1', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '1999', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('20', 'MacBook Pro', 'Silber', 'MBP13', '1 TB', 'MBP13', '1.37 KG', 'MBP13_1', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2499', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('21', 'MacBook Pro', 'Silber', 'MBP13', '128 GB', 'MBP13', '1.37 KG', 'MBP13_1', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '1739', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('22', 'MacBook Pro', 'Silber', 'MBP13', '256 GB', 'MBP13', '1.37 KG', 'MBP13_1', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '1989', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('23', 'MacBook Pro', 'Silber', 'MBP13', '512 GB', 'MBP13', '1.37 KG', 'MBP13_1', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2239', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('24', 'MacBook Pro', 'Silber', 'MBP13', '1 TB', 'MBP13', '1.37 KG', 'MBP13_1', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2739', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('25', 'MacBook Pro', 'Silber', 'MBP13', '128 GB', 'MBP13', '1.37 KG', 'MBP13_2', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '1859', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('26', 'MacBook Pro', 'Silber', 'MBP13', '256 GB', 'MBP13', '1.37 KG', 'MBP13_2', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2109', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('27', 'MacBook Pro', 'Silber', 'MBP13', '512 GB', 'MBP13', '1.37 KG', 'MBP13_2', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2359', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('28', 'MacBook Pro', 'Silber', 'MBP13', '1 TB', 'MBP13', '1.37 KG', 'MBP13_2', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2859', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('29', 'MacBook Pro', 'Silber', 'MBP13', '128 GB', 'MBP13', '1.37 KG', 'MBP13_2', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2099', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('30', 'MacBook Pro', 'Silber', 'MBP13', '256 GB', 'MBP13', '1.37 KG', 'MBP13_2', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2349', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('31', 'MacBook Pro', 'Silber', 'MBP13', '512 GB', 'MBP13', '1.37 KG', 'MBP13_2', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2599', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('32', 'MacBook Pro', 'Silber', 'MBP13', '1 TB', 'MBP13', '1.37 KG', 'MBP13_2', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '3099', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('33', 'MacBook Pro', 'Space Grau', 'MBP13TB', '256 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '1999', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('34', 'MacBook Pro', 'Space Grau', 'MBP13TB', '512 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2249', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('35', 'MacBook Pro', 'Space Grau', 'MBP13TB', '1 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2749', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('36', 'MacBook Pro', 'Space Grau', 'MBP13TB', '2 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '3749', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('37', 'MacBook Pro', 'Space Grau', 'MBP13TB', '256 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2239', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('38', 'MacBook Pro', 'Space Grau', 'MBP13TB', '512 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2489', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('39', 'MacBook Pro', 'Space Grau', 'MBP13TB', '1 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2989', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('40', 'MacBook Pro', 'Space Grau', 'MBP13TB', '2 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '3989', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('41', 'MacBook Pro', 'Space Grau', 'MBP13TB', '256 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2359', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('42', 'MacBook Pro', 'Space Grau', 'MBP13TB', '512 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2609', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('43', 'MacBook Pro', 'Space Grau', 'MBP13TB', '1 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '3109', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('44', 'MacBook Pro', 'Space Grau', 'MBP13TB', '2 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '4109', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('45', 'MacBook Pro', 'Space Grau', 'MBP13TB', '256 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2599', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('46', 'MacBook Pro', 'Space Grau', 'MBP13TB', '512 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2849', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('47', 'MacBook Pro', 'Space Grau', 'MBP13TB', '1 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '3349', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('48', 'MacBook Pro', 'Space Grau', 'MBP13TB', '2 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '4349', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('49', 'MacBook Pro', 'Silber', 'MBP13TB', '256 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '1999', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('50', 'MacBook Pro', 'Silber', 'MBP13TB', '512 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2249', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('51', 'MacBook Pro', 'Silber', 'MBP13TB', '1 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2749', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('52', 'MacBook Pro', 'Silber', 'MBP13TB', '2 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '3749', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('53', 'MacBook Pro', 'Silber', 'MBP13TB', '256 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2239', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('54', 'MacBook Pro', 'Silber', 'MBP13TB', '512 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2489', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('55', 'MacBook Pro', 'Silber', 'MBP13TB', '1 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2989', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('56', 'MacBook Pro', 'Silber', 'MBP13TB', '2 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '3989', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('57', 'MacBook Pro', 'Silber', 'MBP13TB', '256 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2359', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('58', 'MacBook Pro', 'Silber', 'MBP13TB', '512 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2609', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('59', 'MacBook Pro', 'Silber', 'MBP13TB', '1 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '3109', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('60', 'MacBook Pro', 'Silber', 'MBP13TB', '2 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '4109', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('61', 'MacBook Pro', 'Silber', 'MBP13TB', '256 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2599', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('62', 'MacBook Pro', 'Silber', 'MBP13TB', '512 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2849', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('63', 'MacBook Pro', 'Silber', 'MBP13TB', '1 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '3349', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('64', 'MacBook Pro', 'Silber', 'MBP13TB', '2 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '4349', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp13touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('65', 'MacBook Pro', 'Space Grau', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '2799', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('66', 'MacBook Pro', 'Space Grau', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3039', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('67', 'MacBook Pro', 'Space Grau', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3519', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('68', 'MacBook Pro', 'Space Grau', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4479', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('69', 'MacBook Pro', 'Space Grau', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '6879', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('70', 'MacBook Pro', 'Space Grau', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '2919', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('71', 'MacBook Pro', 'Space Grau', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3159', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('72', 'MacBook Pro', 'Space Grau', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3639', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('73', 'MacBook Pro', 'Space Grau', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4599', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('74', 'MacBook Pro', 'Space Grau', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '6999', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('75', 'MacBook Pro', 'Space Grau', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3279', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('76', 'MacBook Pro', 'Space Grau', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3519', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('77', 'MacBook Pro', 'Space Grau', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3999', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('78', 'MacBook Pro', 'Space Grau', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4959', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('79', 'MacBook Pro', 'Space Grau', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '7359', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('80', 'MacBook Pro', 'Space Grau', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3399', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('81', 'MacBook Pro', 'Space Grau', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3639', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('82', 'MacBook Pro', 'Space Grau', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4119', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('83', 'MacBook Pro', 'Space Grau', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '5079', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('84', 'MacBook Pro', 'Space Grau', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '7479', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('85', 'MacBook Pro', 'Space Grau', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3279', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('86', 'MacBook Pro', 'Space Grau', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3519', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('87', 'MacBook Pro', 'Space Grau', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3999', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('88', 'MacBook Pro', 'Space Grau', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4959', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('89', 'MacBook Pro', 'Space Grau', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '7359', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('90', 'MacBook Pro', 'Space Grau', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3399', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('91', 'MacBook Pro', 'Space Grau', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3639', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('92', 'MacBook Pro', 'Space Grau', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4119', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('93', 'MacBook Pro', 'Space Grau', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '5079', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('94', 'MacBook Pro', 'Space Grau', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '7479', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('95', 'MacBook Pro', 'Space Grau', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3759', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('96', 'MacBook Pro', 'Space Grau', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3999', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('97', 'MacBook Pro', 'Space Grau', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4479', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('98', 'MacBook Pro', 'Space Grau', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '5439', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('99', 'MacBook Pro', 'Space Grau', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '7839', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('100', 'MacBook Pro', 'Space Grau', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3879', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('101', 'MacBook Pro', 'Space Grau', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4119', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('102', 'MacBook Pro', 'Space Grau', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4599', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('103', 'MacBook Pro', 'Space Grau', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '5559', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('104', 'MacBook Pro', 'Space Grau', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '7959', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-space.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('105', 'MacBook Pro', 'Silber', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '2799', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('106', 'MacBook Pro', 'Silber', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3039', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('107', 'MacBook Pro', 'Silber', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3519', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('108', 'MacBook Pro', 'Silber', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4479', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('109', 'MacBook Pro', 'Silber', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '6879', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('110', 'MacBook Pro', 'Silber', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '2919', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('111', 'MacBook Pro', 'Silber', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3159', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('112', 'MacBook Pro', 'Silber', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3639', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('113', 'MacBook Pro', 'Silber', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4599', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('114', 'MacBook Pro', 'Silber', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '6999', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('115', 'MacBook Pro', 'Silber', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3279', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('116', 'MacBook Pro', 'Silber', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3519', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('117', 'MacBook Pro', 'Silber', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3999', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('118', 'MacBook Pro', 'Silber', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4959', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('119', 'MacBook Pro', 'Silber', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '7359', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('120', 'MacBook Pro', 'Silber', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3399', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('121', 'MacBook Pro', 'Silber', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3639', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('122', 'MacBook Pro', 'Silber', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4119', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('123', 'MacBook Pro', 'Silber', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '5079', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('124', 'MacBook Pro', 'Silber', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '7479', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('125', 'MacBook Pro', 'Silber', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3279', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('126', 'MacBook Pro', 'Silber', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3519', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('127', 'MacBook Pro', 'Silber', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3999', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('128', 'MacBook Pro', 'Silber', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4959', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('129', 'MacBook Pro', 'Silber', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '7359', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('130', 'MacBook Pro', 'Silber', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3399', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('131', 'MacBook Pro', 'Silber', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3639', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('132', 'MacBook Pro', 'Silber', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4119', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('133', 'MacBook Pro', 'Silber', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '5079', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('134', 'MacBook Pro', 'Silber', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '7479', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('135', 'MacBook Pro', 'Silber', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3759', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('136', 'MacBook Pro', 'Silber', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3999', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('137', 'MacBook Pro', 'Silber', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4479', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('138', 'MacBook Pro', 'Silber', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '5439', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('139', 'MacBook Pro', 'Silber', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '7839', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('140', 'MacBook Pro', 'Silber', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3879', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('141', 'MacBook Pro', 'Silber', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4119', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('142', 'MacBook Pro', 'Silber', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4599', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('143', 'MacBook Pro', 'Silber', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '5559', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('144', 'MacBook Pro', 'Silber', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '7959', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/mbp15touch-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('145', 'MacBook Air', 'Space Grau', 'MBA13RD', '128 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '8 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '1349', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-air-space-gray.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('146', 'MacBook Air', 'Space Grau', 'MBA13RD', '256 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '8 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '1599', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-air-space-gray.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('147', 'MacBook Air', 'Space Grau', 'MBA13RD', '512 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '8 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '1849', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-air-space-gray.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('148', 'MacBook Air', 'Space Grau', 'MBA13RD', '1.5 TB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '8 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '2849', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-air-space-gray.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('149', 'MacBook Air', 'Space Grau', 'MBA13RD', '128 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '16 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '1589', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-air-space-gray.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('150', 'MacBook Air', 'Space Grau', 'MBA13RD', '256 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '16 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '1839', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-air-space-gray.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('151', 'MacBook Air', 'Space Grau', 'MBA13RD', '512 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '16 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '2089', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-air-space-gray.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('152', 'MacBook Air', 'Space Grau', 'MBA13RD', '1.5 TB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '16 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '3089', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-air-space-gray.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('153', 'MacBook Air', 'Gold', 'MBA13RD', '128 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '8 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '1349', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-air-gold.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('154', 'MacBook Air', 'Gold', 'MBA13RD', '256 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '8 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '1599', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-air-gold.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('155', 'MacBook Air', 'Gold', 'MBA13RD', '512 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '8 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '1849', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-air-gold.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('156', 'MacBook Air', 'Gold', 'MBA13RD', '1.5 TB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '8 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '2849', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-air-gold.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('157', 'MacBook Air', 'Gold', 'MBA13RD', '128 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '16 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '1589', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-air-gold.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('158', 'MacBook Air', 'Gold', 'MBA13RD', '256 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '16 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '1839', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-air-gold.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('159', 'MacBook Air', 'Gold', 'MBA13RD', '512 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '16 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '2089', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-air-gold.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('160', 'MacBook Air', 'Gold', 'MBA13RD', '1.5 TB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '16 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '3089', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-air-gold.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('161', 'MacBook Air', 'Silber', 'MBA13RD', '128 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '8 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '1349', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-air-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('162', 'MacBook Air', 'Silber', 'MBA13RD', '256 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '8 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '1599', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-air-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('163', 'MacBook Air', 'Silber', 'MBA13RD', '512 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '8 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '1849', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-air-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('164', 'MacBook Air', 'Silber', 'MBA13RD', '1.5 TB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '8 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '2849', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-air-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('165', 'MacBook Air', 'Silber', 'MBA13RD', '128 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '16 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '1589', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-air-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('166', 'MacBook Air', 'Silber', 'MBA13RD', '256 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '16 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '1839', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-air-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('167', 'MacBook Air', 'Silber', 'MBA13RD', '512 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '16 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '2089', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-air-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('168', 'MacBook Air', 'Silber', 'MBA13RD', '1.5 TB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '16 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '3089', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-air-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('169', 'MacBook Air', 'Silber', 'MBA13', '128 GB', 'MBA13', '1.35 KG', 'MBA13_1', '8 GB', 'Intel HD Graphics 6000', 'MBA13', 'MBA13', 'MBA13', 'MBA13', '1099', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-air.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('170', 'MacBook Air', 'Silber', 'MBA13', '256 GB', 'MBA13', '1.35 KG', 'MBA13_1', '8 GB', 'Intel HD Graphics 6000', 'MBA13', 'MBA13', 'MBA13', 'MBA13', '1349', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-air.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('171', 'MacBook Air', 'Silber', 'MBA13', '512 GB', 'MBA13', '1.35 KG', 'MBA13_1', '8 GB', 'Intel HD Graphics 6000', 'MBA13', 'MBA13', 'MBA13', 'MBA13', '1599', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-air.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('172', 'MacBook Air', 'Silber', 'MBA13', '128 GB', 'MBA13', '1.35 KG', 'MBA13_2', '8 GB', 'Intel HD Graphics 6000', 'MBA13', 'MBA13', 'MBA13', 'MBA13', '1279', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-air.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('173', 'MacBook Air', 'Silber', 'MBA13', '256 GB', 'MBA13', '1.35 KG', 'MBA13_2', '8 GB', 'Intel HD Graphics 6000', 'MBA13', 'MBA13', 'MBA13', 'MBA13', '1529', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-air.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('174', 'MacBook Air', 'Silber', 'MBA13', '512 GB', 'MBA13', '1.35 KG', 'MBA13_2', '8 GB', 'Intel HD Graphics 6000', 'MBA13', 'MBA13', 'MBA13', 'MBA13', '1779', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-air.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('175', 'MacBook', 'Silber', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_1', '8 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1499', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('176', 'MacBook', 'Silber', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_2', '8 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1619', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('177', 'MacBook', 'Silber', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_3', '8 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1799', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('178', 'MacBook', 'Silber', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_1', '16 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1739', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('179', 'MacBook', 'Silber', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_2', '16 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1859', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('180', 'MacBook', 'Silber', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_3', '16 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '2039', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('181', 'MacBook', 'Silber', 'MB12', '512 GB', 'MB12', '0.92 KG', 'MB12_2', '8 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1799', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('182', 'MacBook', 'Silber', 'MB12', '512 GB', 'MB12', '0.92 KG', 'MB12_3', '8 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1979', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('183', 'MacBook', 'Silber', 'MB12', '512 GB', 'MB12', '0.92 KG', 'MB12_2', '16 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '2039', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('184', 'MacBook', 'Silber', 'MB12', '512 GB', 'MB12', '0.92 KG', 'MB12_3', '16 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '2219', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-silver.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('185', 'MacBook', 'Gold', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_1', '8 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1499', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-gold.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('186', 'MacBook', 'Gold', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_2', '8 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1619', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-gold.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('187', 'MacBook', 'Gold', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_3', '8 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1799', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-gold.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('188', 'MacBook', 'Gold', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_1', '16 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1739', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-gold.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('189', 'MacBook', 'Gold', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_2', '16 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1859', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-gold.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('190', 'MacBook', 'Gold', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_3', '16 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '2039', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-gold.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('191', 'MacBook', 'Gold', 'MB12', '512 GB', 'MB12', '0.92 KG', 'MB12_2', '8 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1799', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-gold.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('192', 'MacBook', 'Gold', 'MB12', '512 GB', 'MB12', '0.92 KG', 'MB12_3', '8 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1979', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-gold.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('193', 'MacBook', 'Gold', 'MB12', '512 GB', 'MB12', '0.92 KG', 'MB12_2', '16 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '2039', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-gold.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('194', 'MacBook', 'Gold', 'MB12', '512 GB', 'MB12', '0.92 KG', 'MB12_3', '16 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '2219', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-gold.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('195', 'MacBook', 'Space Grau', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_1', '8 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1499', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-space-gray.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('196', 'MacBook', 'Space Grau', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_2', '8 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1619', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-space-gray.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('197', 'MacBook', 'Space Grau', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_3', '8 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1799', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-space-gray.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('198', 'MacBook', 'Space Grau', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_1', '16 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1739', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-space-gray.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('199', 'MacBook', 'Space Grau', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_2', '16 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1859', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-space-gray.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('200', 'MacBook', 'Space Grau', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_3', '16 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '2039', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-space-gray.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('201', 'MacBook', 'Space Grau', 'MB12', '512 GB', 'MB12', '0.92 KG', 'MB12_2', '8 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1799', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-space-gray.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('202', 'MacBook', 'Space Grau', 'MB12', '512 GB', 'MB12', '0.92 KG', 'MB12_3', '8 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1979', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-space-gray.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('203', 'MacBook', 'Space Grau', 'MB12', '512 GB', 'MB12', '0.92 KG', 'MB12_2', '16 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '2039', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-space-gray.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('204', 'MacBook', 'Space Grau', 'MB12', '512 GB', 'MB12', '0.92 KG', 'MB12_3', '16 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '2219', 'Laptop', 'https://bscchatbotblob.blob.core.windows.net/images/macbook-space-gray.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('205', 'iMac', 'Silber', 'iM21_1', '256 GB', 'iM21', '5.66 KG', 'iM21_1', '8 GB', 'Intel Iris Plus Graphics 640', 'iM21', 'iM_1', 'iM21', 'wire', '1539', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-215.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('206', 'iMac', 'Silber', 'iM21_1', '256 GB', 'iM21', '5.66 KG', 'iM21_1', '8 GB', 'Intel Iris Plus Graphics 640', 'iM21', 'iM_2', 'iM21', 'wire', '1603', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-215.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('207', 'iMac', 'Silber', 'iM21_1', '256 GB', 'iM21', '5.66 KG', 'iM21_1', '8 GB', 'Intel Iris Plus Graphics 640', 'iM21', 'iM_3', 'iM21', 'wire', '1688', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-215.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('208', 'iMac', 'Silber', 'iM21_1', '256 GB', 'iM21', '5.66 KG', 'iM21_1', '16 GB', 'Intel Iris Plus Graphics 640', 'iM21', 'iM_1', 'iM21', 'wire', '1779', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-215.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('209', 'iMac', 'Silber', 'iM21_1', '256 GB', 'iM21', '5.66 KG', 'iM21_1', '16 GB', 'Intel Iris Plus Graphics 640', 'iM21', 'iM_2', 'iM21', 'wire', '1843', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-215.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('210', 'iMac', 'Silber', 'iM21_1', '256 GB', 'iM21', '5.66 KG', 'iM21_1', '16 GB', 'Intel Iris Plus Graphics 640', 'iM21', 'iM_3', 'iM21', 'wire', '1928', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-215.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('211', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_2', '8 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '1739', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('212', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_2', '8 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '1803', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('213', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_2', '8 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '1888', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('214', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_2', '16 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '1979', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('215', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_2', '16 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2043', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('216', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_2', '16 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2128', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('217', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_3', '8 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2099', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('218', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_3', '8 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2163', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('219', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_3', '8 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2248', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('220', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_3', '16 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2339', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('221', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_3', '16 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2403', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('222', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_3', '16 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2488', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('223', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_2', '8 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '1979', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('224', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_2', '8 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2043', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('225', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_2', '8 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2128', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
GO
INSERT INTO [dbo].[Mac] VALUES ('226', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_2', '16 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2219', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('227', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_2', '16 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2283', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('228', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_2', '16 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2368', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('229', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_3', '8 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2339', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('230', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_3', '8 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2403', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('231', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_3', '8 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2488', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('232', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_3', '16 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2579', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('233', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_3', '16 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2643', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('234', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_3', '16 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2728', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('235', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_4', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '1819', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('236', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_4', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '1883', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('237', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_4', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '1968', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('238', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_3', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2059', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('239', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_3', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2123', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('240', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_3', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2208', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('241', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_4', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2059', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('242', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_4', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2123', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('243', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_4', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2208', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('244', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_3', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2299', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('245', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_3', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2363', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('246', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_3', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2448', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('247', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_4', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2539', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('248', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_4', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2603', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('249', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_4', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2688', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('250', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_3', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2779', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('251', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_3', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2843', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('252', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_3', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2928', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('253', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_4', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2059', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('254', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_4', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2123', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('255', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_4', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2208', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('256', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_3', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2299', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('257', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_3', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2363', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('258', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_3', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2448', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('259', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_4', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2299', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('260', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_4', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2363', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('261', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_4', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2448', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('262', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_3', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2539', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('263', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_3', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2603', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('264', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_3', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2688', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('265', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_4', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2779', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('266', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_4', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2843', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('267', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_4', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2928', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('268', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_3', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '3019', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('269', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_3', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '3083', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('270', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_3', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '3168', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('271', 'iMac', 'Silber', 'iM21_2', '1 TB', 'iM21', '5.66 KG', 'iM21_4', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2539', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('272', 'iMac', 'Silber', 'iM21_2', '1 TB', 'iM21', '5.66 KG', 'iM21_4', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2603', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('273', 'iMac', 'Silber', 'iM21_2', '1 TB', 'iM21', '5.66 KG', 'iM21_4', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2688', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('274', 'iMac', 'Silber', 'iM21_2', '1 TB', 'iM21', '5.66 KG', 'iM21_3', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2779', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('275', 'iMac', 'Silber', 'iM21_2', '1 TB', 'iM21', '5.66 KG', 'iM21_3', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2843', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('276', 'iMac', 'Silber', 'iM21_2', '1 TB', 'iM21', '5.66 KG', 'iM21_3', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2928', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('277', 'iMac', 'Silber', 'iM21_2', '1 TB', 'iM21', '5.66 KG', 'iM21_4', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2779', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('278', 'iMac', 'Silber', 'iM21_2', '1 TB', 'iM21', '5.66 KG', 'iM21_4', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2843', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('279', 'iMac', 'Silber', 'iM21_2', '1 TB', 'iM21', '5.66 KG', 'iM21_4', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2928', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('280', 'iMac', 'Silber', 'iM21_2', '1 TB', 'iM21', '5.66 KG', 'iM21_3', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '3019', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('281', 'iMac', 'Silber', 'iM21_2', '1 TB', 'iM21', '5.66 KG', 'iM21_3', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '3083', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('282', 'iMac', 'Silber', 'iM21_2', '1 TB', 'iM21', '5.66 KG', 'iM21_3', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '3168', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('283', 'iMac', 'Silber', 'iM21_2', '1 TB', 'iM21', '5.66 KG', 'iM21_4', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '3259', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('284', 'iMac', 'Silber', 'iM21_2', '1 TB', 'iM21', '5.66 KG', 'iM21_4', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '3323', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('285', 'iMac', 'Silber', 'iM21_2', '1 TB', 'iM21', '5.66 KG', 'iM21_4', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '3408', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('286', 'iMac', 'Silber', 'iM21_2', '1 TB', 'iM21', '5.66 KG', 'iM21_3', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '3499', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('287', 'iMac', 'Silber', 'iM21_2', '1 TB', 'iM21', '5.66 KG', 'iM21_3', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '3563', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('288', 'iMac', 'Silber', 'iM21_2', '1 TB', 'iM21', '5.66 KG', 'iM21_3', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '3648', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-4k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('289', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_1', '8 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '2219', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('290', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_1', '8 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '2283', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('291', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_1', '8 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '2368', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('292', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_1', '16 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '2459', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('293', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_1', '16 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '2523', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('294', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_1', '16 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '2608', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('295', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_1', '32 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '2939', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('296', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_1', '32 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3003', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('297', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_1', '32 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3088', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('298', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_1', '8 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '2459', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('299', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_1', '8 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '2523', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('300', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_1', '8 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '2608', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('301', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_1', '16 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '2699', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('302', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_1', '16 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '2763', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('303', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_1', '16 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '2848', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('304', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_1', '32 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3179', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('305', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_1', '32 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3243', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('306', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_1', '32 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3328', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('307', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_1', '8 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '2939', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('308', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_1', '8 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3003', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('309', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_1', '8 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3088', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('310', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_1', '16 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3179', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('311', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_1', '16 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3243', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('312', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_1', '16 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3328', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('313', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_1', '32 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3659', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('314', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_1', '32 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3723', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('315', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_1', '32 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3808', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('316', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_2', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '2419', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('317', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_2', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '2489', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('318', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_2', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '2568', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('319', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_2', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '2659', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('320', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_2', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '2729', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('321', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_2', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '2808', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('322', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_2', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3139', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('323', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_2', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3209', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('324', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_2', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3288', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('325', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_2', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '2659', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('326', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_2', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '2729', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('327', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_2', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '2808', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('328', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_2', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '2899', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('329', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_2', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '2969', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('330', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_2', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3048', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('331', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_2', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3379', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('332', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_2', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3449', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('333', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_2', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3528', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('334', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_2', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3139', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('335', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_2', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3209', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('336', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_2', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3288', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('337', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_2', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3379', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('338', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_2', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3449', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('339', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_2', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3528', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('340', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_2', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3859', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('341', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_2', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3929', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('342', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_2', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '4008', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('343', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_2', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4099', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('344', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_2', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '4169', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('345', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_2', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '4248', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('346', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_2', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4339', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('347', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_2', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '4409', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('348', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_2', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '4488', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('349', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_2', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4819', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('350', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_2', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '4889', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('351', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_2', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '4968', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('352', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '2779', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('353', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '2849', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('354', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '2928', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('355', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3019', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('356', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3089', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('357', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3168', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('358', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3499', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('359', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3569', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('360', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3648', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('361', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3019', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('362', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3089', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('363', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3168', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('364', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3259', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('365', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3329', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('366', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3408', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('367', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3739', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('368', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3809', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('369', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3888', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('370', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3499', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('371', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3569', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('372', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3648', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('373', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3739', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('374', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3809', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('375', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3888', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('376', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4219', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('377', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '4289', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('378', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '4368', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('379', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4459', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('380', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '4529', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('381', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '4608', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('382', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4699', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('383', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '4769', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('384', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '4848', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('385', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '5179', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('386', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '5249', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('387', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '5328', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('388', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_3', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '2839', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('389', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_3', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '2903', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('390', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_3', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '2988', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('391', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_3', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3319', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('392', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_3', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3383', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('393', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_3', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3468', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('394', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_3', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4279', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('395', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_3', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '4343', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('396', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_3', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '4428', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('397', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_3', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3079', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('398', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_3', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3143', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('399', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_3', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3228', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('400', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_3', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3559', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('401', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_3', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3623', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('402', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_3', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3708', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('403', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_3', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4519', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('404', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_3', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '4583', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('405', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_3', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '4668', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('406', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_3', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3559', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('407', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_3', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3623', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('408', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_3', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3708', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('409', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_3', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4039', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('410', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_3', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '4103', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('411', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_3', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '4188', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('412', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_3', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4999', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('413', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_3', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '5063', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('414', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_3', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '5148', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('415', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_3', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4519', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('416', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_3', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '4583', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('417', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_3', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '4668', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('418', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_3', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4999', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('419', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_3', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '5063', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('420', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_3', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '5148', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('421', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_3', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '5959', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('422', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_3', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '6023', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('423', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_3', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '6108', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('424', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3079', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('425', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3143', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('426', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3228', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('427', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3559', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('428', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3623', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('429', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3708', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('430', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4519', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('431', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '4583', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('432', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '4668', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('433', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3319', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('434', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3383', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('435', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3468', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('436', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3799', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('437', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3863', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('438', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3948', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('439', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4759', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('440', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '4823', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('441', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '4908', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('442', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3799', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('443', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3863', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('444', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3948', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('445', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4279', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('446', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '4343', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('447', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '4428', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('448', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '5239', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('449', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '5303', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('450', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '5388', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('451', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4759', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('452', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '4823', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('453', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '4908', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('454', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '5239', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('455', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '5303', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('456', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '5388', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('457', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '6199', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('458', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '6263', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('459', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '6348', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imac-5k.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('460', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '5499', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('461', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '5549', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('462', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '5648', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
GO
INSERT INTO [dbo].[Mac] VALUES ('463', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '6459', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('464', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '6509', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('465', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '6608', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('466', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '8859', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('467', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '8909', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('468', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '9008', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('469', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '6459', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('470', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '6509', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('471', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '6608', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('472', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '7419', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('473', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '7469', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('474', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '7568', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('475', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '9819', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('476', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '9869', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('477', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '9968', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('478', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '7419', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('479', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '7469', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('480', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '7568', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('481', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '8379', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('482', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '8429', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('483', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '8528', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('484', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '10779', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('485', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '10829', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('486', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '10928', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('487', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '8379', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('488', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '8429', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('489', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '8528', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('490', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '9339', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('491', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '9389', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('492', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '9488', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('493', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '11739', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('494', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '11789', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('495', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '11888', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('496', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '6459', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('497', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '6509', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('498', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '6608', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('499', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '7419', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('500', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '7469', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('501', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '7568', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('502', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '9819', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('503', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '9869', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('504', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '9968', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('505', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '7419', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('506', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '7469', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('507', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '7568', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('508', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '8379', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('509', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '8429', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('510', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '8528', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('511', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '10779', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('512', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '10829', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('513', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '10928', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('514', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '8379', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('515', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '8429', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('516', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '8528', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('517', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '9339', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('518', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '9389', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('519', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '9488', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('520', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '11739', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('521', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '11789', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('522', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '11888', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('523', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '9339', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('524', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '9389', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('525', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '9488', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('526', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '10299', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('527', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '10349', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('528', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '10448', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('529', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '12699', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('530', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '12749', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('531', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '12848', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('532', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '8379', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('533', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '8429', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('534', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '8528', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('535', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '9339', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('536', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '9389', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('537', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '9488', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('538', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '11739', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('539', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '11789', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('540', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '11888', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('541', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '9339', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('542', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '9389', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('543', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '9488', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('544', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '10299', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('545', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '10349', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('546', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '10448', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('547', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '12699', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('548', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '12749', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('549', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '12848', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('550', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '10299', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('551', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '10349', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('552', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '10448', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('553', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '11259', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('554', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '11309', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('555', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '11408', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('556', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '13659', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('557', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '13709', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('558', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '13808', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('559', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '11259', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('560', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '11309', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('561', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '11408', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('562', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '12219', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('563', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '12269', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('564', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '12368', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('565', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '14619', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('566', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '14669', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('567', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '14768', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('568', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '6219', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('569', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '6269', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('570', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '6368', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('571', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '7179', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('572', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '7229', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('573', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '7328', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('574', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '9579', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('575', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '9629', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('576', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '9728', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('577', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '7179', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('578', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '7229', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('579', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '7328', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('580', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '8139', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('581', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '8189', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('582', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '8288', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('583', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '10539', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('584', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '10589', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('585', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '10688', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('586', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '8139', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('587', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '8189', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('588', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '8288', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('589', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '9099', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('590', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '9149', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('591', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '9248', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('592', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '11499', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('593', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '11549', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('594', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '11648', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('595', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '9099', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('596', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '9149', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('597', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '9248', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('598', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '10059', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('599', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '10109', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('600', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '10208', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('601', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '12459', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('602', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '12509', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('603', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '12608', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('604', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '7179', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('605', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '7229', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('606', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '7328', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('607', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '8139', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('608', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '8189', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('609', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '8288', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('610', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '10539', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('611', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '10589', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('612', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '10688', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('613', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '8139', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('614', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '8189', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('615', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '8288', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('616', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '9099', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('617', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '9149', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('618', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '9248', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('619', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '11499', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('620', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '11549', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('621', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '11648', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('622', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '9099', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('623', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '9149', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('624', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '9248', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('625', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '10059', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('626', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '10109', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('627', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '10208', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('628', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '12459', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('629', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '12509', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('630', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '12608', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('631', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '10059', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('632', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '10109', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('633', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '10208', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('634', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '11019', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('635', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '11069', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('636', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '11168', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('637', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '13419', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('638', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '13469', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('639', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '13568', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('640', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '9099', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('641', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '9149', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('642', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '9248', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('643', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '10059', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('644', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '10109', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('645', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '10208', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('646', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '12459', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('647', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '12509', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('648', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '12608', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('649', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '10059', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('650', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '10109', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('651', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '10208', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('652', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '11019', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('653', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '11069', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('654', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '11168', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('655', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '13419', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('656', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '13469', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('657', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '13568', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('658', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '11019', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('659', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '11069', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('660', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '11168', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('661', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '11979', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('662', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '12029', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('663', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '12128', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('664', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '14379', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('665', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '14429', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('666', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '14528', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('667', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '11979', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('668', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '12029', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('669', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '12128', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('670', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '12939', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('671', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '12989', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('672', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '13088', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('673', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '15339', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('674', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '15389', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('675', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '15488', 'All-in-one', 'https://bscchatbotblob.blob.core.windows.net/images/imacpro-27-retina.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('676', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_1', '16 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '3399', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('677', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_1', '16 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '3639', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('678', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_1', '16 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '4119', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('679', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_1', '32 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '3879', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('680', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_1', '32 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '4119', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('681', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_1', '32 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '4599', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('682', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_1', '64 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '4839', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('683', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_1', '64 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '5079', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
GO
INSERT INTO [dbo].[Mac] VALUES ('684', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_1', '64 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '5559', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('685', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_2', '16 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '4359', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('686', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_2', '16 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '4599', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('687', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_2', '16 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '5079', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('688', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_2', '32 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '4839', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('689', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_2', '32 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '5079', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('690', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_2', '32 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '5559', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('691', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_2', '64 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '5799', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('692', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_2', '64 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '6039', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('693', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_2', '64 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '6519', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('694', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_3', '16 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '5799', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('695', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_3', '16 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '6039', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('696', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_3', '16 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '6519', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('697', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_3', '32 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '6279', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('698', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_3', '32 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '6519', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('699', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_3', '32 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '6999', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('700', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_3', '64 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '7239', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('701', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_3', '64 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '7479', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('702', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_3', '64 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '7959', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('703', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_1', '16 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '3639', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('704', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_1', '16 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '3879', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('705', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_1', '16 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '4359', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('706', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_1', '32 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '4119', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('707', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_1', '32 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '4359', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('708', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_1', '32 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '4839', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('709', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_1', '64 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '5079', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('710', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_1', '64 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '5319', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('711', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_1', '64 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '5799', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('712', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_2', '16 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '4599', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('713', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_2', '16 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '4839', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('714', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_2', '16 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '5319', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('715', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_2', '32 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '5079', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('716', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_2', '32 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '5319', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('717', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_2', '32 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '5799', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('718', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_2', '64 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '6039', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('719', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_2', '64 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '6279', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('720', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_2', '64 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '6759', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('721', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_3', '16 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '6039', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('722', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_3', '16 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '6279', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('723', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_3', '16 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '6759', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('724', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_3', '32 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '6519', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('725', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_3', '32 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '6759', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('726', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_3', '32 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '7239', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('727', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_3', '64 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '7479', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('728', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_3', '64 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '7719', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('729', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_3', '64 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '8199', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-pro.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('730', 'Mac Mini', 'Space Grau', 'no', '128 GB', 'MM', '1.3 KG', 'MM_1', '8 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '899', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('731', 'Mac Mini', 'Space Grau', 'no', '256 GB', 'MM', '1.3 KG', 'MM_1', '8 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1139', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('732', 'Mac Mini', 'Space Grau', 'no', '512 GB', 'MM', '1.3 KG', 'MM_1', '8 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1379', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('733', 'Mac Mini', 'Space Grau', 'no', '1 TB', 'MM', '1.3 KG', 'MM_1', '8 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1859', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('734', 'Mac Mini', 'Space Grau', 'no', '2 TB', 'MM', '1.3 KG', 'MM_1', '8 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2819', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('735', 'Mac Mini', 'Space Grau', 'no', '128 GB', 'MM', '1.3 KG', 'MM_1', '16 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1139', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('736', 'Mac Mini', 'Space Grau', 'no', '256 GB', 'MM', '1.3 KG', 'MM_1', '16 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1379', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('737', 'Mac Mini', 'Space Grau', 'no', '512 GB', 'MM', '1.3 KG', 'MM_1', '16 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1619', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('738', 'Mac Mini', 'Space Grau', 'no', '1 TB', 'MM', '1.3 KG', 'MM_1', '16 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2099', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('739', 'Mac Mini', 'Space Grau', 'no', '2 TB', 'MM', '1.3 KG', 'MM_1', '16 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '3059', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('740', 'Mac Mini', 'Space Grau', 'no', '128 GB', 'MM', '1.3 KG', 'MM_1', '32 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1619', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('741', 'Mac Mini', 'Space Grau', 'no', '256 GB', 'MM', '1.3 KG', 'MM_1', '32 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1859', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('742', 'Mac Mini', 'Space Grau', 'no', '512 GB', 'MM', '1.3 KG', 'MM_1', '32 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2099', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('743', 'Mac Mini', 'Space Grau', 'no', '1 TB', 'MM', '1.3 KG', 'MM_1', '32 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2579', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('744', 'Mac Mini', 'Space Grau', 'no', '2 TB', 'MM', '1.3 KG', 'MM_1', '32 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '3539', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('745', 'Mac Mini', 'Space Grau', 'no', '128 GB', 'MM', '1.3 KG', 'MM_1', '64 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2579', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('746', 'Mac Mini', 'Space Grau', 'no', '256 GB', 'MM', '1.3 KG', 'MM_1', '64 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2819', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('747', 'Mac Mini', 'Space Grau', 'no', '512 GB', 'MM', '1.3 KG', 'MM_1', '64 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '3059', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('748', 'Mac Mini', 'Space Grau', 'no', '1 TB', 'MM', '1.3 KG', 'MM_1', '64 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '3539', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('749', 'Mac Mini', 'Space Grau', 'no', '2 TB', 'MM', '1.3 KG', 'MM_1', '64 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '4499', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('750', 'Mac Mini', 'Space Grau', 'no', '128 GB', 'MM', '1.3 KG', 'MM_2', '8 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1249', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('751', 'Mac Mini', 'Space Grau', 'no', '256 GB', 'MM', '1.3 KG', 'MM_2', '8 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1489', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('752', 'Mac Mini', 'Space Grau', 'no', '512 GB', 'MM', '1.3 KG', 'MM_2', '8 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1729', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('753', 'Mac Mini', 'Space Grau', 'no', '1 TB', 'MM', '1.3 KG', 'MM_2', '8 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2209', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('754', 'Mac Mini', 'Space Grau', 'no', '2 TB', 'MM', '1.3 KG', 'MM_2', '8 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '3169', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('755', 'Mac Mini', 'Space Grau', 'no', '128 GB', 'MM', '1.3 KG', 'MM_2', '16 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1489', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('756', 'Mac Mini', 'Space Grau', 'no', '256 GB', 'MM', '1.3 KG', 'MM_2', '16 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1729', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('757', 'Mac Mini', 'Space Grau', 'no', '512 GB', 'MM', '1.3 KG', 'MM_2', '16 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1969', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('758', 'Mac Mini', 'Space Grau', 'no', '1 TB', 'MM', '1.3 KG', 'MM_2', '16 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2449', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('759', 'Mac Mini', 'Space Grau', 'no', '2 TB', 'MM', '1.3 KG', 'MM_2', '16 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '3409', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('760', 'Mac Mini', 'Space Grau', 'no', '128 GB', 'MM', '1.3 KG', 'MM_2', '32 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1969', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('761', 'Mac Mini', 'Space Grau', 'no', '256 GB', 'MM', '1.3 KG', 'MM_2', '32 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2209', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('762', 'Mac Mini', 'Space Grau', 'no', '512 GB', 'MM', '1.3 KG', 'MM_2', '32 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2449', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('763', 'Mac Mini', 'Space Grau', 'no', '1 TB', 'MM', '1.3 KG', 'MM_2', '32 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2929', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('764', 'Mac Mini', 'Space Grau', 'no', '2 TB', 'MM', '1.3 KG', 'MM_2', '32 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '3889', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('765', 'Mac Mini', 'Space Grau', 'no', '128 GB', 'MM', '1.3 KG', 'MM_2', '64 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2929', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('766', 'Mac Mini', 'Space Grau', 'no', '256 GB', 'MM', '1.3 KG', 'MM_2', '64 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '3169', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('767', 'Mac Mini', 'Space Grau', 'no', '512 GB', 'MM', '1.3 KG', 'MM_2', '64 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '3409', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('768', 'Mac Mini', 'Space Grau', 'no', '1 TB', 'MM', '1.3 KG', 'MM_2', '64 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '3889', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('769', 'Mac Mini', 'Space Grau', 'no', '2 TB', 'MM', '1.3 KG', 'MM_2', '64 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '4849', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('770', 'Mac Mini', 'Space Grau', 'no', '256 GB', 'MM', '1.3 KG', 'MM_3', '8 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1249', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('771', 'Mac Mini', 'Space Grau', 'no', '512 GB', 'MM', '1.3 KG', 'MM_3', '8 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1489', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('772', 'Mac Mini', 'Space Grau', 'no', '1 TB', 'MM', '1.3 KG', 'MM_3', '8 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1969', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('773', 'Mac Mini', 'Space Grau', 'no', '2 TB', 'MM', '1.3 KG', 'MM_3', '8 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2929', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('774', 'Mac Mini', 'Space Grau', 'no', '256 GB', 'MM', '1.3 KG', 'MM_3', '16 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1489', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('775', 'Mac Mini', 'Space Grau', 'no', '512 GB', 'MM', '1.3 KG', 'MM_3', '16 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1729', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('776', 'Mac Mini', 'Space Grau', 'no', '1 TB', 'MM', '1.3 KG', 'MM_3', '16 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2209', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('777', 'Mac Mini', 'Space Grau', 'no', '2 TB', 'MM', '1.3 KG', 'MM_3', '16 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '3169', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('778', 'Mac Mini', 'Space Grau', 'no', '256 GB', 'MM', '1.3 KG', 'MM_3', '32 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1969', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('779', 'Mac Mini', 'Space Grau', 'no', '512 GB', 'MM', '1.3 KG', 'MM_3', '32 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2209', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('780', 'Mac Mini', 'Space Grau', 'no', '1 TB', 'MM', '1.3 KG', 'MM_3', '32 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2689', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('781', 'Mac Mini', 'Space Grau', 'no', '2 TB', 'MM', '1.3 KG', 'MM_3', '32 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '3649', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('782', 'Mac Mini', 'Space Grau', 'no', '256 GB', 'MM', '1.3 KG', 'MM_3', '64 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2929', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('783', 'Mac Mini', 'Space Grau', 'no', '512 GB', 'MM', '1.3 KG', 'MM_3', '64 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '3169', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('784', 'Mac Mini', 'Space Grau', 'no', '1 TB', 'MM', '1.3 KG', 'MM_3', '64 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '3649', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
INSERT INTO [dbo].[Mac] VALUES ('785', 'Mac Mini', 'Space Grau', 'no', '2 TB', 'MM', '1.3 KG', 'MM_3', '64 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '4609', 'Desktop', 'https://bscchatbotblob.blob.core.windows.net/images/mac-mini.jpeg');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[Mac_Display]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[Mac_Display]') AND type IN ('U'))
	DROP TABLE [dbo].[Mac_Display]
GO
CREATE TABLE [dbo].[Mac_Display] (
	[ID] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[name] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[inch] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LED-backlight] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IPS] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[resolution] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TrueTone] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ppi] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__Mac_Disp__3214EC277E0DBBD8] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[Mac_Display]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[Mac_Display] VALUES ('iM21_1', 'Display', '21,5', 'ja', 'nein', '1920 x 1080 ', 'nein', 'n.a');
INSERT INTO [dbo].[Mac_Display] VALUES ('iM21_2', 'Retina 4K Display', '21,5', 'nein', 'nein', '4096 x 2304', 'nein', 'n.a');
INSERT INTO [dbo].[Mac_Display] VALUES ('iM27', 'Retina Display', '27', 'nein', 'nein', '5120 x 2880', 'nein', 'n.a');
INSERT INTO [dbo].[Mac_Display] VALUES ('iMP', 'Retina Display', '27', 'no', 'no', '5120 x 2880', 'no', 'n.a');
INSERT INTO [dbo].[Mac_Display] VALUES ('MB12', 'Retina Display', '12', 'ja', 'ja', '2304 x 1440, 1440 x 900, 1280 x 800 und 1024 x 640', 'nein', '226');
INSERT INTO [dbo].[Mac_Display] VALUES ('MBA13', 'Widescreen Display', '13,3', 'ja', 'nein', '1440 x 900 (nativ), 1280 x 800, 1152 x 720 und 1024 x 640 Pixel bei einem Seitenverhältnis von 16:10 und 1024 x 768 und 800 x 600 Pixel bei einem Seitenverhältnis von 4:3', 'nein', 'n.a');
INSERT INTO [dbo].[Mac_Display] VALUES ('MBA13RD', 'Retina Display', '13,3', 'ja', 'ja', '2560 x 1600 , 1680 x 1050, 1440 x 900 und 1024 x 640', 'nein', '227');
INSERT INTO [dbo].[Mac_Display] VALUES ('MBP13', 'Retina Display', '13,3', 'ja', 'ja', '2560 x 1600 , 1680 x 1050, 1440 x 900 und 1024 x 640', 'nein', '227');
INSERT INTO [dbo].[Mac_Display] VALUES ('MBP13TB', 'Retina Display', '13,3', 'ja', 'ja', '2560 x 1600 , 1680 x 1050, 1440 x 900 und 1024 x 640', 'ja', '227');
INSERT INTO [dbo].[Mac_Display] VALUES ('MBP15', 'Retina Display', '15,4', 'ja', 'ja', '2880 x 1800, 1920 x 1200, 1680 x 1050, 1280 x 800 und 1024 x 640', 'ja', '220');
INSERT INTO [dbo].[Mac_Display] VALUES ('no', 'no', 'no', 'no', 'no', 'no', 'no', 'no');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[Mac_Size]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[Mac_Size]') AND type IN ('U'))
	DROP TABLE [dbo].[Mac_Size]
GO
CREATE TABLE [dbo].[Mac_Size] (
	[ID] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[height] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[width] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[depth] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__Mac_Size__3214EC27BF8623F3] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[Mac_Size]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[Mac_Size] VALUES ('iM21', '45,0 cm', '52,8 cm', '17,5 cm');
INSERT INTO [dbo].[Mac_Size] VALUES ('iM27', '51,6 cm', '65,0 cm', '20,3 cm');
INSERT INTO [dbo].[Mac_Size] VALUES ('iMP', '51,6 cm', '65,0 cm', '20,3 cm');
INSERT INTO [dbo].[Mac_Size] VALUES ('MB12', '0,35 bis 1,31 cm', '28,05 cm', '19,65 cm');
INSERT INTO [dbo].[Mac_Size] VALUES ('MBA13', '0,3 bis 1,7 cm', '32,5 cm', '22,7 cm');
INSERT INTO [dbo].[Mac_Size] VALUES ('MBA13RD', '0,41 bis 1,56 cm', '30,41 cm', '21,24 cm');
INSERT INTO [dbo].[Mac_Size] VALUES ('MBP13', '1,49 cm', '30,41 cm', '21,24 cm');
INSERT INTO [dbo].[Mac_Size] VALUES ('MBP13TB', '1,49 cm', '30,41 cm', '21,24 cm');
INSERT INTO [dbo].[Mac_Size] VALUES ('MBP15', '1,55 cm', '34,93 cm', '24,07 cm');
INSERT INTO [dbo].[Mac_Size] VALUES ('MM', '3,6 cm', '19,7 cm', '19,7 cm');
INSERT INTO [dbo].[Mac_Size] VALUES ('MP', '25,1 cm', '16,7 cm', '16,7 cm');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[Mac_Processor]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[Mac_Processor]') AND type IN ('U'))
	DROP TABLE [dbo].[Mac_Processor]
GO
CREATE TABLE [dbo].[Mac_Processor] (
	[ID] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[name] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[GHZ] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[cores] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[GHZ_Turbo] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__Mac_Proc__3214EC27183EC418] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[Mac_Processor]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[Mac_Processor] VALUES ('iM21_1', 'Intel Core i5', '2,3 GHz', '2', '3,6 GHz');
INSERT INTO [dbo].[Mac_Processor] VALUES ('iM21_2', 'Intel Core i5', '3,0 GHz', '4', '3,5 GHz');
INSERT INTO [dbo].[Mac_Processor] VALUES ('iM21_3', 'Intel Core i7 ', '3,6 GHz ', '4', '4,2 GHz');
INSERT INTO [dbo].[Mac_Processor] VALUES ('iM21_4', 'Intel Core i5', '3,4 GHz', '4', '3,8 GHz');
INSERT INTO [dbo].[Mac_Processor] VALUES ('iM27_1', 'Intel Core i5', '3,4 GHz', '4', '3,8 GHz');
INSERT INTO [dbo].[Mac_Processor] VALUES ('iM27_2', 'Intel Core i5', '3,5 GHz', '4', '4,1 GHz');
INSERT INTO [dbo].[Mac_Processor] VALUES ('iM27_3', 'Intel Core i5', '3,8 GHz', '4', '4,2 GHz');
INSERT INTO [dbo].[Mac_Processor] VALUES ('iM27_4', 'Intel Core i7 ', '4,2 GHz ', '4', '4,5 GHz');
INSERT INTO [dbo].[Mac_Processor] VALUES ('iMP_1', 'Intel Xeon W', '3,2 GHz', '8', '4,2 GHz');
INSERT INTO [dbo].[Mac_Processor] VALUES ('iMP_2', 'Intel Xeon W', '3,0 GHz', '10', '4,5 GHz');
INSERT INTO [dbo].[Mac_Processor] VALUES ('iMP_3', 'Intel Xeon W', '2,5 GHz', '14', '4,3 GHz');
INSERT INTO [dbo].[Mac_Processor] VALUES ('iMP_4', 'Intel Xeon W', '2,3 GHz', '18', '4,3 GHz');
INSERT INTO [dbo].[Mac_Processor] VALUES ('MB12_1', 'Intel Core m3', '1,2 GHz', '2', '3,0 GHz');
INSERT INTO [dbo].[Mac_Processor] VALUES ('MB12_2', 'Intel Core i5', '1,3 GHz', '2', '3,2 GHz');
INSERT INTO [dbo].[Mac_Processor] VALUES ('MB12_3', 'Intel Core i7', '1,4 GHz ', '2', '3,6 GHz');
INSERT INTO [dbo].[Mac_Processor] VALUES ('MBA13_1', 'Intel Core i5', '1,8 GHz', '2', '2,9 GHz');
INSERT INTO [dbo].[Mac_Processor] VALUES ('MBA13_2', 'Intel Core i7', '2,2 GHz', '2', '3,2 GHz');
INSERT INTO [dbo].[Mac_Processor] VALUES ('MBA13RD_1', 'Intel Core i5', '1,6 GHz', '2', '3,6 GHz');
INSERT INTO [dbo].[Mac_Processor] VALUES ('MBP13_1', 'Intel Core i5', '2,3 GHz', '2', '3,6 GHz');
INSERT INTO [dbo].[Mac_Processor] VALUES ('MBP13_2', 'Intel Core i7 ', '2,5 GHz', '2', '4,0 GHz');
INSERT INTO [dbo].[Mac_Processor] VALUES ('MBP13TB_1', 'Intel Core i5', '2,3 GHz', '4', '3,8 GHz');
INSERT INTO [dbo].[Mac_Processor] VALUES ('MBP13TB_2', 'Intel Core i7', '2,7 GHz', '4', '4,5 GHz');
INSERT INTO [dbo].[Mac_Processor] VALUES ('MBP15_1', 'Intel Core i7', '2,2 GHz', '6', '4,1 GHz');
INSERT INTO [dbo].[Mac_Processor] VALUES ('MBP15_2', 'Intel Core i9', '2,9 GHz', '6', '4,8 GHz');
INSERT INTO [dbo].[Mac_Processor] VALUES ('MBP15_3', 'Intel Core i7', '2,6 GHz', '6', '4,3 GHz');
INSERT INTO [dbo].[Mac_Processor] VALUES ('MM_1', 'Intel Core i3', '3,6 GHz', '4', '3,6 Ghz');
INSERT INTO [dbo].[Mac_Processor] VALUES ('MM_2', 'Intel Core i7', '3,2 GHz', '6', '4,6 GHz');
INSERT INTO [dbo].[Mac_Processor] VALUES ('MM_3', 'Intel Core i5', '3,0 GHz', '6', '4,1 GHz');
INSERT INTO [dbo].[Mac_Processor] VALUES ('MP_1', 'Intel Xeon E5', '3,5 GHz', '6', '3,9 GHz');
INSERT INTO [dbo].[Mac_Processor] VALUES ('MP_2', 'Intel Xeon E5', '3,0 GHz', '8', '3,9 GHz');
INSERT INTO [dbo].[Mac_Processor] VALUES ('MP_3', 'Intel Xeon E5', '2,7 GHz', '12', '3,9 GHz');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[Mac_Ports]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[Mac_Ports]') AND type IN ('U'))
	DROP TABLE [dbo].[Mac_Ports]
GO
CREATE TABLE [dbo].[Mac_Ports] (
	[ID] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[USBC] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Hdmi] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[USB3] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Ethernet] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[klinke35] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[sdkarte] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Thunderbolt2] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__Mac_Port__3214EC27AD5E55B6] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[Mac_Ports]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[Mac_Ports] VALUES ('iM21', '2', '0', '4', '1', '1', '1', '0');
INSERT INTO [dbo].[Mac_Ports] VALUES ('iM27', '2', '0', '4', '1', '1', '1', '0');
INSERT INTO [dbo].[Mac_Ports] VALUES ('iMP', '2', '0', '4', '1', '1', '1', '0');
INSERT INTO [dbo].[Mac_Ports] VALUES ('MB12', '1', '0', '0', '0', '1', '0', '0');
INSERT INTO [dbo].[Mac_Ports] VALUES ('MBA13', '0', '0', '2', '0', '1', '1', '1');
INSERT INTO [dbo].[Mac_Ports] VALUES ('MBA13RD', '2', '0', '0', '0', '1', '0', '0');
INSERT INTO [dbo].[Mac_Ports] VALUES ('MBP13', '2', '0', '0', '0', '1', '0', '0');
INSERT INTO [dbo].[Mac_Ports] VALUES ('MBP13TB', '4', '0', '0', '0', '1', '0', '0');
INSERT INTO [dbo].[Mac_Ports] VALUES ('MBP15', '4', '0', '0', '0', '1', '0', '0');
INSERT INTO [dbo].[Mac_Ports] VALUES ('MM', '4', '1', '2', '1', '1', '0', '0');
INSERT INTO [dbo].[Mac_Ports] VALUES ('MP', '0', '1', '4', '2', '1', '0', '6');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[Mac_KeyboardTrackpad]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[Mac_KeyboardTrackpad]') AND type IN ('U'))
	DROP TABLE [dbo].[Mac_KeyboardTrackpad]
GO
CREATE TABLE [dbo].[Mac_KeyboardTrackpad] (
	[ID] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Keyboard] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Touchbar] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Trackpad] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MagicMouse] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MagicTrackpad] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__Mac_Keyb__3214EC27005F2E98] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[Mac_KeyboardTrackpad]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[Mac_KeyboardTrackpad] VALUES ('iM_1', 'Magic Keyboard', 'no', 'no', 'yes', 'no');
INSERT INTO [dbo].[Mac_KeyboardTrackpad] VALUES ('iM_2', 'Magic Keyboard', 'no', 'no', 'no', 'yes');
INSERT INTO [dbo].[Mac_KeyboardTrackpad] VALUES ('iM_3', 'Magic Keyboard', 'no', 'no', 'yes', 'yes');
INSERT INTO [dbo].[Mac_KeyboardTrackpad] VALUES ('MB12', 'Normalgroße Tastatur', 'Nein', 'Force Touch Trackpad', 'no', 'no');
INSERT INTO [dbo].[Mac_KeyboardTrackpad] VALUES ('MBA13', 'Normalgroße beleuchtete Tastatur', 'Nein', 'Multi-Touch Trackpad', 'no', 'no');
INSERT INTO [dbo].[Mac_KeyboardTrackpad] VALUES ('MBA13RD', 'Normalgroße Tastatur', 'Nein', 'Force Touch Trackpad', 'no', 'no');
INSERT INTO [dbo].[Mac_KeyboardTrackpad] VALUES ('MBP13', 'Normalgroße beleuchtete Tastatur', 'Nein', 'Force Touch Trackpad', 'no', 'no');
INSERT INTO [dbo].[Mac_KeyboardTrackpad] VALUES ('MBP13TB', 'Normalgroße beleuchtete Tastatur', 'Touch Bar mit integriertem Touch ID Sensor', 'Force Touch Trackpad', 'no', 'no');
INSERT INTO [dbo].[Mac_KeyboardTrackpad] VALUES ('MBP15', 'Normalgroße beleuchtete Tastatur', 'Touch Bar mit integriertem Touch ID Sensor', 'Force Touch Trackpad', 'no', 'no');
INSERT INTO [dbo].[Mac_KeyboardTrackpad] VALUES ('no', 'No', 'No', 'No', 'no', 'no');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[Mac_Wireless]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[Mac_Wireless]') AND type IN ('U'))
	DROP TABLE [dbo].[Mac_Wireless]
GO
CREATE TABLE [dbo].[Mac_Wireless] (
	[ID] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Wlan] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Bluetooth] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__Mac_Wire__3214EC278B2D97E5] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[Mac_Wireless]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[Mac_Wireless] VALUES ('iM21', '802.11ac WLAN Technologie,kompatibel mit IEEE 802.11a/b/g/n', 'Bluetooth 4.2');
INSERT INTO [dbo].[Mac_Wireless] VALUES ('iM27', '802.11ac WLAN Technologie,kompatibel mit IEEE 802.11a/b/g/n', 'Bluetooth 4.2');
INSERT INTO [dbo].[Mac_Wireless] VALUES ('iMP', '802.11ac WLAN, kompatibel mit IEEE 802.11a/b/g/n', 'Bluetooth 4.2');
INSERT INTO [dbo].[Mac_Wireless] VALUES ('MB12', '802.11ac WLAN, kompatibel mit IEEE 802.11a/b/g/n', 'Bluetooth 4.2');
INSERT INTO [dbo].[Mac_Wireless] VALUES ('MBA13', '802.11ac WLAN, kompatibel mit IEEE 802.11a/b/g/n', 'Bluetooth 4.0');
INSERT INTO [dbo].[Mac_Wireless] VALUES ('MBA13RD', '802.11ac WLAN, kompatibel mit IEEE 802.11a/b/g/n', 'Bluetooth 4.2');
INSERT INTO [dbo].[Mac_Wireless] VALUES ('MBP13', '802.11ac WLAN, kompatibel mit IEEE 802.11a/b/g/n', 'Bluetooth 4.2');
INSERT INTO [dbo].[Mac_Wireless] VALUES ('MBP13TB', '802.11ac WLAN, kompatibel mit IEEE 802.11a/b/g/n', 'Bluetooth 5.0');
INSERT INTO [dbo].[Mac_Wireless] VALUES ('MBP15', '802.11ac WLAN, kompatibel mit IEEE 802.11a/b/g/n', 'Bluetooth 5.0');
INSERT INTO [dbo].[Mac_Wireless] VALUES ('MM', '802.11ac WLAN, kompatibel mit IEEE 802.11a/b/g/n', 'Bluetooth 5.0');
INSERT INTO [dbo].[Mac_Wireless] VALUES ('MP', '802.11ac WLAN, kompatibel mit IEEE 802.11a/b/g/n', 'Bluetooth 4.0');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[Mac_Powersupply]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[Mac_Powersupply]') AND type IN ('U'))
	DROP TABLE [dbo].[Mac_Powersupply]
GO
CREATE TABLE [dbo].[Mac_Powersupply] (
	[ID] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Webbrowsing] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Moviereplay] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Standby] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__Mac_Powe__3214EC27048FB7EC] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[Mac_Powersupply]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[Mac_Powersupply] VALUES ('MB12', '10 Stunden', '12 Stunden', '30 Tage');
INSERT INTO [dbo].[Mac_Powersupply] VALUES ('MBA13', '12 Stunden', '12 Stunden', '30 Tage');
INSERT INTO [dbo].[Mac_Powersupply] VALUES ('MBA13RD', '12 Stunden', '13 Stunden', '30 Tage');
INSERT INTO [dbo].[Mac_Powersupply] VALUES ('MBP13', '10 Stunden', '10 Stunden', '30 Tage');
INSERT INTO [dbo].[Mac_Powersupply] VALUES ('MBP13TB', '10 Stunden', '10 Stunden', '30 Tage');
INSERT INTO [dbo].[Mac_Powersupply] VALUES ('MBP15', '10 Stunden', '10 Stunden', '30 Tage');
INSERT INTO [dbo].[Mac_Powersupply] VALUES ('wire', 'unbegrenzt', 'unbegrenzt', 'unbegrenzt');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[iPad]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[iPad]') AND type IN ('U'))
	DROP TABLE [dbo].[iPad]
GO
CREATE TABLE [dbo].[iPad] (
	[ID] int NOT NULL,
	[Name] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Color] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Capacity] int NULL,
	[Size] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Weight] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Display] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Processor] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Camera] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Videorecording] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FrontCamera] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Biometrie] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ApplePay] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Wireless] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Locating] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PowerSupply] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Sensors] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SimCard] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Price] int NULL,
	[imageURL] varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Connectivity] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__iPhone_c__3214EC27E2AD6C29] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[iPad]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[iPad] VALUES ('1', 'iPad Pro', 'Silber', '64', 'iPadP11', '468', 'iPadP11', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP11', '1', 'iPad', '1', '1', '1049', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-11-silver.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('2', 'iPad Pro', 'Space Grau', '64', 'iPadP11', '468 g', 'iPadP11', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP11', '1', 'iPad', '1', '1', '1049', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-11-spacegray.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('3', 'iPad Pro', 'Silber', '256', 'iPadP11', '468 g', 'iPadP11', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP11', '1', 'iPad', '1', '1', '1219', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-11-silver.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('4', 'iPad Pro', 'Space Grau', '256', 'iPadP11', '468 g', 'iPadP11', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP11', '1', 'iPad', '1', '1', '1219', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-11-spacegray.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('5', 'iPad Pro', 'Silber', '512', 'iPadP11', '468 g', 'iPadP11', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP11', '1', 'iPad', '1', '1', '1439', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-11-silver.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('6', 'iPad Pro', 'Space Grau', '512', 'iPadP11', '468 g', 'iPadP11', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP11', '1', 'iPad', '1', '1', '1439', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-11-spacegray.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('7', 'iPad Pro', 'Silber', '1024', 'iPadP11', '468 g', 'iPadP11', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP11', '1', 'iPad', '1', '1', '1879', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-11-silver.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('8', 'iPad Pro', 'Space Grau', '1024', 'iPadP11', '468 g', 'iPadP11', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP11', '1', 'iPad', '1', '1', '1879', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-11-spacegray.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('9', 'iPad Pro', 'Silber', '64', 'iPadP11', '468 g', 'iPadP11', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP11', '2', 'iPad', '1', '3', '879', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-11-silver.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('10', 'iPad Pro', 'Space Grau', '64', 'iPadP11', '468 g', 'iPadP11', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP11', '2', 'iPad', '1', '3', '879', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-11-spacegray.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('11', 'iPad Pro', 'Silber', '256', 'iPadP11', '468 g', 'iPadP11', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP11', '2', 'iPad', '1', '3', '1049', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-11-silver.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('12', 'iPad Pro', 'Space Grau', '256', 'iPadP11', '468 g', 'iPadP11', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP11', '2', 'iPad', '1', '3', '1049', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-11-spacegray.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('13', 'iPad Pro', 'Silber', '512', 'iPadP11', '468 g', 'iPadP11', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP11', '2', 'iPad', '1', '3', '1269', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-11-silver.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('14', 'iPad Pro', 'Space Grau', '512', 'iPadP11', '468 g', 'iPadP11', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP11', '2', 'iPad', '1', '3', '1269', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-11-spacegray.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('15', 'iPad Pro', 'Silber', '1024', 'iPadP11', '468 g', 'iPadP11', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP11', '2', 'iPad', '1', '3', '1709', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-11-silver.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('16', 'iPad Pro', 'Space Grau', '1024', 'iPadP11', '468 g', 'iPadP11', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP11', '2', 'iPad', '1', '3', '1709', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-11-spacegray.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('17', 'iPad Pro', 'Silber', '64', 'iPadP12', '633 g', 'iPadP12', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP12', '1', 'iPad', '1', '1', '1269', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-12-silver.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('18', 'iPad Pro', 'Space Grau', '64', 'iPadP12', '633 g', 'iPadP12', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP12', '1', 'iPad', '1', '1', '1269', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-12-spacegray.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('19', 'iPad Pro', 'Silber', '256', 'iPadP12', '633 g', 'iPadP12', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP12', '1', 'iPad', '1', '1', '1439', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-12-silver.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('20', 'iPad Pro', 'Space Grau', '256', 'iPadP12', '633 g', 'iPadP12', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP12', '1', 'iPad', '1', '1', '1439', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-12-spacegray.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('21', 'iPad Pro', 'Silber', '512', 'iPadP12', '633 g', 'iPadP12', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP12', '1', 'iPad', '1', '1', '1659', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-12-silver.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('22', 'iPad Pro', 'Space Grau', '512', 'iPadP12', '633 g', 'iPadP12', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP12', '1', 'iPad', '1', '1', '1659', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-12-spacegray.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('23', 'iPad Pro', 'Silber', '1024', 'iPadP12', '633 g', 'iPadP12', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP12', '1', 'iPad', '1', '1', '2099', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-12-silver.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('24', 'iPad Pro', 'Space Grau', '1024', 'iPadP12', '633 g', 'iPadP12', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP12', '1', 'iPad', '1', '1', '2099', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-12-spacegray.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('25', 'iPad Pro', 'Silber', '64', 'iPadP12', '631 g', 'iPadP12', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP12', '2', 'iPad', '1', '3', '1099', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-12-silver.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('26', 'iPad Pro', 'Space Grau', '64', 'iPadP12', '631 g', 'iPadP12', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP12', '2', 'iPad', '1', '3', '1099', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-12-spacegray.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('27', 'iPad Pro', 'Silber', '256', 'iPadP12', '631 g', 'iPadP12', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP12', '2', 'iPad', '1', '3', '1269', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-12-silver.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('28', 'iPad Pro', 'Space Grau', '256', 'iPadP12', '631 g', 'iPadP12', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP12', '2', 'iPad', '1', '3', '1269', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-12-spacegray.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('29', 'iPad Pro', 'Silber', '512', 'iPadP12', '631 g', 'iPadP12', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP12', '2', 'iPad', '1', '3', '1489', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-12-silver.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('30', 'iPad Pro', 'Space Grau', '512', 'iPadP12', '631 g', 'iPadP12', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP12', '2', 'iPad', '1', '3', '1489', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-12-spacegray.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('31', 'iPad Pro', 'Silber', '1024', 'iPadP12', '631 g', 'iPadP12', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP12', '2', 'iPad', '1', '3', '1929', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-12-silver.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('32', 'iPad Pro', 'Space Grau', '1024', 'iPadP12', '631 g', 'iPadP12', 'A12', '1', '2', 'TD', 'FaceID', 'ja', 'iPadP12', '2', 'iPad', '1', '3', '1929', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-12-spacegray.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('33', 'iPad Pro', 'Silber', '64', 'iPadP10', '469 g', 'ipadP10', 'A10', '1', '1', 'FT', 'Touch ID', 'nein', 'iPadP10', '2', 'iPad', '1', '3', '729', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-10-silver.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('34', 'iPad Pro', 'Space Grau', '64', 'iPadP10', '469 g', 'ipadP10', 'A10', '1', '1', 'FT', 'Touch ID', 'nein', 'iPadP10', '2', 'iPad', '1', '3', '729', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-10-spacegray.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('35', 'iPad Pro', 'Gold', '64', 'iPadP10', '469 g', 'ipadP10', 'A10', '1', '1', 'FT', 'Touch ID', 'nein', 'iPadP10', '2', 'iPad', '1', '3', '729', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-10-gold.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('36', 'iPad Pro', 'Rosegold', '64', 'iPadP10', '469 g', 'ipadP10', 'A10', '1', '1', 'FT', 'Touch ID', 'nein', 'iPadP10', '2', 'iPad', '1', '3', '729', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-10-rosegold.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('37', 'iPad Pro', 'Silber', '256', 'iPadP10', '469 g', 'ipadP10', 'A10', '1', '1', 'FT', 'Touch ID', 'nein', 'iPadP10', '2', 'iPad', '1', '3', '899', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-10-silver.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('38', 'iPad Pro', 'Space Grau', '256', 'iPadP10', '469 g', 'ipadP10', 'A10', '1', '1', 'FT', 'Touch ID', 'nein', 'iPadP10', '2', 'iPad', '1', '3', '899', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-10-spacegray.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('39', 'iPad Pro', 'Gold', '256', 'iPadP10', '469 g', 'ipadP10', 'A10', '1', '1', 'FT', 'Touch ID', 'nein', 'iPadP10', '2', 'iPad', '1', '3', '899', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-10-gold.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('40', 'iPad Pro', 'Rosegold', '256', 'iPadP10', '469 g', 'ipadP10', 'A10', '1', '1', 'FT', 'Touch ID', 'nein', 'iPadP10', '2', 'iPad', '1', '3', '899', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-10-rosegold.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('41', 'iPad Pro', 'Silber', '512', 'iPadP10', '469 g', 'ipadP10', 'A10', '1', '1', 'FT', 'Touch ID', 'nein', 'iPadP10', '2', 'iPad', '1', '3', '1119', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-10-silver.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('42', 'iPad Pro', 'Space Grau', '512', 'iPadP10', '469 g', 'ipadP10', 'A10', '1', '1', 'FT', 'Touch ID', 'nein', 'iPadP10', '2', 'iPad', '1', '3', '1119', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-10-spacegray.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('43', 'iPad Pro', 'Gold', '512', 'iPadP10', '469 g', 'ipadP10', 'A10', '1', '1', 'FT', 'Touch ID', 'nein', 'iPadP10', '2', 'iPad', '1', '3', '1119', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-10-gold.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('44', 'iPad Pro', 'Rosegold', '512', 'iPadP10', '469 g', 'ipadP10', 'A10', '1', '1', 'FT', 'Touch ID', 'nein', 'iPadP10', '2', 'iPad', '1', '3', '1119', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-10-rosegold.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('45', 'iPad Pro', 'Silber', '64', 'iPadP10', '477 g', 'ipadP10', 'A10', '1', '1', 'FT', 'Touch ID', 'nein', 'iPadP10', '1', 'iPad', '1', '1', '729', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-10-silver.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('46', 'iPad Pro', 'Space Grau', '64', 'iPadP10', '477 g', 'ipadP10', 'A10', '1', '1', 'FT', 'Touch ID', 'nein', 'iPadP10', '1', 'iPad', '1', '1', '729', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-10-spacegray.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('47', 'iPad Pro', 'Gold', '64', 'iPadP10', '477 g', 'ipadP10', 'A10', '1', '1', 'FT', 'Touch ID', 'nein', 'iPadP10', '1', 'iPad', '1', '1', '729', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-10-gold.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('48', 'iPad Pro', 'Rosegold', '64', 'iPadP10', '477 g', 'ipadP10', 'A10', '1', '1', 'FT', 'Touch ID', 'nein', 'iPadP10', '1', 'iPad', '1', '1', '729', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-10-rosegold.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('49', 'iPad Pro', 'Silber', '256', 'iPadP10', '477 g', 'ipadP10', 'A10', '1', '1', 'FT', 'Touch ID', 'nein', 'iPadP10', '1', 'iPad', '1', '1', '899', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-10-silver.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('50', 'iPad Pro', 'Space Grau', '256', 'iPadP10', '477 g', 'ipadP10', 'A10', '1', '1', 'FT', 'Touch ID', 'nein', 'iPadP10', '1', 'iPad', '1', '1', '899', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-10-spacegray.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('51', 'iPad Pro', 'Gold', '256', 'iPadP10', '477 g', 'ipadP10', 'A10', '1', '1', 'FT', 'Touch ID', 'nein', 'iPadP10', '1', 'iPad', '1', '1', '899', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-10-gold.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('52', 'iPad Pro', 'Rosegold', '256', 'iPadP10', '477 g', 'ipadP10', 'A10', '1', '1', 'FT', 'Touch ID', 'nein', 'iPadP10', '1', 'iPad', '1', '1', '899', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-10-rosegold.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('53', 'iPad Pro', 'Silber', '512', 'iPadP10', '477 g', 'ipadP10', 'A10', '1', '1', 'FT', 'Touch ID', 'nein', 'iPadP10', '1', 'iPad', '1', '1', '1119', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-10-silver.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('54', 'iPad Pro', 'Space Grau', '512', 'iPadP10', '477 g', 'ipadP10', 'A10', '1', '1', 'FT', 'Touch ID', 'nein', 'iPadP10', '1', 'iPad', '1', '1', '1119', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-10-spacegray.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('55', 'iPad Pro', 'Gold', '512', 'iPadP10', '477 g', 'ipadP10', 'A10', '1', '1', 'FT', 'Touch ID', 'nein', 'iPadP10', '1', 'iPad', '1', '1', '1119', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-10-gold.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('56', 'iPad Pro', 'Rosegold', '512', 'iPadP10', '477 g', 'ipadP10', 'A10', '1', '1', 'FT', 'Touch ID', 'nein', 'iPadP10', '1', 'iPad', '1', '1', '1119', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-pro-10-rosegold.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('57', 'iPad', 'Silber', '32', 'iPad', '469 g', 'iPad', 'A10', '2', '3', 'FTlow', 'Touch ID', 'nein', 'iPad', '2', 'iPad', '1', '3', '349', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-silver.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('58', 'iPad', 'Space Grau', '32', 'iPad', '469 g', 'iPad', 'A10', '2', '3', 'FTlow', 'Touch ID', 'nein', 'iPad', '2', 'iPad', '1', '3', '349', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-spacegray.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('59', 'iPad', 'Gold', '32', 'iPad', '469 g', 'iPad', 'A10', '2', '3', 'FTlow', 'Touch ID', 'nein', 'iPad', '2', 'iPad', '1', '3', '349', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-gold.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('60', 'iPad', 'Silber', '128', 'iPad', '469 g', 'iPad', 'A10', '2', '3', 'FTlow', 'Touch ID', 'nein', 'iPad', '2', 'iPad', '1', '3', '439', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-silver.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('61', 'iPad', 'Space Grau', '128', 'iPad', '469 g', 'iPad', 'A10', '2', '3', 'FTlow', 'Touch ID', 'nein', 'iPad', '2', 'iPad', '1', '3', '439', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-spacegray.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('62', 'iPad', 'Gold', '128', 'iPad', '469 g', 'iPad', 'A10', '2', '3', 'FTlow', 'Touch ID', 'nein', 'iPad', '2', 'iPad', '1', '3', '439', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-gold.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('63', 'iPad', 'Silber', '32', 'iPad', '478 g', 'iPad', 'A10', '2', '3', 'FTlow', 'Touch ID', 'nein', 'iPad', '1', 'iPad', '1', '1', '479', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-silver.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('64', 'iPad', 'Space Grau', '32', 'iPad', '478 g', 'iPad', 'A10', '2', '3', 'FTlow', 'Touch ID', 'nein', 'iPad', '1', 'iPad', '1', '1', '479', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-spacegray.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('65', 'iPad', 'Gold', '32', 'iPad', '478 g', 'iPad', 'A10', '2', '3', 'FTlow', 'Touch ID', 'nein', 'iPad', '1', 'iPad', '1', '1', '479', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-gold.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('66', 'iPad', 'Silber', '128', 'iPad', '478 g', 'iPad', 'A10', '2', '3', 'FTlow', 'Touch ID', 'nein', 'iPad', '1', 'iPad', '1', '1', '569', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-silver.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('67', 'iPad', 'Space Grau', '128', 'iPad', '478 g', 'iPad', 'A10', '2', '3', 'FTlow', 'Touch ID', 'nein', 'iPad', '1', 'iPad', '1', '1', '569', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-spacegray.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('68', 'iPad', 'Gold', '128', 'iPad', '478 g', 'iPad', 'A10', '2', '3', 'FTlow', 'Touch ID', 'nein', 'iPad', '1', 'iPad', '1', '1', '569', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-gold.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('69', 'iPad Mini', 'Silber', '128', 'iPadM', '298.8 g', 'iPadM', 'A8', '2', '3', 'FTlow', 'Touch ID', 'ja', 'ipadM', '2', 'ipad', '1', '3', '429', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-mini4-silver.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('70', 'iPad Mini', 'Space Grau', '128', 'iPadM', '298.8 g', 'iPadM', 'A8', '2', '3', 'FTlow', 'Touch ID', 'ja', 'ipadM', '2', 'ipad', '1', '3', '429', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-mini4-gray.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('71', 'iPad Mini', 'Gold', '128', 'iPadM', '298.8 g', 'iPadM', 'A8', '2', '3', 'FTlow', 'Touch ID', 'ja', 'ipadM', '2', 'ipad', '1', '3', '429', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-mini4-gold.png', 'Wifi');
INSERT INTO [dbo].[iPad] VALUES ('72', 'iPad Mini', 'Silber', '128', 'iPadM', '304 g', 'iPadM', 'A8', '2', '3', 'FTlow', 'Touch ID', 'ja', 'ipadM', '1', 'ipad', '1', '1', '559', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-mini4-silver.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('73', 'iPad Mini', 'Space Grau', '128', 'iPadM', '304 g', 'iPadM', 'A8', '2', '3', 'FTlow', 'Touch ID', 'ja', 'ipadM', '1', 'ipad', '1', '1', '559', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-mini4-gray.png', 'Cellular+Wifi');
INSERT INTO [dbo].[iPad] VALUES ('74', 'iPad Mini', 'Gold', '128', 'iPadM', '304 g', 'iPadM', 'A8', '2', '3', 'FTlow', 'Touch ID', 'ja', 'ipadM', '1', 'ipad', '1', '1', '559', 'https://bscchatbotblob.blob.core.windows.net/images/ipad-mini4-gold.png', 'Cellular+Wifi');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[iPad_Size]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[iPad_Size]') AND type IN ('U'))
	DROP TABLE [dbo].[iPad_Size]
GO
CREATE TABLE [dbo].[iPad_Size] (
	[ID] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Height] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Depth] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Width] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__iPhone_S__3214EC27836229F5] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[iPad_Size]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[iPad_Size] VALUES ('iPad', '240 mm', '7.5 mm', '169.5 mm');
INSERT INTO [dbo].[iPad_Size] VALUES ('iPadM', '203.2 mm', '6.1 mm', '134.8 mm');
INSERT INTO [dbo].[iPad_Size] VALUES ('iPadP10', '250.6 mm', '6.1 mm', '174.1 mm');
INSERT INTO [dbo].[iPad_Size] VALUES ('iPadP11', '247.6 mm', '5.9 mm', '178.5 mm');
INSERT INTO [dbo].[iPad_Size] VALUES ('iPadP12', '280.6 mm', '5.9 mm', '214.9 mm');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[iPad_Display]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[iPad_Display]') AND type IN ('U'))
	DROP TABLE [dbo].[iPad_Display]
GO
CREATE TABLE [dbo].[iPad_Display] (
	[ID] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Size] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Name] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Resolution] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PPI] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TrueTone] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__iPhone_D__3214EC2797445A49] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[iPad_Display]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[iPad_Display] VALUES ('iPad', '9.7', 'Retina Display', '2048 x 1536', '264', 'nein');
INSERT INTO [dbo].[iPad_Display] VALUES ('ipadM', '7.9', 'Retina Display', '2048 x 1536', '326', 'nein');
INSERT INTO [dbo].[iPad_Display] VALUES ('iPadP10', '10.5', 'Retina Display', '2224 x 1668', '264', 'ja');
INSERT INTO [dbo].[iPad_Display] VALUES ('iPadP11', '11', 'Liquid Retina Display', '2388 x 1668', '264', 'ja');
INSERT INTO [dbo].[iPad_Display] VALUES ('iPadP12', '12.9', 'Liquid Retina Display', '2732 x 2048', '264', 'ja');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[iPad_Processor]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[iPad_Processor]') AND type IN ('U'))
	DROP TABLE [dbo].[iPad_Processor]
GO
CREATE TABLE [dbo].[iPad_Processor] (
	[ID] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Name] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NeuralEngine] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__iPhone_P__3214EC27CDC09FE1] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[iPad_Processor]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[iPad_Processor] VALUES ('A10', 'A10X Fusion Chip mit 64-Bit Architektur', 'nein');
INSERT INTO [dbo].[iPad_Processor] VALUES ('A12', 'A12X Bionic Chip mit 64-Bit Architektur', 'ja');
INSERT INTO [dbo].[iPad_Processor] VALUES ('A8', 'A8 Chip mit 64-Bit Architektur', 'nein');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[iPad_Camera]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[iPad_Camera]') AND type IN ('U'))
	DROP TABLE [dbo].[iPad_Camera]
GO
CREATE TABLE [dbo].[iPad_Camera] (
	[ID] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Megapixel] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Aperture] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__iPhone_C__3214EC27CB91BB09] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[iPad_Camera]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[iPad_Camera] VALUES ('1', '12', '1,8');
INSERT INTO [dbo].[iPad_Camera] VALUES ('2', '8', '2,4');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[iPhone]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[iPhone]') AND type IN ('U'))
	DROP TABLE [dbo].[iPhone]
GO
CREATE TABLE [dbo].[iPhone] (
	[ID] int NOT NULL,
	[Name] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Color] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Capacity] int NULL,
	[Size] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Weight] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Display] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IPProtection] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Processor] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Camera] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Videorecording] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FrontCamera] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Biometrie] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ApplePay] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Wireless] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Locating] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PowerSupply] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Sensors] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SimCard] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Price] int NULL,
	[imageURL] varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__iPhone__3214EC27292D8A14] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[iPhone]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[iPhone] VALUES ('1', 'iPhone 7', 'Rosegold', '32', 'iPh7', '138', 'iPh7', 'IP67', 'A10', '1', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7', '1', '1', '519', 'https://bscchatbotblob.blob.core.windows.net/images/iphone7-rosegold.png');
INSERT INTO [dbo].[iPhone] VALUES ('2', 'iPhone 7', 'Gold', '32', 'iPh7', '138', 'iPh7', 'IP67', 'A10', '1', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7', '1', '1', '519', 'https://bscchatbotblob.blob.core.windows.net/images/iphone7-gold.png');
INSERT INTO [dbo].[iPhone] VALUES ('3', 'iPhone 7', 'Silber', '32', 'iPh7', '138', 'iPh7', 'IP67', 'A10', '1', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7', '1', '1', '519', 'https://bscchatbotblob.blob.core.windows.net/images/iphone7-silver.png');
INSERT INTO [dbo].[iPhone] VALUES ('4', 'iPhone 7', 'Schwarz', '32', 'iPh7', '138', 'iPh7', 'IP67', 'A10', '1', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7', '1', '1', '519', 'https://bscchatbotblob.blob.core.windows.net/images/iphone7-black.png');
INSERT INTO [dbo].[iPhone] VALUES ('5', 'iPhone 7', 'Rosegold', '128', 'iPh7', '138', 'iPh7', 'IP67', 'A10', '1', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7', '1', '1', '629', 'https://bscchatbotblob.blob.core.windows.net/images/iphone7-rosegold.png');
INSERT INTO [dbo].[iPhone] VALUES ('6', 'iPhone 7', 'Gold', '128', 'iPh7', '138', 'iPh7', 'IP67', 'A10', '1', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7', '1', '1', '629', 'https://bscchatbotblob.blob.core.windows.net/images/iphone7-gold.png');
INSERT INTO [dbo].[iPhone] VALUES ('7', 'iPhone 7', 'Silber', '128', 'iPh7', '138', 'iPh7', 'IP67', 'A10', '1', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7', '1', '1', '629', 'https://bscchatbotblob.blob.core.windows.net/images/iphone7-silver.png');
INSERT INTO [dbo].[iPhone] VALUES ('8', 'iPhone 7', 'Schwarz', '128', 'iPh7', '138', 'iPh7', 'IP67', 'A10', '1', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7', '1', '1', '629', 'https://bscchatbotblob.blob.core.windows.net/images/iphone7-black.png');
INSERT INTO [dbo].[iPhone] VALUES ('9', 'iPhone 7 Plus', 'Rosegold', '32', 'iPh7Plus', '188', 'iPh7Plus', 'IP67', 'A10', '2', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7Plus', '1', '1', '649', 'https://bscchatbotblob.blob.core.windows.net/images/iphone7-plus-rosegold.png');
INSERT INTO [dbo].[iPhone] VALUES ('10', 'iPhone 7 Plus', 'Gold', '32', 'iPh7Plus', '188', 'iPh7Plus', 'IP67', 'A10', '2', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7Plus', '1', '1', '649', 'https://bscchatbotblob.blob.core.windows.net/images/iphone7-plus-gold.png');
INSERT INTO [dbo].[iPhone] VALUES ('11', 'iPhone 7 Plus', 'Schwarz', '32', 'iPh7Plus', '188', 'iPh7Plus', 'IP67', 'A10', '2', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7Plus', '1', '1', '649', 'https://bscchatbotblob.blob.core.windows.net/images/iphone7-plus-black.png');
INSERT INTO [dbo].[iPhone] VALUES ('12', 'iPhone 7 Plus', 'Silber', '32', 'iPh7Plus', '188', 'iPh7Plus', 'IP67', 'A10', '2', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7Plus', '1', '1', '649', 'https://bscchatbotblob.blob.core.windows.net/images/iphone7-plus-silver.png');
INSERT INTO [dbo].[iPhone] VALUES ('13', 'iPhone 7 Plus', 'Rosegold', '128', 'iPh7Plus', '188', 'iPh7Plus', 'IP67', 'A10', '2', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7Plus', '1', '1', '759', 'https://bscchatbotblob.blob.core.windows.net/images/iphone7-plus-rosegold.png');
INSERT INTO [dbo].[iPhone] VALUES ('14', 'iPhone 7 Plus', 'Gold', '128', 'iPh7Plus', '188', 'iPh7Plus', 'IP67', 'A10', '2', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7Plus', '1', '1', '759', 'https://bscchatbotblob.blob.core.windows.net/images/iphone7-plus-gold.png');
INSERT INTO [dbo].[iPhone] VALUES ('15', 'iPhone 7 Plus', 'Schwarz', '128', 'iPh7Plus', '188', 'iPh7Plus', 'IP67', 'A10', '2', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7Plus', '1', '1', '759', 'https://bscchatbotblob.blob.core.windows.net/images/iphone7-plus-black.png');
INSERT INTO [dbo].[iPhone] VALUES ('16', 'iPhone 7 Plus', 'Silber', '128', 'iPh7Plus', '188', 'iPh7Plus', 'IP67', 'A10', '2', '1', 'FT', 'Touch ID', 'ja', 'iPh7', '1', 'iPh7Plus', '1', '1', '759', 'https://bscchatbotblob.blob.core.windows.net/images/iphone7-plus-silver.png');
INSERT INTO [dbo].[iPhone] VALUES ('17', 'iPhone 8', 'Silber', '64', 'iPh8', '148', 'iPh8', 'IP67', 'A11', '1', '2', 'FT', 'Touch ID', 'ja', 'iPh8', '1', 'iPh8', '1', '1', '679', 'https://bscchatbotblob.blob.core.windows.net/images/iphone8-silver.png');
INSERT INTO [dbo].[iPhone] VALUES ('18', 'iPhone 8', 'Space Grau', '64', 'iPh8', '148', 'iPh8', 'IP67', 'A11', '1', '2', 'FT', 'Touch ID', 'ja', 'iPh8', '1', 'iPh8', '1', '1', '679', 'https://bscchatbotblob.blob.core.windows.net/images/iphone8-spgray.png');
INSERT INTO [dbo].[iPhone] VALUES ('19', 'iPhone 8', 'Gold', '64', 'iPh8', '148', 'iPh8', 'IP67', 'A11', '1', '2', 'FT', 'Touch ID', 'ja', 'iPh8', '1', 'iPh8', '1', '1', '679', 'https://bscchatbotblob.blob.core.windows.net/images/iphone8-gold.png');
INSERT INTO [dbo].[iPhone] VALUES ('20', 'iPhone 8', 'Silber', '256', 'iPh8', '148', 'iPh8', 'IP67', 'A11', '1', '2', 'FT', 'Touch ID', 'ja', 'iPh8', '1', 'iPh8', '1', '1', '849', 'https://bscchatbotblob.blob.core.windows.net/images/iphone8-silver.png');
INSERT INTO [dbo].[iPhone] VALUES ('21', 'iPhone 8', 'Space Grau', '256', 'iPh8', '148', 'iPh8', 'IP67', 'A11', '1', '2', 'FT', 'Touch ID', 'ja', 'iPh8', '1', 'iPh8', '1', '1', '849', 'https://bscchatbotblob.blob.core.windows.net/images/iphone8-spgray.png');
INSERT INTO [dbo].[iPhone] VALUES ('22', 'iPhone 8', 'Gold', '256', 'iPh8', '148', 'iPh8', 'IP67', 'A11', '1', '2', 'FT', 'Touch ID', 'ja', 'iPh8', '1', 'iPh8', '1', '1', '849', 'https://bscchatbotblob.blob.core.windows.net/images/iphone8-gold.png');
INSERT INTO [dbo].[iPhone] VALUES ('23', 'iPhone 8 Plus', 'Silber', '64', 'iPh8Plus', '202', 'iPh8Plus', 'IP67', 'A11', '2', '2', 'FT', 'Touch ID', 'ja', 'iPh8', '1', 'iPh8', '1', '1', '789', 'https://bscchatbotblob.blob.core.windows.net/images/iphone8-plus-silver.png');
INSERT INTO [dbo].[iPhone] VALUES ('24', 'iPhone 8 Plus', 'Space Grau', '64', 'iPh8Plus', '202', 'iPh8Plus', 'IP67', 'A11', '2', '2', 'FT', 'Touch ID', 'ja', 'iPh8', '1', 'iPh8', '1', '1', '789', 'https://bscchatbotblob.blob.core.windows.net/images/iphone8-plus-spgray.png');
INSERT INTO [dbo].[iPhone] VALUES ('25', 'iPhone 8 Plus', 'Gold', '64', 'iPh8Plus', '202', 'iPh8Plus', 'IP67', 'A11', '2', '2', 'FT', 'Touch ID', 'ja', 'iPh8', '1', 'iPh8', '1', '1', '789', 'https://bscchatbotblob.blob.core.windows.net/images/iphone8-plus-gold.png');
INSERT INTO [dbo].[iPhone] VALUES ('26', 'iPhone 8 Plus', 'Silber', '256', 'iPh8Plus', '202', 'iPh8Plus', 'IP67', 'A11', '2', '2', 'FT', 'Touch ID', 'ja', 'iPh8', '1', 'iPh8', '1', '1', '959', 'https://bscchatbotblob.blob.core.windows.net/images/iphone8-plus-silver.png');
INSERT INTO [dbo].[iPhone] VALUES ('27', 'iPhone 8 Plus', 'Space Grau', '256', 'iPh8Plus', '202', 'iPh8Plus', 'IP67', 'A11', '2', '2', 'FT', 'Touch ID', 'ja', 'iPh8', '1', 'iPh8', '1', '1', '959', 'https://bscchatbotblob.blob.core.windows.net/images/iphone8-plus-spgray.png');
INSERT INTO [dbo].[iPhone] VALUES ('28', 'iPhone 8 Plus', 'Gold', '256', 'iPh8Plus', '202', 'iPh8Plus', 'IP67', 'A11', '2', '2', 'FT', 'Touch ID', 'ja', 'iPh8', '1', 'iPh8', '1', '1', '959', 'https://bscchatbotblob.blob.core.windows.net/images/iphone8-plus-gold.png');
INSERT INTO [dbo].[iPhone] VALUES ('29', 'iPhone XR', 'Blau', '64', 'iPhXR', '194', 'iPhXR', 'IP67', 'A12', '1', '2', 'TD', 'Face ID', 'ja', 'iPhXR', '1', 'iPhXR', '1', '2', '849', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xr-blue.png');
INSERT INTO [dbo].[iPhone] VALUES ('30', 'iPhone XR', 'Schwarz', '64', 'iPhXR', '194', 'iPhXR', 'IP67', 'A12', '1', '2', 'TD', 'Face ID', 'ja', 'iPhXR', '1', 'iPhXR', '1', '2', '849', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xr-black.png');
INSERT INTO [dbo].[iPhone] VALUES ('31', 'iPhone XR', 'Koralle', '64', 'iPhXR', '194', 'iPhXR', 'IP67', 'A12', '1', '2', 'TD', 'Face ID', 'ja', 'iPhXR', '1', 'iPhXR', '1', '2', '849', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xr-coral.png');
INSERT INTO [dbo].[iPhone] VALUES ('32', 'iPhone XR', 'Weiß', '64', 'iPhXR', '194', 'iPhXR', 'IP67', 'A12', '1', '2', 'TD', 'Face ID', 'ja', 'iPhXR', '1', 'iPhXR', '1', '2', '849', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xr-white.png');
INSERT INTO [dbo].[iPhone] VALUES ('33', 'iPhone XR', 'Gelb', '64', 'iPhXR', '194', 'iPhXR', 'IP67', 'A12', '1', '2', 'TD', 'Face ID', 'ja', 'iPhXR', '1', 'iPhXR', '1', '2', '849', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xr-yellow.png');
INSERT INTO [dbo].[iPhone] VALUES ('34', 'iPhone XR', 'Rot', '64', 'iPhXR', '194', 'iPhXR', 'IP67', 'A12', '1', '2', 'TD', 'Face ID', 'ja', 'iPhXR', '1', 'iPhXR', '1', '2', '849', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xr-red.png');
INSERT INTO [dbo].[iPhone] VALUES ('35', 'iPhone XR', 'Blau', '128', 'iPhXR', '194', 'iPhXR', 'IP67', 'A12', '1', '2', 'TD', 'Face ID', 'ja', 'iPhXR', '1', 'iPhXR', '1', '2', '909', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xr-blue.png');
INSERT INTO [dbo].[iPhone] VALUES ('36', 'iPhone XR', 'Schwarz', '128', 'iPhXR', '194', 'iPhXR', 'IP67', 'A12', '1', '2', 'TD', 'Face ID', 'ja', 'iPhXR', '1', 'iPhXR', '1', '2', '909', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xr-black.png');
INSERT INTO [dbo].[iPhone] VALUES ('37', 'iPhone XR', 'Koralle', '128', 'iPhXR', '194', 'iPhXR', 'IP67', 'A12', '1', '2', 'TD', 'Face ID', 'ja', 'iPhXR', '1', 'iPhXR', '1', '2', '909', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xr-coral.png');
INSERT INTO [dbo].[iPhone] VALUES ('38', 'iPhone XR', 'Weiß', '128', 'iPhXR', '194', 'iPhXR', 'IP67', 'A12', '1', '2', 'TD', 'Face ID', 'ja', 'iPhXR', '1', 'iPhXR', '1', '2', '909', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xr-white.png');
INSERT INTO [dbo].[iPhone] VALUES ('39', 'iPhone XR', 'Gelb', '128', 'iPhXR', '194', 'iPhXR', 'IP67', 'A12', '1', '2', 'TD', 'Face ID', 'ja', 'iPhXR', '1', 'iPhXR', '1', '2', '909', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xr-yellow.png');
INSERT INTO [dbo].[iPhone] VALUES ('40', 'iPhone XR', 'Rot', '128', 'iPhXR', '194', 'iPhXR', 'IP67', 'A12', '1', '2', 'TD', 'Face ID', 'ja', 'iPhXR', '1', 'iPhXR', '1', '2', '909', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xr-red.png');
INSERT INTO [dbo].[iPhone] VALUES ('41', 'iPhone XR', 'Blau', '256', 'iPhXR', '194', 'iPhXR', 'IP67', 'A12', '1', '2', 'TD', 'Face ID', 'ja', 'iPhXR', '1', 'iPhXR', '1', '2', '1019', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xr-blue.png');
INSERT INTO [dbo].[iPhone] VALUES ('42', 'iPhone XR', 'Schwarz', '256', 'iPhXR', '194', 'iPhXR', 'IP67', 'A12', '1', '2', 'TD', 'Face ID', 'ja', 'iPhXR', '1', 'iPhXR', '1', '2', '1019', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xr-black.png');
INSERT INTO [dbo].[iPhone] VALUES ('43', 'iPhone XR', 'Koralle', '256', 'iPhXR', '194', 'iPhXR', 'IP67', 'A12', '1', '2', 'TD', 'Face ID', 'ja', 'iPhXR', '1', 'iPhXR', '1', '2', '1019', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xr-coral.png');
INSERT INTO [dbo].[iPhone] VALUES ('44', 'iPhone XR', 'Weiß', '256', 'iPhXR', '194', 'iPhXR', 'IP67', 'A12', '1', '2', 'TD', 'Face ID', 'ja', 'iPhXR', '1', 'iPhXR', '1', '2', '1019', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xr-white.png');
INSERT INTO [dbo].[iPhone] VALUES ('45', 'iPhone XR', 'Gelb', '256', 'iPhXR', '194', 'iPhXR', 'IP67', 'A12', '1', '2', 'TD', 'Face ID', 'ja', 'iPhXR', '1', 'iPhXR', '1', '2', '1019', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xr-yellow.png');
INSERT INTO [dbo].[iPhone] VALUES ('46', 'iPhone XR', 'Rot', '256', 'iPhXR', '194', 'iPhXR', 'IP67', 'A12', '1', '2', 'TD', 'Face ID', 'ja', 'iPhXR', '1', 'iPhXR', '1', '2', '1019', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xr-red.png');
INSERT INTO [dbo].[iPhone] VALUES ('47', 'iPhone XS', 'Gold', '64', 'iPhXS', '177', 'iPhXS', 'IP68', 'A12', '2', '2', 'TD', 'Face ID', 'ja', 'iPhXS', '1', 'iPhXS', '1', '2', '1149', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xs-gold.png');
INSERT INTO [dbo].[iPhone] VALUES ('48', 'iPhone XS', 'Space Grau', '64', 'iPhXS', '177', 'iPhXS', 'IP68', 'A12', '2', '2', 'TD', 'Face ID', 'ja', 'iPhXS', '1', 'iPhXS', '1', '2', '1149', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xs-space.png');
INSERT INTO [dbo].[iPhone] VALUES ('49', 'iPhone XS', 'Silber', '64', 'iPhXS', '177', 'iPhXS', 'IP68', 'A12', '2', '2', 'TD', 'Face ID', 'ja', 'iPhXS', '1', 'iPhXS', '1', '2', '1149', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xs-silver.png');
INSERT INTO [dbo].[iPhone] VALUES ('50', 'iPhone XS', 'Gold', '256', 'iPhXS', '177', 'iPhXS', 'IP68', 'A12', '2', '2', 'TD', 'Face ID', 'ja', 'iPhXS', '1', 'iPhXS', '1', '2', '1319', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xs-gold.png');
INSERT INTO [dbo].[iPhone] VALUES ('51', 'iPhone XS', 'Space Grau', '256', 'iPhXS', '177', 'iPhXS', 'IP68', 'A12', '2', '2', 'TD', 'Face ID', 'ja', 'iPhXS', '1', 'iPhXS', '1', '2', '1319', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xs-space.png');
INSERT INTO [dbo].[iPhone] VALUES ('52', 'iPhone XS', 'Silber', '256', 'iPhXS', '177', 'iPhXS', 'IP68', 'A12', '2', '2', 'TD', 'Face ID', 'ja', 'iPhXS', '1', 'iPhXS', '1', '2', '1319', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xs-silver.png');
INSERT INTO [dbo].[iPhone] VALUES ('53', 'iPhone XS', 'Gold', '512', 'iPhXS', '177', 'iPhXS', 'IP68', 'A12', '2', '2', 'TD', 'Face ID', 'ja', 'iPhXS', '1', 'iPhXS', '1', '2', '1549', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xs-gold.png');
INSERT INTO [dbo].[iPhone] VALUES ('54', 'iPhone XS', 'Space Grau', '512', 'iPhXS', '177', 'iPhXS', 'IP68', 'A12', '2', '2', 'TD', 'Face ID', 'ja', 'iPhXS', '1', 'iPhXS', '1', '2', '1549', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xs-space.png');
INSERT INTO [dbo].[iPhone] VALUES ('55', 'iPhone XS', 'Silber', '512', 'iPhXS', '177', 'iPhXS', 'IP68', 'A12', '2', '2', 'TD', 'Face ID', 'ja', 'iPhXS', '1', 'iPhXS', '1', '2', '1549', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xs-silver.png');
INSERT INTO [dbo].[iPhone] VALUES ('56', 'iPhone XS Max', 'Gold', '64', 'iPhXSMax', '208', 'iPhXSMax', 'IP68', 'A12', '2', '2', 'TD', 'Face ID', 'ja', 'iPhXS', '1', 'iPhXSMax', '1', '2', '1249', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xs-max-gold.png');
INSERT INTO [dbo].[iPhone] VALUES ('57', 'iPhone XS Max', 'Space Grau', '64', 'iPhXSMax', '208', 'iPhXSMax', 'IP68', 'A12', '2', '2', 'TD', 'Face ID', 'ja', 'iPhXS', '1', 'iPhXSMax', '1', '2', '1249', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xs-max-space.png');
INSERT INTO [dbo].[iPhone] VALUES ('58', 'iPhone XS Max', 'Silber', '64', 'iPhXSMax', '208', 'iPhXSMax', 'IP68', 'A12', '2', '2', 'TD', 'Face ID', 'ja', 'iPhXS', '1', 'iPhXSMax', '1', '2', '1249', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xs-max-silver.png');
INSERT INTO [dbo].[iPhone] VALUES ('59', 'iPhone XS Max', 'Gold', '256', 'iPhXSMax', '208', 'iPhXSMax', 'IP68', 'A12', '2', '2', 'TD', 'Face ID', 'ja', 'iPhXS', '1', 'iPhXSMax', '1', '2', '1419', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xs-max-gold.png');
INSERT INTO [dbo].[iPhone] VALUES ('60', 'iPhone XS Max', 'Space Grau', '256', 'iPhXSMax', '208', 'iPhXSMax', 'IP68', 'A12', '2', '2', 'TD', 'Face ID', 'ja', 'iPhXS', '1', 'iPhXSMax', '1', '2', '1419', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xs-max-space.png');
INSERT INTO [dbo].[iPhone] VALUES ('61', 'iPhone XS Max', 'Silber', '256', 'iPhXSMax', '208', 'iPhXSMax', 'IP68', 'A12', '2', '2', 'TD', 'Face ID', 'ja', 'iPhXS', '1', 'iPhXSMax', '1', '2', '1419', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xs-max-silver.png');
INSERT INTO [dbo].[iPhone] VALUES ('62', 'iPhone XS Max', 'Gold', '512', 'iPhXSMax', '208', 'iPhXSMax', 'IP68', 'A12', '2', '2', 'TD', 'Face ID', 'ja', 'iPhXS', '1', 'iPhXSMax', '1', '2', '1649', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xs-max-gold.png');
INSERT INTO [dbo].[iPhone] VALUES ('63', 'iPhone XS Max', 'Space Grau', '512', 'iPhXSMax', '208', 'iPhXSMax', 'IP68', 'A12', '2', '2', 'TD', 'Face ID', 'ja', 'iPhXS', '1', 'iPhXSMax', '1', '2', '1649', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xs-max-space.png');
INSERT INTO [dbo].[iPhone] VALUES ('64', 'iPhone XS Max', 'Silber', '512', 'iPhXSMax', '208', 'iPhXSMax', 'IP68', 'A12', '2', '2', 'TD', 'Face ID', 'ja', 'iPhXS', '1', 'iPhXSMax', '1', '2', '1649', 'https://bscchatbotblob.blob.core.windows.net/images/iphone-xs-max-silver.png');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[iPad_VideoRecording]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[iPad_VideoRecording]') AND type IN ('U'))
	DROP TABLE [dbo].[iPad_VideoRecording]
GO
CREATE TABLE [dbo].[iPad_VideoRecording] (
	[ID] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[x4K_30fps] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[x4K_60fps] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[x1080p_30fps] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[x1080p_60fps] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[x720p_30fps] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__iPhone_V__3214EC27B7A23D78] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[iPad_VideoRecording]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[iPad_VideoRecording] VALUES ('1', 'ja', 'nein', 'ja', 'ja', 'ja');
INSERT INTO [dbo].[iPad_VideoRecording] VALUES ('2', 'ja', 'ja', 'ja', 'ja', 'ja');
INSERT INTO [dbo].[iPad_VideoRecording] VALUES ('3', 'nein', 'nein', 'ja', 'ja', 'ja');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[iPad_FrontCamera]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[iPad_FrontCamera]') AND type IN ('U'))
	DROP TABLE [dbo].[iPad_FrontCamera]
GO
CREATE TABLE [dbo].[iPad_FrontCamera] (
	[ID] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Megapixel] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Aperture] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Name] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__iPhone_F__3214EC27A90AAE92] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[iPad_FrontCamera]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[iPad_FrontCamera] VALUES ('FT', '7', '2,2', 'FaceTime HD Kamera');
INSERT INTO [dbo].[iPad_FrontCamera] VALUES ('FTlow', '1.2', '2,2', 'FaceTime HD Kamera');
INSERT INTO [dbo].[iPad_FrontCamera] VALUES ('TD', '7', '2,2', 'TrueDepth Kamera');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[iPhone_Size]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[iPhone_Size]') AND type IN ('U'))
	DROP TABLE [dbo].[iPhone_Size]
GO
CREATE TABLE [dbo].[iPhone_Size] (
	[ID] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Height] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Depth] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Width] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__iPhone_S__3214EC27E09B1B71] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[iPhone_Size]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[iPhone_Size] VALUES ('iPh7', '138,3 mm', '7,1 mm', '67,1 mm');
INSERT INTO [dbo].[iPhone_Size] VALUES ('iPh7Plus', '158,2 mm', '7,3 mm', '77,9 mm');
INSERT INTO [dbo].[iPhone_Size] VALUES ('iPh8', '138,4 mm', '7,3 mm', '67,3 mm');
INSERT INTO [dbo].[iPhone_Size] VALUES ('iPh8Plus', '158,4 mm', '7,5 mm', '78,1 mm');
INSERT INTO [dbo].[iPhone_Size] VALUES ('iPhXR', '150,9 mm', '8,3 mm', '75,7 mm');
INSERT INTO [dbo].[iPhone_Size] VALUES ('iPhXS', '143,6 mm', '7,7 mm', '70,9 mm');
INSERT INTO [dbo].[iPhone_Size] VALUES ('iPhXSMax', '157,7 mm', '7,7 mm', '77,4 mm');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[iPad_Wireless]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[iPad_Wireless]') AND type IN ('U'))
	DROP TABLE [dbo].[iPad_Wireless]
GO
CREATE TABLE [dbo].[iPad_Wireless] (
	[ID] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[LTE] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[WLAN] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Bluetooth] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__iPhone_W__3214EC27605CA56F] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[iPad_Wireless]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[iPad_Wireless] VALUES ('iPad', 'LTE', 'WLAN (802.11a/b/g/n/ac) mit MIMO', '4.2');
INSERT INTO [dbo].[iPad_Wireless] VALUES ('iPadM', 'LTE', 'WLAN (802.11a/b/g/n/ac) mit MIMO', '4.2');
INSERT INTO [dbo].[iPad_Wireless] VALUES ('iPadP10', 'LTE Advanced', 'WLAN (802.11a/b/g/n/ac) mit MIMO', '4.2');
INSERT INTO [dbo].[iPad_Wireless] VALUES ('iPadP11', 'Gigabit fähiges LTE', 'WLAN (802.11a/b/g/n/ac) mit MIMO', '5.0');
INSERT INTO [dbo].[iPad_Wireless] VALUES ('iPadP12', 'Gigabit fähiges LTE', 'WLAN (802.11a/b/g/n/ac) mit MIMO', '5.0');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[iPhone_Display]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[iPhone_Display]') AND type IN ('U'))
	DROP TABLE [dbo].[iPhone_Display]
GO
CREATE TABLE [dbo].[iPhone_Display] (
	[ID] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Size] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Technology] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[HDR] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Resolution] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PPI] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Contrast] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TrueTone] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ThreeDTouch] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MaxBrightness] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__iPhone_D__3214EC272CD1B292] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[iPhone_Display]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[iPhone_Display] VALUES ('iPh7', '4.7', 'LCD', 'nein', '1334 x 750', '326', '1400:1', 'nein', 'nein', '625');
INSERT INTO [dbo].[iPhone_Display] VALUES ('iPh7Plus', '5.5', 'LCD', 'nein', '1920 x 1080', '401', '1300:1', 'nein', 'nein', '625');
INSERT INTO [dbo].[iPhone_Display] VALUES ('iPh8', '4.7', 'LCD', 'nein', '1334 x 750', '326', '1400:1', 'nein', 'nein', '625');
INSERT INTO [dbo].[iPhone_Display] VALUES ('iPh8Plus', '5.5', 'LCD', 'nein', '1920 x 1080', '401', '1300:1', 'nein', 'nein', '625');
INSERT INTO [dbo].[iPhone_Display] VALUES ('iPhXR', '6.1', 'LCD', 'nein', '1792 x 828', '326', '1400:1', 'ja', 'nein', '625 ');
INSERT INTO [dbo].[iPhone_Display] VALUES ('iPhXS', '5.8', 'OLED', 'ja', '2436 x 1125', '458', '1.000.000:1', 'ja', 'ja', '625');
INSERT INTO [dbo].[iPhone_Display] VALUES ('iPhXSMax', '6.5', 'OLED', 'ja', '2688 x 1242', '458', '1.000.000:1', 'ja', 'ja', '625');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[iPad_Locating]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[iPad_Locating]') AND type IN ('U'))
	DROP TABLE [dbo].[iPad_Locating]
GO
CREATE TABLE [dbo].[iPad_Locating] (
	[ID] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[GPS] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DigitalCompass] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[WLAN] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Mobile] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[iBeacon] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__iPhone_L__3214EC27BC1E2EC3] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[iPad_Locating]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[iPad_Locating] VALUES ('1', 'ja', 'ja', 'ja', 'ja', 'ja');
INSERT INTO [dbo].[iPad_Locating] VALUES ('2', 'nein', 'ja', 'ja', 'nein', 'ja');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[iPhone_IPProtection]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[iPhone_IPProtection]') AND type IN ('U'))
	DROP TABLE [dbo].[iPhone_IPProtection]
GO
CREATE TABLE [dbo].[iPhone_IPProtection] (
	[ID] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Duration] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Depth] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__iPhone_I__3214EC27F3588868] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[iPhone_IPProtection]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[iPhone_IPProtection] VALUES ('IP67', '30 Minuten', '1 m');
INSERT INTO [dbo].[iPhone_IPProtection] VALUES ('IP68', '30 Minuten', '2 m');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[iPad_PowerSupply]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[iPad_PowerSupply]') AND type IN ('U'))
	DROP TABLE [dbo].[iPad_PowerSupply]
GO
CREATE TABLE [dbo].[iPad_PowerSupply] (
	[ID] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[InternetUse] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[WirelessVideoPlayback] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[WirelessAudioPlayback] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__iPhone_P__3214EC27A93DDA53] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[iPad_PowerSupply]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[iPad_PowerSupply] VALUES ('iPad', '10 Stunden', '10 Stunden', '10 Stunden');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[iPhone_Processor]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[iPhone_Processor]') AND type IN ('U'))
	DROP TABLE [dbo].[iPhone_Processor]
GO
CREATE TABLE [dbo].[iPhone_Processor] (
	[ID] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Name] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NeuralEngine] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__iPhone_P__3214EC27C626C5EA] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[iPhone_Processor]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[iPhone_Processor] VALUES ('A10', 'A10 Fusion Chip', 'nein');
INSERT INTO [dbo].[iPhone_Processor] VALUES ('A11', 'A11 Bionic Chip', 'ja');
INSERT INTO [dbo].[iPhone_Processor] VALUES ('A12', 'A12 Bionic Chip', 'ja');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[iPad_Sensors]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[iPad_Sensors]') AND type IN ('U'))
	DROP TABLE [dbo].[iPad_Sensors]
GO
CREATE TABLE [dbo].[iPad_Sensors] (
	[ID] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Barometer] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ThreeAxisGyro] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Accelerometer] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AmbientLightSensor] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__iPhone_S__3214EC279C2642F4] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[iPad_Sensors]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[iPad_Sensors] VALUES ('1', 'ja', 'ja', 'ja', 'ja');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[iPhone_Camera]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[iPhone_Camera]') AND type IN ('U'))
	DROP TABLE [dbo].[iPhone_Camera]
GO
CREATE TABLE [dbo].[iPhone_Camera] (
	[ID] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Megapixel] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[WideAngelAperture] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TelephotoLensAperture] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Aperture] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__iPhone_C__3214EC278619565C] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[iPhone_Camera]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[iPhone_Camera] VALUES ('1', '12', null, null, '1,8');
INSERT INTO [dbo].[iPhone_Camera] VALUES ('2', '12', '1,8', '2,8', null);
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[iPad_Sim]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[iPad_Sim]') AND type IN ('U'))
	DROP TABLE [dbo].[iPad_Sim]
GO
CREATE TABLE [dbo].[iPad_Sim] (
	[ID] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[NanoSim] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ESim] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__iPhone_S__3214EC2796E92430] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[iPad_Sim]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[iPad_Sim] VALUES ('1', 'ja', 'ja');
INSERT INTO [dbo].[iPad_Sim] VALUES ('2', 'ja', 'nein');
INSERT INTO [dbo].[iPad_Sim] VALUES ('3', 'nein', 'nein');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[iPhone_VideoRecording]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[iPhone_VideoRecording]') AND type IN ('U'))
	DROP TABLE [dbo].[iPhone_VideoRecording]
GO
CREATE TABLE [dbo].[iPhone_VideoRecording] (
	[ID] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[x4K_24fps] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[x4K_30fps] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[x4K_60fps] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[x1080p_30fps] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[x1080p_60fps] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[x720p_30fps] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__iPhone_V__3214EC273B22EE14] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[iPhone_VideoRecording]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[iPhone_VideoRecording] VALUES ('1', 'nein', 'ja', 'nein', 'ja', 'ja', 'ja');
INSERT INTO [dbo].[iPhone_VideoRecording] VALUES ('2', 'ja', 'ja', 'ja', 'ja', 'ja', 'ja');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[iPhone_FrontCamera]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[iPhone_FrontCamera]') AND type IN ('U'))
	DROP TABLE [dbo].[iPhone_FrontCamera]
GO
CREATE TABLE [dbo].[iPhone_FrontCamera] (
	[ID] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Megapixel] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Aperture] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Name] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__iPhone_F__3214EC27982CA774] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[iPhone_FrontCamera]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[iPhone_FrontCamera] VALUES ('FT', '7', '2,2', 'FaceTime HD Kamera');
INSERT INTO [dbo].[iPhone_FrontCamera] VALUES ('TD', '7', '2,2', 'TrueDepth Kamera');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[iPhone_Wireless]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[iPhone_Wireless]') AND type IN ('U'))
	DROP TABLE [dbo].[iPhone_Wireless]
GO
CREATE TABLE [dbo].[iPhone_Wireless] (
	[ID] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[LTE] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[WLAN] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Bluetooth] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NFC] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ExpressCard] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__iPhone_W__3214EC274DA0E35D] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[iPhone_Wireless]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[iPhone_Wireless] VALUES ('iPh7', 'LTE', '802.11ac WLAN mit MIMO', '4.2', 'ja', 'ja');
INSERT INTO [dbo].[iPhone_Wireless] VALUES ('iPh8', 'LTE', '802.11ac WLAN mit MIMO', '5.0', 'ja', 'ja');
INSERT INTO [dbo].[iPhone_Wireless] VALUES ('iPhXR', 'LTE Advanced', '802.11ac WLAN mit 2x2 MIMO', '5.0', 'ja', 'ja');
INSERT INTO [dbo].[iPhone_Wireless] VALUES ('iPhXS', 'Gigabit fähiges LTE mit 4x4 MIMO und LAA', '802.11ac WLAN mit 2x2 MIMO', '5.0', 'ja', 'ja');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[iPhone_Locating]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[iPhone_Locating]') AND type IN ('U'))
	DROP TABLE [dbo].[iPhone_Locating]
GO
CREATE TABLE [dbo].[iPhone_Locating] (
	[ID] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[GPS] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DigitalCompass] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[WLAN] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Mobile] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[iBeacon] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__iPhone_L__3214EC276F473CBA] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[iPhone_Locating]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[iPhone_Locating] VALUES ('1', 'ja', 'ja', 'ja', 'ja', 'ja');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[iPhone_PowerSupply]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[iPhone_PowerSupply]') AND type IN ('U'))
	DROP TABLE [dbo].[iPhone_PowerSupply]
GO
CREATE TABLE [dbo].[iPhone_PowerSupply] (
	[ID] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Standby] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InternetUse] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[WirelessVideoPlayback] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[WirelessAudioPlayback] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TalkTime] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__iPhone_P__3214EC27D1983D57] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[iPhone_PowerSupply]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[iPhone_PowerSupply] VALUES ('iPh7', '10 Tage', '12 Stunden', '13 Stunden', '40 Stunden', '14 Stunden');
INSERT INTO [dbo].[iPhone_PowerSupply] VALUES ('iPh7Plus', '16 Stunden', '13 Stunden', '40 Stunden', '60 Stunden', '21 Stunden');
INSERT INTO [dbo].[iPhone_PowerSupply] VALUES ('iPh8', null, '12 Stunden', '13 Stunden', '40 Stunden', '14 Stunden');
INSERT INTO [dbo].[iPhone_PowerSupply] VALUES ('iPh8Plus', null, '13 Stunden', '14 Stunden', '60 Stunden', '21 Stunden');
INSERT INTO [dbo].[iPhone_PowerSupply] VALUES ('iphXR', null, '15 Stunden', '16 Stunden', '65 Stunden', '25 Stunden');
INSERT INTO [dbo].[iPhone_PowerSupply] VALUES ('iphXS', null, '12 Stunden', '14 Stunden', '60 Stunden', '20 Stunden');
INSERT INTO [dbo].[iPhone_PowerSupply] VALUES ('iPhXSMax', null, '13 Stunden', '15 Stunden', '65 Stunden', '25 Stunden');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[iPhone_Sensors]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[iPhone_Sensors]') AND type IN ('U'))
	DROP TABLE [dbo].[iPhone_Sensors]
GO
CREATE TABLE [dbo].[iPhone_Sensors] (
	[ID] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Barometer] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ThreeAxisGyro] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Accelerometer] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ProximitySensor] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AmbientLightSensor] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__iPhone_S__3214EC271C26A272] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[iPhone_Sensors]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[iPhone_Sensors] VALUES ('1', 'ja', 'ja', 'ja', 'ja', 'ja');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[iPhone_Sim]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[iPhone_Sim]') AND type IN ('U'))
	DROP TABLE [dbo].[iPhone_Sim]
GO
CREATE TABLE [dbo].[iPhone_Sim] (
	[ID] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[NanoSim] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ESim] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__iPhone_S__3214EC27AB7A16AD] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[iPhone_Sim]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[iPhone_Sim] VALUES ('1', 'ja', 'nein');
INSERT INTO [dbo].[iPhone_Sim] VALUES ('2', 'ja', 'ja');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[Mac_copy]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[Mac_copy]') AND type IN ('U'))
	DROP TABLE [dbo].[Mac_copy]
GO
CREATE TABLE [dbo].[Mac_copy] (
	[ID] int NOT NULL,
	[Name] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Color] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Display] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Capacity] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Size] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Weight] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Processor] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[RAM] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Graphik] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Ports] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[KeyboardTrackpad] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Wireless] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PowerSupply] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Price] int NULL,
	CONSTRAINT [PK__Mac_copy__3214EC27D83DA091] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[Mac_copy]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[Mac_copy] VALUES ('1', 'MacBook Pro', 'Space Grau', 'MBP13', '128 GB', 'MBP13', '1,37 KG', 'MBP13_1', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '1499');
INSERT INTO [dbo].[Mac_copy] VALUES ('2', 'MacBook Pro', 'Space Grau', 'MBP13', '256 GB', 'MBP13', '1.37 KG', 'MBP13_1', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '1749');
INSERT INTO [dbo].[Mac_copy] VALUES ('3', 'MacBook Pro', 'Space Grau', 'MBP13', '512 GB', 'MBP13', '1.37 KG', 'MBP13_1', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '1999');
INSERT INTO [dbo].[Mac_copy] VALUES ('4', 'MacBook Pro', 'Space Grau', 'MBP13', '1 TB', 'MBP13', '1.37 KG', 'MBP13_1', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2499');
INSERT INTO [dbo].[Mac_copy] VALUES ('5', 'MacBook Pro', 'Space Grau', 'MBP13', '128 GB', 'MBP13', '1.37 KG', 'MBP13_1', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '1739');
INSERT INTO [dbo].[Mac_copy] VALUES ('6', 'MacBook Pro', 'Space Grau', 'MBP13', '256 GB', 'MBP13', '1.37 KG', 'MBP13_1', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '1989');
INSERT INTO [dbo].[Mac_copy] VALUES ('7', 'MacBook Pro', 'Space Grau', 'MBP13', '512 GB', 'MBP13', '1.37 KG', 'MBP13_1', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2239');
INSERT INTO [dbo].[Mac_copy] VALUES ('8', 'MacBook Pro', 'Space Grau', 'MBP13', '1 TB', 'MBP13', '1.37 KG', 'MBP13_1', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2739');
INSERT INTO [dbo].[Mac_copy] VALUES ('9', 'MacBook Pro', 'Space Grau', 'MBP13', '128 GB', 'MBP13', '1.37 KG', 'MBP13_2', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '1859');
INSERT INTO [dbo].[Mac_copy] VALUES ('10', 'MacBook Pro', 'Space Grau', 'MBP13', '256 GB', 'MBP13', '1.37 KG', 'MBP13_2', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2109');
INSERT INTO [dbo].[Mac_copy] VALUES ('11', 'MacBook Pro', 'Space Grau', 'MBP13', '512 GB', 'MBP13', '1.37 KG', 'MBP13_2', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2359');
INSERT INTO [dbo].[Mac_copy] VALUES ('12', 'MacBook Pro', 'Space Grau', 'MBP13', '1 TB', 'MBP13', '1.37 KG', 'MBP13_2', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2859');
INSERT INTO [dbo].[Mac_copy] VALUES ('13', 'MacBook Pro', 'Space Grau', 'MBP13', '128 GB', 'MBP13', '1.37 KG', 'MBP13_2', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2099');
INSERT INTO [dbo].[Mac_copy] VALUES ('14', 'MacBook Pro', 'Space Grau', 'MBP13', '256 GB', 'MBP13', '1.37 KG', 'MBP13_2', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2349');
INSERT INTO [dbo].[Mac_copy] VALUES ('15', 'MacBook Pro', 'Space Grau', 'MBP13', '512 GB', 'MBP13', '1.37 KG', 'MBP13_2', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2599');
INSERT INTO [dbo].[Mac_copy] VALUES ('16', 'MacBook Pro', 'Space Grau', 'MBP13', '1 TB', 'MBP13', '1.37 KG', 'MBP13_2', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '3099');
INSERT INTO [dbo].[Mac_copy] VALUES ('17', 'MacBook Pro', 'Silber', 'MBP13', '128 GB', 'MBP13', '1.37 KG', 'MBP13_1', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '1499');
INSERT INTO [dbo].[Mac_copy] VALUES ('18', 'MacBook Pro', 'Silber', 'MBP13', '256 GB', 'MBP13', '1.37 KG', 'MBP13_1', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '1749');
INSERT INTO [dbo].[Mac_copy] VALUES ('19', 'MacBook Pro', 'Silber', 'MBP13', '512 GB', 'MBP13', '1.37 KG', 'MBP13_1', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '1999');
INSERT INTO [dbo].[Mac_copy] VALUES ('20', 'MacBook Pro', 'Silber', 'MBP13', '1 TB', 'MBP13', '1.37 KG', 'MBP13_1', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2499');
INSERT INTO [dbo].[Mac_copy] VALUES ('21', 'MacBook Pro', 'Silber', 'MBP13', '128 GB', 'MBP13', '1.37 KG', 'MBP13_1', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '1739');
INSERT INTO [dbo].[Mac_copy] VALUES ('22', 'MacBook Pro', 'Silber', 'MBP13', '256 GB', 'MBP13', '1.37 KG', 'MBP13_1', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '1989');
INSERT INTO [dbo].[Mac_copy] VALUES ('23', 'MacBook Pro', 'Silber', 'MBP13', '512 GB', 'MBP13', '1.37 KG', 'MBP13_1', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2239');
INSERT INTO [dbo].[Mac_copy] VALUES ('24', 'MacBook Pro', 'Silber', 'MBP13', '1 TB', 'MBP13', '1.37 KG', 'MBP13_1', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2739');
INSERT INTO [dbo].[Mac_copy] VALUES ('25', 'MacBook Pro', 'Silber', 'MBP13', '128 GB', 'MBP13', '1.37 KG', 'MBP13_2', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '1859');
INSERT INTO [dbo].[Mac_copy] VALUES ('26', 'MacBook Pro', 'Silber', 'MBP13', '256 GB', 'MBP13', '1.37 KG', 'MBP13_2', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2109');
INSERT INTO [dbo].[Mac_copy] VALUES ('27', 'MacBook Pro', 'Silber', 'MBP13', '512 GB', 'MBP13', '1.37 KG', 'MBP13_2', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2359');
INSERT INTO [dbo].[Mac_copy] VALUES ('28', 'MacBook Pro', 'Silber', 'MBP13', '1 TB', 'MBP13', '1.37 KG', 'MBP13_2', '8 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2859');
INSERT INTO [dbo].[Mac_copy] VALUES ('29', 'MacBook Pro', 'Silber', 'MBP13', '128 GB', 'MBP13', '1.37 KG', 'MBP13_2', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2099');
INSERT INTO [dbo].[Mac_copy] VALUES ('30', 'MacBook Pro', 'Silber', 'MBP13', '256 GB', 'MBP13', '1.37 KG', 'MBP13_2', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2349');
INSERT INTO [dbo].[Mac_copy] VALUES ('31', 'MacBook Pro', 'Silber', 'MBP13', '512 GB', 'MBP13', '1.37 KG', 'MBP13_2', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '2599');
INSERT INTO [dbo].[Mac_copy] VALUES ('32', 'MacBook Pro', 'Silber', 'MBP13', '1 TB', 'MBP13', '1.37 KG', 'MBP13_2', '16 GB', 'Intel Iris Plus Graphics 640', 'MBP13', 'MBP13', 'MBP13', 'MBP13', '3099');
INSERT INTO [dbo].[Mac_copy] VALUES ('33', 'MacBook Pro', 'Space Grau', 'MBP13TB', '256 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '1999');
INSERT INTO [dbo].[Mac_copy] VALUES ('34', 'MacBook Pro', 'Space Grau', 'MBP13TB', '512 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2249');
INSERT INTO [dbo].[Mac_copy] VALUES ('35', 'MacBook Pro', 'Space Grau', 'MBP13TB', '1 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2749');
INSERT INTO [dbo].[Mac_copy] VALUES ('36', 'MacBook Pro', 'Space Grau', 'MBP13TB', '2 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '3749');
INSERT INTO [dbo].[Mac_copy] VALUES ('37', 'MacBook Pro', 'Space Grau', 'MBP13TB', '256 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2239');
INSERT INTO [dbo].[Mac_copy] VALUES ('38', 'MacBook Pro', 'Space Grau', 'MBP13TB', '512 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2489');
INSERT INTO [dbo].[Mac_copy] VALUES ('39', 'MacBook Pro', 'Space Grau', 'MBP13TB', '1 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2989');
INSERT INTO [dbo].[Mac_copy] VALUES ('40', 'MacBook Pro', 'Space Grau', 'MBP13TB', '2 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '3989');
INSERT INTO [dbo].[Mac_copy] VALUES ('41', 'MacBook Pro', 'Space Grau', 'MBP13TB', '256 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2359');
INSERT INTO [dbo].[Mac_copy] VALUES ('42', 'MacBook Pro', 'Space Grau', 'MBP13TB', '512 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2609');
INSERT INTO [dbo].[Mac_copy] VALUES ('43', 'MacBook Pro', 'Space Grau', 'MBP13TB', '1 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '3109');
INSERT INTO [dbo].[Mac_copy] VALUES ('44', 'MacBook Pro', 'Space Grau', 'MBP13TB', '2 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '4109');
INSERT INTO [dbo].[Mac_copy] VALUES ('45', 'MacBook Pro', 'Space Grau', 'MBP13TB', '256 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2599');
INSERT INTO [dbo].[Mac_copy] VALUES ('46', 'MacBook Pro', 'Space Grau', 'MBP13TB', '512 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2849');
INSERT INTO [dbo].[Mac_copy] VALUES ('47', 'MacBook Pro', 'Space Grau', 'MBP13TB', '1 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '3349');
INSERT INTO [dbo].[Mac_copy] VALUES ('48', 'MacBook Pro', 'Space Grau', 'MBP13TB', '2 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '4349');
INSERT INTO [dbo].[Mac_copy] VALUES ('49', 'MacBook Pro', 'Silber', 'MBP13TB', '256 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '1999');
INSERT INTO [dbo].[Mac_copy] VALUES ('50', 'MacBook Pro', 'Silber', 'MBP13TB', '512 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2249');
INSERT INTO [dbo].[Mac_copy] VALUES ('51', 'MacBook Pro', 'Silber', 'MBP13TB', '1 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2749');
INSERT INTO [dbo].[Mac_copy] VALUES ('52', 'MacBook Pro', 'Silber', 'MBP13TB', '2 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '3749');
INSERT INTO [dbo].[Mac_copy] VALUES ('53', 'MacBook Pro', 'Silber', 'MBP13TB', '256 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2239');
INSERT INTO [dbo].[Mac_copy] VALUES ('54', 'MacBook Pro', 'Silber', 'MBP13TB', '512 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2489');
INSERT INTO [dbo].[Mac_copy] VALUES ('55', 'MacBook Pro', 'Silber', 'MBP13TB', '1 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2989');
INSERT INTO [dbo].[Mac_copy] VALUES ('56', 'MacBook Pro', 'Silber', 'MBP13TB', '2 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_1', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '3989');
INSERT INTO [dbo].[Mac_copy] VALUES ('57', 'MacBook Pro', 'Silber', 'MBP13TB', '256 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2359');
INSERT INTO [dbo].[Mac_copy] VALUES ('58', 'MacBook Pro', 'Silber', 'MBP13TB', '512 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2609');
INSERT INTO [dbo].[Mac_copy] VALUES ('59', 'MacBook Pro', 'Silber', 'MBP13TB', '1 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '3109');
INSERT INTO [dbo].[Mac_copy] VALUES ('60', 'MacBook Pro', 'Silber', 'MBP13TB', '2 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '8 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '4109');
INSERT INTO [dbo].[Mac_copy] VALUES ('61', 'MacBook Pro', 'Silber', 'MBP13TB', '256 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2599');
INSERT INTO [dbo].[Mac_copy] VALUES ('62', 'MacBook Pro', 'Silber', 'MBP13TB', '512 GB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '2849');
INSERT INTO [dbo].[Mac_copy] VALUES ('63', 'MacBook Pro', 'Silber', 'MBP13TB', '1 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '3349');
INSERT INTO [dbo].[Mac_copy] VALUES ('64', 'MacBook Pro', 'Silber', 'MBP13TB', '2 TB', 'MBP13TB', '1.37 KG', 'MBP13TB_2', '16 GB', 'Intel Iris Plus Graphics 655', 'MBP13TB', 'MBP13TB', 'MBP13TB', 'MBP13TB', '4349');
INSERT INTO [dbo].[Mac_copy] VALUES ('65', 'MacBook Pro', 'Space Grau', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '2799');
INSERT INTO [dbo].[Mac_copy] VALUES ('66', 'MacBook Pro', 'Space Grau', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3039');
INSERT INTO [dbo].[Mac_copy] VALUES ('67', 'MacBook Pro', 'Space Grau', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3519');
INSERT INTO [dbo].[Mac_copy] VALUES ('68', 'MacBook Pro', 'Space Grau', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4479');
INSERT INTO [dbo].[Mac_copy] VALUES ('69', 'MacBook Pro', 'Space Grau', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '6879');
INSERT INTO [dbo].[Mac_copy] VALUES ('70', 'MacBook Pro', 'Space Grau', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '2919');
INSERT INTO [dbo].[Mac_copy] VALUES ('71', 'MacBook Pro', 'Space Grau', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3159');
INSERT INTO [dbo].[Mac_copy] VALUES ('72', 'MacBook Pro', 'Space Grau', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3639');
INSERT INTO [dbo].[Mac_copy] VALUES ('73', 'MacBook Pro', 'Space Grau', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4599');
INSERT INTO [dbo].[Mac_copy] VALUES ('74', 'MacBook Pro', 'Space Grau', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '6999');
INSERT INTO [dbo].[Mac_copy] VALUES ('75', 'MacBook Pro', 'Space Grau', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3279');
INSERT INTO [dbo].[Mac_copy] VALUES ('76', 'MacBook Pro', 'Space Grau', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3519');
INSERT INTO [dbo].[Mac_copy] VALUES ('77', 'MacBook Pro', 'Space Grau', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3999');
INSERT INTO [dbo].[Mac_copy] VALUES ('78', 'MacBook Pro', 'Space Grau', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4959');
INSERT INTO [dbo].[Mac_copy] VALUES ('79', 'MacBook Pro', 'Space Grau', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '7359');
INSERT INTO [dbo].[Mac_copy] VALUES ('80', 'MacBook Pro', 'Space Grau', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3399');
INSERT INTO [dbo].[Mac_copy] VALUES ('81', 'MacBook Pro', 'Space Grau', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3639');
INSERT INTO [dbo].[Mac_copy] VALUES ('82', 'MacBook Pro', 'Space Grau', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4119');
INSERT INTO [dbo].[Mac_copy] VALUES ('83', 'MacBook Pro', 'Space Grau', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '5079');
INSERT INTO [dbo].[Mac_copy] VALUES ('84', 'MacBook Pro', 'Space Grau', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '7479');
INSERT INTO [dbo].[Mac_copy] VALUES ('85', 'MacBook Pro', 'Space Grau', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3279');
INSERT INTO [dbo].[Mac_copy] VALUES ('86', 'MacBook Pro', 'Space Grau', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3519');
INSERT INTO [dbo].[Mac_copy] VALUES ('87', 'MacBook Pro', 'Space Grau', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3999');
INSERT INTO [dbo].[Mac_copy] VALUES ('88', 'MacBook Pro', 'Space Grau', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4959');
INSERT INTO [dbo].[Mac_copy] VALUES ('89', 'MacBook Pro', 'Space Grau', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '7359');
INSERT INTO [dbo].[Mac_copy] VALUES ('90', 'MacBook Pro', 'Space Grau', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3399');
INSERT INTO [dbo].[Mac_copy] VALUES ('91', 'MacBook Pro', 'Space Grau', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3639');
INSERT INTO [dbo].[Mac_copy] VALUES ('92', 'MacBook Pro', 'Space Grau', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4119');
INSERT INTO [dbo].[Mac_copy] VALUES ('93', 'MacBook Pro', 'Space Grau', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '5079');
INSERT INTO [dbo].[Mac_copy] VALUES ('94', 'MacBook Pro', 'Space Grau', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '7479');
INSERT INTO [dbo].[Mac_copy] VALUES ('95', 'MacBook Pro', 'Space Grau', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3759');
INSERT INTO [dbo].[Mac_copy] VALUES ('96', 'MacBook Pro', 'Space Grau', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3999');
INSERT INTO [dbo].[Mac_copy] VALUES ('97', 'MacBook Pro', 'Space Grau', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4479');
INSERT INTO [dbo].[Mac_copy] VALUES ('98', 'MacBook Pro', 'Space Grau', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '5439');
INSERT INTO [dbo].[Mac_copy] VALUES ('99', 'MacBook Pro', 'Space Grau', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '7839');
INSERT INTO [dbo].[Mac_copy] VALUES ('100', 'MacBook Pro', 'Space Grau', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3879');
INSERT INTO [dbo].[Mac_copy] VALUES ('101', 'MacBook Pro', 'Space Grau', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4119');
INSERT INTO [dbo].[Mac_copy] VALUES ('102', 'MacBook Pro', 'Space Grau', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4599');
INSERT INTO [dbo].[Mac_copy] VALUES ('103', 'MacBook Pro', 'Space Grau', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '5559');
INSERT INTO [dbo].[Mac_copy] VALUES ('104', 'MacBook Pro', 'Space Grau', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '7959');
INSERT INTO [dbo].[Mac_copy] VALUES ('105', 'MacBook Pro', 'Silber', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '2799');
INSERT INTO [dbo].[Mac_copy] VALUES ('106', 'MacBook Pro', 'Silber', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3039');
INSERT INTO [dbo].[Mac_copy] VALUES ('107', 'MacBook Pro', 'Silber', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3519');
INSERT INTO [dbo].[Mac_copy] VALUES ('108', 'MacBook Pro', 'Silber', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4479');
INSERT INTO [dbo].[Mac_copy] VALUES ('109', 'MacBook Pro', 'Silber', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '6879');
INSERT INTO [dbo].[Mac_copy] VALUES ('110', 'MacBook Pro', 'Silber', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '2919');
INSERT INTO [dbo].[Mac_copy] VALUES ('111', 'MacBook Pro', 'Silber', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3159');
INSERT INTO [dbo].[Mac_copy] VALUES ('112', 'MacBook Pro', 'Silber', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3639');
INSERT INTO [dbo].[Mac_copy] VALUES ('113', 'MacBook Pro', 'Silber', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4599');
INSERT INTO [dbo].[Mac_copy] VALUES ('114', 'MacBook Pro', 'Silber', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_1', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '6999');
INSERT INTO [dbo].[Mac_copy] VALUES ('115', 'MacBook Pro', 'Silber', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3279');
INSERT INTO [dbo].[Mac_copy] VALUES ('116', 'MacBook Pro', 'Silber', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3519');
INSERT INTO [dbo].[Mac_copy] VALUES ('117', 'MacBook Pro', 'Silber', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3999');
INSERT INTO [dbo].[Mac_copy] VALUES ('118', 'MacBook Pro', 'Silber', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4959');
INSERT INTO [dbo].[Mac_copy] VALUES ('119', 'MacBook Pro', 'Silber', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '7359');
INSERT INTO [dbo].[Mac_copy] VALUES ('120', 'MacBook Pro', 'Silber', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3399');
INSERT INTO [dbo].[Mac_copy] VALUES ('121', 'MacBook Pro', 'Silber', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3639');
INSERT INTO [dbo].[Mac_copy] VALUES ('122', 'MacBook Pro', 'Silber', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4119');
INSERT INTO [dbo].[Mac_copy] VALUES ('123', 'MacBook Pro', 'Silber', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '5079');
INSERT INTO [dbo].[Mac_copy] VALUES ('124', 'MacBook Pro', 'Silber', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_1', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '7479');
INSERT INTO [dbo].[Mac_copy] VALUES ('125', 'MacBook Pro', 'Silber', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3279');
INSERT INTO [dbo].[Mac_copy] VALUES ('126', 'MacBook Pro', 'Silber', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3519');
INSERT INTO [dbo].[Mac_copy] VALUES ('127', 'MacBook Pro', 'Silber', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3999');
INSERT INTO [dbo].[Mac_copy] VALUES ('128', 'MacBook Pro', 'Silber', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4959');
INSERT INTO [dbo].[Mac_copy] VALUES ('129', 'MacBook Pro', 'Silber', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '7359');
INSERT INTO [dbo].[Mac_copy] VALUES ('130', 'MacBook Pro', 'Silber', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3399');
INSERT INTO [dbo].[Mac_copy] VALUES ('131', 'MacBook Pro', 'Silber', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3639');
INSERT INTO [dbo].[Mac_copy] VALUES ('132', 'MacBook Pro', 'Silber', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4119');
INSERT INTO [dbo].[Mac_copy] VALUES ('133', 'MacBook Pro', 'Silber', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '5079');
INSERT INTO [dbo].[Mac_copy] VALUES ('134', 'MacBook Pro', 'Silber', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_2', '16 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '7479');
INSERT INTO [dbo].[Mac_copy] VALUES ('135', 'MacBook Pro', 'Silber', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3759');
INSERT INTO [dbo].[Mac_copy] VALUES ('136', 'MacBook Pro', 'Silber', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3999');
INSERT INTO [dbo].[Mac_copy] VALUES ('137', 'MacBook Pro', 'Silber', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4479');
INSERT INTO [dbo].[Mac_copy] VALUES ('138', 'MacBook Pro', 'Silber', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '5439');
INSERT INTO [dbo].[Mac_copy] VALUES ('139', 'MacBook Pro', 'Silber', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 555X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '7839');
INSERT INTO [dbo].[Mac_copy] VALUES ('140', 'MacBook Pro', 'Silber', 'MBP15', '256 GB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '3879');
INSERT INTO [dbo].[Mac_copy] VALUES ('141', 'MacBook Pro', 'Silber', 'MBP15', '512 GB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4119');
INSERT INTO [dbo].[Mac_copy] VALUES ('142', 'MacBook Pro', 'Silber', 'MBP15', '1 TB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '4599');
INSERT INTO [dbo].[Mac_copy] VALUES ('143', 'MacBook Pro', 'Silber', 'MBP15', '2 TB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '5559');
INSERT INTO [dbo].[Mac_copy] VALUES ('144', 'MacBook Pro', 'Silber', 'MBP15', '4 TB', 'MBP15', '1.83 KG', 'MBP15_2', '32 GB', 'Radeon Pro 560X mit 4 GB GDDR5 Grafikspeicher', 'MBP15', 'MBP15', 'MBP15', 'MBP15', '7959');
INSERT INTO [dbo].[Mac_copy] VALUES ('145', 'MacBook Air', 'Space Grau', 'MBA13RD', '128 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '8 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '1349');
INSERT INTO [dbo].[Mac_copy] VALUES ('146', 'MacBook Air', 'Space Grau', 'MBA13RD', '256 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '8 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '1599');
INSERT INTO [dbo].[Mac_copy] VALUES ('147', 'MacBook Air', 'Space Grau', 'MBA13RD', '512 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '8 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '1849');
INSERT INTO [dbo].[Mac_copy] VALUES ('148', 'MacBook Air', 'Space Grau', 'MBA13RD', '1.5 TB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '8 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '2849');
INSERT INTO [dbo].[Mac_copy] VALUES ('149', 'MacBook Air', 'Space Grau', 'MBA13RD', '128 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '16 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '1589');
INSERT INTO [dbo].[Mac_copy] VALUES ('150', 'MacBook Air', 'Space Grau', 'MBA13RD', '256 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '16 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '1839');
INSERT INTO [dbo].[Mac_copy] VALUES ('151', 'MacBook Air', 'Space Grau', 'MBA13RD', '512 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '16 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '2089');
INSERT INTO [dbo].[Mac_copy] VALUES ('152', 'MacBook Air', 'Space Grau', 'MBA13RD', '1.5 TB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '16 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '3089');
INSERT INTO [dbo].[Mac_copy] VALUES ('153', 'MacBook Air', 'Gold', 'MBA13RD', '128 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '8 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '1349');
INSERT INTO [dbo].[Mac_copy] VALUES ('154', 'MacBook Air', 'Gold', 'MBA13RD', '256 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '8 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '1599');
INSERT INTO [dbo].[Mac_copy] VALUES ('155', 'MacBook Air', 'Gold', 'MBA13RD', '512 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '8 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '1849');
INSERT INTO [dbo].[Mac_copy] VALUES ('156', 'MacBook Air', 'Gold', 'MBA13RD', '1.5 TB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '8 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '2849');
INSERT INTO [dbo].[Mac_copy] VALUES ('157', 'MacBook Air', 'Gold', 'MBA13RD', '128 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '16 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '1589');
INSERT INTO [dbo].[Mac_copy] VALUES ('158', 'MacBook Air', 'Gold', 'MBA13RD', '256 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '16 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '1839');
INSERT INTO [dbo].[Mac_copy] VALUES ('159', 'MacBook Air', 'Gold', 'MBA13RD', '512 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '16 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '2089');
INSERT INTO [dbo].[Mac_copy] VALUES ('160', 'MacBook Air', 'Gold', 'MBA13RD', '1.5 TB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '16 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '3089');
INSERT INTO [dbo].[Mac_copy] VALUES ('161', 'MacBook Air', 'Silber', 'MBA13RD', '128 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '8 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '1349');
INSERT INTO [dbo].[Mac_copy] VALUES ('162', 'MacBook Air', 'Silber', 'MBA13RD', '256 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '8 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '1599');
INSERT INTO [dbo].[Mac_copy] VALUES ('163', 'MacBook Air', 'Silber', 'MBA13RD', '512 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '8 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '1849');
INSERT INTO [dbo].[Mac_copy] VALUES ('164', 'MacBook Air', 'Silber', 'MBA13RD', '1.5 TB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '8 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '2849');
INSERT INTO [dbo].[Mac_copy] VALUES ('165', 'MacBook Air', 'Silber', 'MBA13RD', '128 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '16 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '1589');
INSERT INTO [dbo].[Mac_copy] VALUES ('166', 'MacBook Air', 'Silber', 'MBA13RD', '256 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '16 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '1839');
INSERT INTO [dbo].[Mac_copy] VALUES ('167', 'MacBook Air', 'Silber', 'MBA13RD', '512 GB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '16 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '2089');
INSERT INTO [dbo].[Mac_copy] VALUES ('168', 'MacBook Air', 'Silber', 'MBA13RD', '1.5 TB', 'MBA13RD', '1.25 KG', 'MBA13RD_1', '16 GB', 'Intel UHD Graphics 617', 'MBA13RD', 'MBA13RD', 'MBA13RD', 'MBA13RD', '3089');
INSERT INTO [dbo].[Mac_copy] VALUES ('169', 'MacBook Air', 'Silber', 'MBA13', '128 GB', 'MBA13', '1.35 KG', 'MBA13_1', '8 GB', 'Intel HD Graphics 6000', 'MBA13', 'MBA13', 'MBA13', 'MBA13', '1099');
INSERT INTO [dbo].[Mac_copy] VALUES ('170', 'MacBook Air', 'Silber', 'MBA13', '256 GB', 'MBA13', '1.35 KG', 'MBA13_1', '8 GB', 'Intel HD Graphics 6000', 'MBA13', 'MBA13', 'MBA13', 'MBA13', '1349');
INSERT INTO [dbo].[Mac_copy] VALUES ('171', 'MacBook Air', 'Silber', 'MBA13', '512 GB', 'MBA13', '1.35 KG', 'MBA13_1', '8 GB', 'Intel HD Graphics 6000', 'MBA13', 'MBA13', 'MBA13', 'MBA13', '1599');
INSERT INTO [dbo].[Mac_copy] VALUES ('172', 'MacBook Air', 'Silber', 'MBA13', '128 GB', 'MBA13', '1.35 KG', 'MBA13_2', '8 GB', 'Intel HD Graphics 6000', 'MBA13', 'MBA13', 'MBA13', 'MBA13', '1279');
INSERT INTO [dbo].[Mac_copy] VALUES ('173', 'MacBook Air', 'Silber', 'MBA13', '256 GB', 'MBA13', '1.35 KG', 'MBA13_2', '8 GB', 'Intel HD Graphics 6000', 'MBA13', 'MBA13', 'MBA13', 'MBA13', '1529');
INSERT INTO [dbo].[Mac_copy] VALUES ('174', 'MacBook Air', 'Silber', 'MBA13', '512 GB', 'MBA13', '1.35 KG', 'MBA13_2', '8 GB', 'Intel HD Graphics 6000', 'MBA13', 'MBA13', 'MBA13', 'MBA13', '1779');
INSERT INTO [dbo].[Mac_copy] VALUES ('175', 'MacBook', 'Silber', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_1', '8 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1499');
INSERT INTO [dbo].[Mac_copy] VALUES ('176', 'MacBook', 'Silber', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_2', '8 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1619');
INSERT INTO [dbo].[Mac_copy] VALUES ('177', 'MacBook', 'Silber', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_3', '8 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1799');
INSERT INTO [dbo].[Mac_copy] VALUES ('178', 'MacBook', 'Silber', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_1', '16 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1739');
INSERT INTO [dbo].[Mac_copy] VALUES ('179', 'MacBook', 'Silber', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_2', '16 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1859');
INSERT INTO [dbo].[Mac_copy] VALUES ('180', 'MacBook', 'Silber', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_3', '16 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '2039');
INSERT INTO [dbo].[Mac_copy] VALUES ('181', 'MacBook', 'Silber', 'MB12', '512 GB', 'MB12', '0.92 KG', 'MB12_2', '8 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1799');
INSERT INTO [dbo].[Mac_copy] VALUES ('182', 'MacBook', 'Silber', 'MB12', '512 GB', 'MB12', '0.92 KG', 'MB12_3', '8 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1979');
INSERT INTO [dbo].[Mac_copy] VALUES ('183', 'MacBook', 'Silber', 'MB12', '512 GB', 'MB12', '0.92 KG', 'MB12_2', '16 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '2039');
INSERT INTO [dbo].[Mac_copy] VALUES ('184', 'MacBook', 'Silber', 'MB12', '512 GB', 'MB12', '0.92 KG', 'MB12_3', '16 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '2219');
INSERT INTO [dbo].[Mac_copy] VALUES ('185', 'MacBook', 'Gold', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_1', '8 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1499');
INSERT INTO [dbo].[Mac_copy] VALUES ('186', 'MacBook', 'Gold', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_2', '8 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1619');
INSERT INTO [dbo].[Mac_copy] VALUES ('187', 'MacBook', 'Gold', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_3', '8 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1799');
INSERT INTO [dbo].[Mac_copy] VALUES ('188', 'MacBook', 'Gold', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_1', '16 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1739');
INSERT INTO [dbo].[Mac_copy] VALUES ('189', 'MacBook', 'Gold', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_2', '16 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1859');
INSERT INTO [dbo].[Mac_copy] VALUES ('190', 'MacBook', 'Gold', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_3', '16 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '2039');
INSERT INTO [dbo].[Mac_copy] VALUES ('191', 'MacBook', 'Gold', 'MB12', '512 GB', 'MB12', '0.92 KG', 'MB12_2', '8 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1799');
INSERT INTO [dbo].[Mac_copy] VALUES ('192', 'MacBook', 'Gold', 'MB12', '512 GB', 'MB12', '0.92 KG', 'MB12_3', '8 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1979');
INSERT INTO [dbo].[Mac_copy] VALUES ('193', 'MacBook', 'Gold', 'MB12', '512 GB', 'MB12', '0.92 KG', 'MB12_2', '16 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '2039');
INSERT INTO [dbo].[Mac_copy] VALUES ('194', 'MacBook', 'Gold', 'MB12', '512 GB', 'MB12', '0.92 KG', 'MB12_3', '16 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '2219');
INSERT INTO [dbo].[Mac_copy] VALUES ('195', 'MacBook', 'Space Grau', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_1', '8 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1499');
INSERT INTO [dbo].[Mac_copy] VALUES ('196', 'MacBook', 'Space Grau', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_2', '8 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1619');
INSERT INTO [dbo].[Mac_copy] VALUES ('197', 'MacBook', 'Space Grau', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_3', '8 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1799');
INSERT INTO [dbo].[Mac_copy] VALUES ('198', 'MacBook', 'Space Grau', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_1', '16 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1739');
INSERT INTO [dbo].[Mac_copy] VALUES ('199', 'MacBook', 'Space Grau', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_2', '16 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1859');
INSERT INTO [dbo].[Mac_copy] VALUES ('200', 'MacBook', 'Space Grau', 'MB12', '256 GB', 'MB12', '0.92 KG', 'MB12_3', '16 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '2039');
INSERT INTO [dbo].[Mac_copy] VALUES ('201', 'MacBook', 'Space Grau', 'MB12', '512 GB', 'MB12', '0.92 KG', 'MB12_2', '8 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1799');
INSERT INTO [dbo].[Mac_copy] VALUES ('202', 'MacBook', 'Space Grau', 'MB12', '512 GB', 'MB12', '0.92 KG', 'MB12_3', '8 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '1979');
INSERT INTO [dbo].[Mac_copy] VALUES ('203', 'MacBook', 'Space Grau', 'MB12', '512 GB', 'MB12', '0.92 KG', 'MB12_2', '16 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '2039');
INSERT INTO [dbo].[Mac_copy] VALUES ('204', 'MacBook', 'Space Grau', 'MB12', '512 GB', 'MB12', '0.92 KG', 'MB12_3', '16 GB', 'Intel HD Graphics 615', 'MB12', 'MB12', 'MB12', 'MB12', '2219');
INSERT INTO [dbo].[Mac_copy] VALUES ('205', 'iMac', 'Silber', 'iM21_1', '256 GB', 'iM21', '5.66 KG', 'iM21_1', '8 GB', 'Intel Iris Plus Graphics 640', 'iM21', 'iM_1', 'iM21', 'wire', '1539');
INSERT INTO [dbo].[Mac_copy] VALUES ('206', 'iMac', 'Silber', 'iM21_1', '256 GB', 'iM21', '5.66 KG', 'iM21_1', '8 GB', 'Intel Iris Plus Graphics 640', 'iM21', 'iM_2', 'iM21', 'wire', '1603');
INSERT INTO [dbo].[Mac_copy] VALUES ('207', 'iMac', 'Silber', 'iM21_1', '256 GB', 'iM21', '5.66 KG', 'iM21_1', '8 GB', 'Intel Iris Plus Graphics 640', 'iM21', 'iM_3', 'iM21', 'wire', '1688');
INSERT INTO [dbo].[Mac_copy] VALUES ('208', 'iMac', 'Silber', 'iM21_1', '256 GB', 'iM21', '5.66 KG', 'iM21_1', '16 GB', 'Intel Iris Plus Graphics 640', 'iM21', 'iM_1', 'iM21', 'wire', '1779');
INSERT INTO [dbo].[Mac_copy] VALUES ('209', 'iMac', 'Silber', 'iM21_1', '256 GB', 'iM21', '5.66 KG', 'iM21_1', '16 GB', 'Intel Iris Plus Graphics 640', 'iM21', 'iM_2', 'iM21', 'wire', '1843');
INSERT INTO [dbo].[Mac_copy] VALUES ('210', 'iMac', 'Silber', 'iM21_1', '256 GB', 'iM21', '5.66 KG', 'iM21_1', '16 GB', 'Intel Iris Plus Graphics 640', 'iM21', 'iM_3', 'iM21', 'wire', '1928');
INSERT INTO [dbo].[Mac_copy] VALUES ('211', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_2', '8 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '1739');
INSERT INTO [dbo].[Mac_copy] VALUES ('212', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_2', '8 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '1803');
INSERT INTO [dbo].[Mac_copy] VALUES ('213', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_2', '8 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '1888');
INSERT INTO [dbo].[Mac_copy] VALUES ('214', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_2', '16 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '1979');
INSERT INTO [dbo].[Mac_copy] VALUES ('215', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_2', '16 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2043');
INSERT INTO [dbo].[Mac_copy] VALUES ('216', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_2', '16 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2128');
INSERT INTO [dbo].[Mac_copy] VALUES ('217', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_3', '8 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2099');
INSERT INTO [dbo].[Mac_copy] VALUES ('218', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_3', '8 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2163');
INSERT INTO [dbo].[Mac_copy] VALUES ('219', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_3', '8 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2248');
INSERT INTO [dbo].[Mac_copy] VALUES ('220', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_3', '16 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2339');
INSERT INTO [dbo].[Mac_copy] VALUES ('221', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_3', '16 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2403');
INSERT INTO [dbo].[Mac_copy] VALUES ('222', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_3', '16 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2488');
INSERT INTO [dbo].[Mac_copy] VALUES ('223', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_2', '8 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '1979');
INSERT INTO [dbo].[Mac_copy] VALUES ('224', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_2', '8 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2043');
INSERT INTO [dbo].[Mac_copy] VALUES ('225', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_2', '8 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2128');
INSERT INTO [dbo].[Mac_copy] VALUES ('226', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_2', '16 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2219');
INSERT INTO [dbo].[Mac_copy] VALUES ('227', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_2', '16 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2283');
INSERT INTO [dbo].[Mac_copy] VALUES ('228', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_2', '16 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2368');
INSERT INTO [dbo].[Mac_copy] VALUES ('229', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_3', '8 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2339');
INSERT INTO [dbo].[Mac_copy] VALUES ('230', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_3', '8 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2403');
INSERT INTO [dbo].[Mac_copy] VALUES ('231', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_3', '8 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2488');
INSERT INTO [dbo].[Mac_copy] VALUES ('232', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_3', '16 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2579');
INSERT INTO [dbo].[Mac_copy] VALUES ('233', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_3', '16 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2643');
INSERT INTO [dbo].[Mac_copy] VALUES ('234', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_3', '16 GB', 'Radeon Pro 555 mit 2 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2728');
INSERT INTO [dbo].[Mac_copy] VALUES ('235', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_4', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '1819');
INSERT INTO [dbo].[Mac_copy] VALUES ('236', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_4', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '1883');
INSERT INTO [dbo].[Mac_copy] VALUES ('237', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_4', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '1968');
INSERT INTO [dbo].[Mac_copy] VALUES ('238', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_3', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2059');
INSERT INTO [dbo].[Mac_copy] VALUES ('239', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_3', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2123');
INSERT INTO [dbo].[Mac_copy] VALUES ('240', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_3', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2208');
INSERT INTO [dbo].[Mac_copy] VALUES ('241', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_4', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2059');
INSERT INTO [dbo].[Mac_copy] VALUES ('242', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_4', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2123');
INSERT INTO [dbo].[Mac_copy] VALUES ('243', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_4', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2208');
INSERT INTO [dbo].[Mac_copy] VALUES ('244', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_3', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2299');
INSERT INTO [dbo].[Mac_copy] VALUES ('245', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_3', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2363');
INSERT INTO [dbo].[Mac_copy] VALUES ('246', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_3', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2448');
INSERT INTO [dbo].[Mac_copy] VALUES ('247', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_4', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2539');
INSERT INTO [dbo].[Mac_copy] VALUES ('248', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_4', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2603');
INSERT INTO [dbo].[Mac_copy] VALUES ('249', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_4', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2688');
INSERT INTO [dbo].[Mac_copy] VALUES ('250', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_3', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2779');
INSERT INTO [dbo].[Mac_copy] VALUES ('251', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_3', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2843');
INSERT INTO [dbo].[Mac_copy] VALUES ('252', 'iMac', 'Silber', 'iM21_2', '256 GB', 'iM21', '5.66 KG', 'iM21_3', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2928');
INSERT INTO [dbo].[Mac_copy] VALUES ('253', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_4', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2059');
INSERT INTO [dbo].[Mac_copy] VALUES ('254', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_4', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2123');
INSERT INTO [dbo].[Mac_copy] VALUES ('255', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_4', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2208');
INSERT INTO [dbo].[Mac_copy] VALUES ('256', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_3', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2299');
INSERT INTO [dbo].[Mac_copy] VALUES ('257', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_3', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2363');
INSERT INTO [dbo].[Mac_copy] VALUES ('258', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_3', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2448');
INSERT INTO [dbo].[Mac_copy] VALUES ('259', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_4', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2299');
INSERT INTO [dbo].[Mac_copy] VALUES ('260', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_4', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2363');
INSERT INTO [dbo].[Mac_copy] VALUES ('261', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_4', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2448');
INSERT INTO [dbo].[Mac_copy] VALUES ('262', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_3', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2539');
INSERT INTO [dbo].[Mac_copy] VALUES ('263', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_3', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2603');
INSERT INTO [dbo].[Mac_copy] VALUES ('264', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_3', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2688');
INSERT INTO [dbo].[Mac_copy] VALUES ('265', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_4', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2779');
INSERT INTO [dbo].[Mac_copy] VALUES ('266', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_4', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2843');
INSERT INTO [dbo].[Mac_copy] VALUES ('267', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_4', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2928');
INSERT INTO [dbo].[Mac_copy] VALUES ('268', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_3', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '3019');
INSERT INTO [dbo].[Mac_copy] VALUES ('269', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_3', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '3083');
INSERT INTO [dbo].[Mac_copy] VALUES ('270', 'iMac', 'Silber', 'iM21_2', '512 GB', 'iM21', '5.66 KG', 'iM21_3', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '3168');
INSERT INTO [dbo].[Mac_copy] VALUES ('271', 'iMac', 'Silber', 'iM21_2', '1TB', 'iM21', '5.66 KG', 'iM21_4', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2539');
INSERT INTO [dbo].[Mac_copy] VALUES ('272', 'iMac', 'Silber', 'iM21_2', '1TB', 'iM21', '5.66 KG', 'iM21_4', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2603');
INSERT INTO [dbo].[Mac_copy] VALUES ('273', 'iMac', 'Silber', 'iM21_2', '1TB', 'iM21', '5.66 KG', 'iM21_4', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2688');
INSERT INTO [dbo].[Mac_copy] VALUES ('274', 'iMac', 'Silber', 'iM21_2', '1TB', 'iM21', '5.66 KG', 'iM21_3', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2779');
INSERT INTO [dbo].[Mac_copy] VALUES ('275', 'iMac', 'Silber', 'iM21_2', '1TB', 'iM21', '5.66 KG', 'iM21_3', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2843');
INSERT INTO [dbo].[Mac_copy] VALUES ('276', 'iMac', 'Silber', 'iM21_2', '1TB', 'iM21', '5.66 KG', 'iM21_3', '8 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2928');
INSERT INTO [dbo].[Mac_copy] VALUES ('277', 'iMac', 'Silber', 'iM21_2', '1TB', 'iM21', '5.66 KG', 'iM21_4', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '2779');
INSERT INTO [dbo].[Mac_copy] VALUES ('278', 'iMac', 'Silber', 'iM21_2', '1TB', 'iM21', '5.66 KG', 'iM21_4', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '2843');
INSERT INTO [dbo].[Mac_copy] VALUES ('279', 'iMac', 'Silber', 'iM21_2', '1TB', 'iM21', '5.66 KG', 'iM21_4', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '2928');
INSERT INTO [dbo].[Mac_copy] VALUES ('280', 'iMac', 'Silber', 'iM21_2', '1TB', 'iM21', '5.66 KG', 'iM21_3', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '3019');
INSERT INTO [dbo].[Mac_copy] VALUES ('281', 'iMac', 'Silber', 'iM21_2', '1TB', 'iM21', '5.66 KG', 'iM21_3', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '3083');
INSERT INTO [dbo].[Mac_copy] VALUES ('282', 'iMac', 'Silber', 'iM21_2', '1TB', 'iM21', '5.66 KG', 'iM21_3', '16 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '3168');
INSERT INTO [dbo].[Mac_copy] VALUES ('283', 'iMac', 'Silber', 'iM21_2', '1TB', 'iM21', '5.66 KG', 'iM21_4', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '3259');
INSERT INTO [dbo].[Mac_copy] VALUES ('284', 'iMac', 'Silber', 'iM21_2', '1TB', 'iM21', '5.66 KG', 'iM21_4', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '3323');
INSERT INTO [dbo].[Mac_copy] VALUES ('285', 'iMac', 'Silber', 'iM21_2', '1TB', 'iM21', '5.66 KG', 'iM21_4', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '3408');
INSERT INTO [dbo].[Mac_copy] VALUES ('286', 'iMac', 'Silber', 'iM21_2', '1TB', 'iM21', '5.66 KG', 'iM21_3', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_1', 'iM21', 'wire', '3499');
INSERT INTO [dbo].[Mac_copy] VALUES ('287', 'iMac', 'Silber', 'iM21_2', '1TB', 'iM21', '5.66 KG', 'iM21_3', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_2', 'iM21', 'wire', '3563');
INSERT INTO [dbo].[Mac_copy] VALUES ('288', 'iMac', 'Silber', 'iM21_2', '1TB', 'iM21', '5.66 KG', 'iM21_3', '32 GB', 'Radeon Pro 560 mit 4 GB Videospeicher', 'iM21', 'iM_3', 'iM21', 'wire', '3648');
INSERT INTO [dbo].[Mac_copy] VALUES ('289', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_1', '8 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '2219');
INSERT INTO [dbo].[Mac_copy] VALUES ('290', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_1', '8 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '2283');
INSERT INTO [dbo].[Mac_copy] VALUES ('291', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_1', '8 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '2368');
INSERT INTO [dbo].[Mac_copy] VALUES ('292', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_1', '16 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '2459');
INSERT INTO [dbo].[Mac_copy] VALUES ('293', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_1', '16 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '2523');
INSERT INTO [dbo].[Mac_copy] VALUES ('294', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_1', '16 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '2608');
INSERT INTO [dbo].[Mac_copy] VALUES ('295', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_1', '32 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '2939');
INSERT INTO [dbo].[Mac_copy] VALUES ('296', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_1', '32 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3003');
INSERT INTO [dbo].[Mac_copy] VALUES ('297', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_1', '32 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3088');
INSERT INTO [dbo].[Mac_copy] VALUES ('298', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_1', '8 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '2459');
INSERT INTO [dbo].[Mac_copy] VALUES ('299', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_1', '8 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '2523');
INSERT INTO [dbo].[Mac_copy] VALUES ('300', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_1', '8 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '2608');
INSERT INTO [dbo].[Mac_copy] VALUES ('301', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_1', '16 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '2699');
INSERT INTO [dbo].[Mac_copy] VALUES ('302', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_1', '16 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '2763');
INSERT INTO [dbo].[Mac_copy] VALUES ('303', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_1', '16 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '2848');
INSERT INTO [dbo].[Mac_copy] VALUES ('304', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_1', '32 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3179');
INSERT INTO [dbo].[Mac_copy] VALUES ('305', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_1', '32 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3243');
INSERT INTO [dbo].[Mac_copy] VALUES ('306', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_1', '32 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3328');
INSERT INTO [dbo].[Mac_copy] VALUES ('307', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_1', '8 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '2939');
INSERT INTO [dbo].[Mac_copy] VALUES ('308', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_1', '8 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3003');
INSERT INTO [dbo].[Mac_copy] VALUES ('309', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_1', '8 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3088');
INSERT INTO [dbo].[Mac_copy] VALUES ('310', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_1', '16 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3179');
INSERT INTO [dbo].[Mac_copy] VALUES ('311', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_1', '16 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3243');
INSERT INTO [dbo].[Mac_copy] VALUES ('312', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_1', '16 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3328');
INSERT INTO [dbo].[Mac_copy] VALUES ('313', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_1', '32 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3659');
INSERT INTO [dbo].[Mac_copy] VALUES ('314', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_1', '32 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3723');
INSERT INTO [dbo].[Mac_copy] VALUES ('315', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_1', '32 GB', 'Radeon Pro 570 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3808');
INSERT INTO [dbo].[Mac_copy] VALUES ('316', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_2', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '2419');
GO
INSERT INTO [dbo].[Mac_copy] VALUES ('317', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_2', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '2489');
INSERT INTO [dbo].[Mac_copy] VALUES ('318', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_2', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '2568');
INSERT INTO [dbo].[Mac_copy] VALUES ('319', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_2', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '2659');
INSERT INTO [dbo].[Mac_copy] VALUES ('320', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_2', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '2729');
INSERT INTO [dbo].[Mac_copy] VALUES ('321', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_2', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '2808');
INSERT INTO [dbo].[Mac_copy] VALUES ('322', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_2', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3139');
INSERT INTO [dbo].[Mac_copy] VALUES ('323', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_2', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3209');
INSERT INTO [dbo].[Mac_copy] VALUES ('324', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_2', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3288');
INSERT INTO [dbo].[Mac_copy] VALUES ('325', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_2', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '2659');
INSERT INTO [dbo].[Mac_copy] VALUES ('326', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_2', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '2729');
INSERT INTO [dbo].[Mac_copy] VALUES ('327', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_2', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '2808');
INSERT INTO [dbo].[Mac_copy] VALUES ('328', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_2', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '2899');
INSERT INTO [dbo].[Mac_copy] VALUES ('329', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_2', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '2969');
INSERT INTO [dbo].[Mac_copy] VALUES ('330', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_2', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3048');
INSERT INTO [dbo].[Mac_copy] VALUES ('331', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_2', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3379');
INSERT INTO [dbo].[Mac_copy] VALUES ('332', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_2', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3449');
INSERT INTO [dbo].[Mac_copy] VALUES ('333', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_2', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3528');
INSERT INTO [dbo].[Mac_copy] VALUES ('334', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_2', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3139');
INSERT INTO [dbo].[Mac_copy] VALUES ('335', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_2', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3209');
INSERT INTO [dbo].[Mac_copy] VALUES ('336', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_2', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3288');
INSERT INTO [dbo].[Mac_copy] VALUES ('337', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_2', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3379');
INSERT INTO [dbo].[Mac_copy] VALUES ('338', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_2', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3449');
INSERT INTO [dbo].[Mac_copy] VALUES ('339', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_2', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3528');
INSERT INTO [dbo].[Mac_copy] VALUES ('340', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_2', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3859');
INSERT INTO [dbo].[Mac_copy] VALUES ('341', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_2', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3929');
INSERT INTO [dbo].[Mac_copy] VALUES ('342', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_2', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '4008');
INSERT INTO [dbo].[Mac_copy] VALUES ('343', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_2', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4099');
INSERT INTO [dbo].[Mac_copy] VALUES ('344', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_2', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '4169');
INSERT INTO [dbo].[Mac_copy] VALUES ('345', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_2', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '4248');
INSERT INTO [dbo].[Mac_copy] VALUES ('346', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_2', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4339');
INSERT INTO [dbo].[Mac_copy] VALUES ('347', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_2', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '4409');
INSERT INTO [dbo].[Mac_copy] VALUES ('348', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_2', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '4488');
INSERT INTO [dbo].[Mac_copy] VALUES ('349', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_2', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4819');
INSERT INTO [dbo].[Mac_copy] VALUES ('350', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_2', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '4889');
INSERT INTO [dbo].[Mac_copy] VALUES ('351', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_2', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '4968');
INSERT INTO [dbo].[Mac_copy] VALUES ('352', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '2779');
INSERT INTO [dbo].[Mac_copy] VALUES ('353', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '2849');
INSERT INTO [dbo].[Mac_copy] VALUES ('354', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '2928');
INSERT INTO [dbo].[Mac_copy] VALUES ('355', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3019');
INSERT INTO [dbo].[Mac_copy] VALUES ('356', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3089');
INSERT INTO [dbo].[Mac_copy] VALUES ('357', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3168');
INSERT INTO [dbo].[Mac_copy] VALUES ('358', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3499');
INSERT INTO [dbo].[Mac_copy] VALUES ('359', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3569');
INSERT INTO [dbo].[Mac_copy] VALUES ('360', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3648');
INSERT INTO [dbo].[Mac_copy] VALUES ('361', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3019');
INSERT INTO [dbo].[Mac_copy] VALUES ('362', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3089');
INSERT INTO [dbo].[Mac_copy] VALUES ('363', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3168');
INSERT INTO [dbo].[Mac_copy] VALUES ('364', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3259');
INSERT INTO [dbo].[Mac_copy] VALUES ('365', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3329');
INSERT INTO [dbo].[Mac_copy] VALUES ('366', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3408');
INSERT INTO [dbo].[Mac_copy] VALUES ('367', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3739');
INSERT INTO [dbo].[Mac_copy] VALUES ('368', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3809');
INSERT INTO [dbo].[Mac_copy] VALUES ('369', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3888');
INSERT INTO [dbo].[Mac_copy] VALUES ('370', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3499');
INSERT INTO [dbo].[Mac_copy] VALUES ('371', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3569');
INSERT INTO [dbo].[Mac_copy] VALUES ('372', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3648');
INSERT INTO [dbo].[Mac_copy] VALUES ('373', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3739');
INSERT INTO [dbo].[Mac_copy] VALUES ('374', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3809');
INSERT INTO [dbo].[Mac_copy] VALUES ('375', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3888');
INSERT INTO [dbo].[Mac_copy] VALUES ('376', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4219');
INSERT INTO [dbo].[Mac_copy] VALUES ('377', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '4289');
INSERT INTO [dbo].[Mac_copy] VALUES ('378', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '4368');
INSERT INTO [dbo].[Mac_copy] VALUES ('379', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4459');
INSERT INTO [dbo].[Mac_copy] VALUES ('380', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '4529');
INSERT INTO [dbo].[Mac_copy] VALUES ('381', 'iMac', 'Silber', 'iM27', '256 GB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '4608');
INSERT INTO [dbo].[Mac_copy] VALUES ('382', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4699');
INSERT INTO [dbo].[Mac_copy] VALUES ('383', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '4769');
INSERT INTO [dbo].[Mac_copy] VALUES ('384', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '4848');
INSERT INTO [dbo].[Mac_copy] VALUES ('385', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '5179');
INSERT INTO [dbo].[Mac_copy] VALUES ('386', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '5249');
INSERT INTO [dbo].[Mac_copy] VALUES ('387', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 575 mit 4 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '5328');
INSERT INTO [dbo].[Mac_copy] VALUES ('388', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_3', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '2839');
INSERT INTO [dbo].[Mac_copy] VALUES ('389', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_3', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '2903');
INSERT INTO [dbo].[Mac_copy] VALUES ('390', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_3', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '2988');
INSERT INTO [dbo].[Mac_copy] VALUES ('391', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_3', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3319');
INSERT INTO [dbo].[Mac_copy] VALUES ('392', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_3', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3383');
INSERT INTO [dbo].[Mac_copy] VALUES ('393', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_3', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3468');
INSERT INTO [dbo].[Mac_copy] VALUES ('394', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_3', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4279');
INSERT INTO [dbo].[Mac_copy] VALUES ('395', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_3', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '4343');
INSERT INTO [dbo].[Mac_copy] VALUES ('396', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_3', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '4428');
INSERT INTO [dbo].[Mac_copy] VALUES ('397', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_3', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3079');
INSERT INTO [dbo].[Mac_copy] VALUES ('398', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_3', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3143');
INSERT INTO [dbo].[Mac_copy] VALUES ('399', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_3', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3228');
INSERT INTO [dbo].[Mac_copy] VALUES ('400', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_3', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3559');
INSERT INTO [dbo].[Mac_copy] VALUES ('401', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_3', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3623');
INSERT INTO [dbo].[Mac_copy] VALUES ('402', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_3', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3708');
INSERT INTO [dbo].[Mac_copy] VALUES ('403', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_3', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4519');
INSERT INTO [dbo].[Mac_copy] VALUES ('404', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_3', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '4583');
INSERT INTO [dbo].[Mac_copy] VALUES ('405', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_3', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '4668');
INSERT INTO [dbo].[Mac_copy] VALUES ('406', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_3', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3559');
INSERT INTO [dbo].[Mac_copy] VALUES ('407', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_3', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3623');
INSERT INTO [dbo].[Mac_copy] VALUES ('408', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_3', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3708');
INSERT INTO [dbo].[Mac_copy] VALUES ('409', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_3', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4039');
INSERT INTO [dbo].[Mac_copy] VALUES ('410', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_3', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '4103');
INSERT INTO [dbo].[Mac_copy] VALUES ('411', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_3', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '4188');
INSERT INTO [dbo].[Mac_copy] VALUES ('412', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_3', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4999');
INSERT INTO [dbo].[Mac_copy] VALUES ('413', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_3', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '5063');
INSERT INTO [dbo].[Mac_copy] VALUES ('414', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_3', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '5148');
INSERT INTO [dbo].[Mac_copy] VALUES ('415', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_3', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4519');
INSERT INTO [dbo].[Mac_copy] VALUES ('416', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_3', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '4583');
INSERT INTO [dbo].[Mac_copy] VALUES ('417', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_3', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '4668');
INSERT INTO [dbo].[Mac_copy] VALUES ('418', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_3', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4999');
INSERT INTO [dbo].[Mac_copy] VALUES ('419', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_3', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '5063');
INSERT INTO [dbo].[Mac_copy] VALUES ('420', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_3', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '5148');
INSERT INTO [dbo].[Mac_copy] VALUES ('421', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_3', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '5959');
INSERT INTO [dbo].[Mac_copy] VALUES ('422', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_3', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '6023');
INSERT INTO [dbo].[Mac_copy] VALUES ('423', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_3', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '6108');
INSERT INTO [dbo].[Mac_copy] VALUES ('424', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3079');
INSERT INTO [dbo].[Mac_copy] VALUES ('425', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3143');
INSERT INTO [dbo].[Mac_copy] VALUES ('426', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3228');
INSERT INTO [dbo].[Mac_copy] VALUES ('427', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3559');
INSERT INTO [dbo].[Mac_copy] VALUES ('428', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3623');
INSERT INTO [dbo].[Mac_copy] VALUES ('429', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3708');
INSERT INTO [dbo].[Mac_copy] VALUES ('430', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4519');
INSERT INTO [dbo].[Mac_copy] VALUES ('431', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '4583');
INSERT INTO [dbo].[Mac_copy] VALUES ('432', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_4', '8 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '4668');
INSERT INTO [dbo].[Mac_copy] VALUES ('433', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3319');
INSERT INTO [dbo].[Mac_copy] VALUES ('434', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3383');
INSERT INTO [dbo].[Mac_copy] VALUES ('435', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3468');
INSERT INTO [dbo].[Mac_copy] VALUES ('436', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3799');
INSERT INTO [dbo].[Mac_copy] VALUES ('437', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3863');
INSERT INTO [dbo].[Mac_copy] VALUES ('438', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3948');
INSERT INTO [dbo].[Mac_copy] VALUES ('439', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4759');
INSERT INTO [dbo].[Mac_copy] VALUES ('440', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '4823');
INSERT INTO [dbo].[Mac_copy] VALUES ('441', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_4', '16 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '4908');
INSERT INTO [dbo].[Mac_copy] VALUES ('442', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '3799');
INSERT INTO [dbo].[Mac_copy] VALUES ('443', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '3863');
INSERT INTO [dbo].[Mac_copy] VALUES ('444', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '3948');
INSERT INTO [dbo].[Mac_copy] VALUES ('445', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4279');
INSERT INTO [dbo].[Mac_copy] VALUES ('446', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '4343');
INSERT INTO [dbo].[Mac_copy] VALUES ('447', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '4428');
INSERT INTO [dbo].[Mac_copy] VALUES ('448', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '5239');
INSERT INTO [dbo].[Mac_copy] VALUES ('449', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '5303');
INSERT INTO [dbo].[Mac_copy] VALUES ('450', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_4', '32 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '5388');
INSERT INTO [dbo].[Mac_copy] VALUES ('451', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '4759');
INSERT INTO [dbo].[Mac_copy] VALUES ('452', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '4823');
INSERT INTO [dbo].[Mac_copy] VALUES ('453', 'iMac', 'Silber', 'iM27', '512 GB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '4908');
INSERT INTO [dbo].[Mac_copy] VALUES ('454', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '5239');
INSERT INTO [dbo].[Mac_copy] VALUES ('455', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '5303');
INSERT INTO [dbo].[Mac_copy] VALUES ('456', 'iMac', 'Silber', 'iM27', '1 TB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '5388');
INSERT INTO [dbo].[Mac_copy] VALUES ('457', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_1', 'iM27', 'wire', '6199');
INSERT INTO [dbo].[Mac_copy] VALUES ('458', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_2', 'iM27', 'wire', '6263');
INSERT INTO [dbo].[Mac_copy] VALUES ('459', 'iMac', 'Silber', 'iM27', '2 TB', 'iM27', '9.44 KG', 'iM27_4', '64 GB', 'Radeon Pro 580 mit 8 GB Videospeicher', 'iM27', 'iM_3', 'iM27', 'wire', '6348');
INSERT INTO [dbo].[Mac_copy] VALUES ('460', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '5499');
INSERT INTO [dbo].[Mac_copy] VALUES ('461', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '5549');
INSERT INTO [dbo].[Mac_copy] VALUES ('462', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '5648');
INSERT INTO [dbo].[Mac_copy] VALUES ('463', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '6459');
INSERT INTO [dbo].[Mac_copy] VALUES ('464', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '6509');
INSERT INTO [dbo].[Mac_copy] VALUES ('465', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '6608');
INSERT INTO [dbo].[Mac_copy] VALUES ('466', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '8859');
INSERT INTO [dbo].[Mac_copy] VALUES ('467', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '8909');
INSERT INTO [dbo].[Mac_copy] VALUES ('468', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '9008');
INSERT INTO [dbo].[Mac_copy] VALUES ('469', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '6459');
INSERT INTO [dbo].[Mac_copy] VALUES ('470', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '6509');
INSERT INTO [dbo].[Mac_copy] VALUES ('471', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '6608');
INSERT INTO [dbo].[Mac_copy] VALUES ('472', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '7419');
INSERT INTO [dbo].[Mac_copy] VALUES ('473', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '7469');
INSERT INTO [dbo].[Mac_copy] VALUES ('474', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '7568');
INSERT INTO [dbo].[Mac_copy] VALUES ('475', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '9819');
INSERT INTO [dbo].[Mac_copy] VALUES ('476', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '9869');
INSERT INTO [dbo].[Mac_copy] VALUES ('477', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '9968');
INSERT INTO [dbo].[Mac_copy] VALUES ('478', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '7419');
INSERT INTO [dbo].[Mac_copy] VALUES ('479', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '7469');
INSERT INTO [dbo].[Mac_copy] VALUES ('480', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '7568');
INSERT INTO [dbo].[Mac_copy] VALUES ('481', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '8379');
INSERT INTO [dbo].[Mac_copy] VALUES ('482', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '8429');
INSERT INTO [dbo].[Mac_copy] VALUES ('483', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '8528');
INSERT INTO [dbo].[Mac_copy] VALUES ('484', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '10779');
INSERT INTO [dbo].[Mac_copy] VALUES ('485', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '10829');
INSERT INTO [dbo].[Mac_copy] VALUES ('486', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '10928');
INSERT INTO [dbo].[Mac_copy] VALUES ('487', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '8379');
INSERT INTO [dbo].[Mac_copy] VALUES ('488', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '8429');
INSERT INTO [dbo].[Mac_copy] VALUES ('489', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '8528');
INSERT INTO [dbo].[Mac_copy] VALUES ('490', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '9339');
INSERT INTO [dbo].[Mac_copy] VALUES ('491', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '9389');
INSERT INTO [dbo].[Mac_copy] VALUES ('492', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '9488');
INSERT INTO [dbo].[Mac_copy] VALUES ('493', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '11739');
INSERT INTO [dbo].[Mac_copy] VALUES ('494', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '11789');
INSERT INTO [dbo].[Mac_copy] VALUES ('495', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '11888');
INSERT INTO [dbo].[Mac_copy] VALUES ('496', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '6459');
INSERT INTO [dbo].[Mac_copy] VALUES ('497', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '6509');
INSERT INTO [dbo].[Mac_copy] VALUES ('498', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '6608');
INSERT INTO [dbo].[Mac_copy] VALUES ('499', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '7419');
INSERT INTO [dbo].[Mac_copy] VALUES ('500', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '7469');
INSERT INTO [dbo].[Mac_copy] VALUES ('501', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '7568');
INSERT INTO [dbo].[Mac_copy] VALUES ('502', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '9819');
INSERT INTO [dbo].[Mac_copy] VALUES ('503', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '9869');
INSERT INTO [dbo].[Mac_copy] VALUES ('504', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '9968');
INSERT INTO [dbo].[Mac_copy] VALUES ('505', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '7419');
INSERT INTO [dbo].[Mac_copy] VALUES ('506', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '7469');
INSERT INTO [dbo].[Mac_copy] VALUES ('507', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '7568');
INSERT INTO [dbo].[Mac_copy] VALUES ('508', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '8379');
INSERT INTO [dbo].[Mac_copy] VALUES ('509', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '8429');
INSERT INTO [dbo].[Mac_copy] VALUES ('510', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '8528');
INSERT INTO [dbo].[Mac_copy] VALUES ('511', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '10779');
INSERT INTO [dbo].[Mac_copy] VALUES ('512', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '10829');
INSERT INTO [dbo].[Mac_copy] VALUES ('513', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '10928');
INSERT INTO [dbo].[Mac_copy] VALUES ('514', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '8379');
INSERT INTO [dbo].[Mac_copy] VALUES ('515', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '8429');
INSERT INTO [dbo].[Mac_copy] VALUES ('516', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '8528');
INSERT INTO [dbo].[Mac_copy] VALUES ('517', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '9339');
INSERT INTO [dbo].[Mac_copy] VALUES ('518', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '9389');
INSERT INTO [dbo].[Mac_copy] VALUES ('519', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '9488');
INSERT INTO [dbo].[Mac_copy] VALUES ('520', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '11739');
INSERT INTO [dbo].[Mac_copy] VALUES ('521', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '11789');
INSERT INTO [dbo].[Mac_copy] VALUES ('522', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '11888');
INSERT INTO [dbo].[Mac_copy] VALUES ('523', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '9339');
INSERT INTO [dbo].[Mac_copy] VALUES ('524', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '9389');
INSERT INTO [dbo].[Mac_copy] VALUES ('525', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '9488');
INSERT INTO [dbo].[Mac_copy] VALUES ('526', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '10299');
INSERT INTO [dbo].[Mac_copy] VALUES ('527', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '10349');
INSERT INTO [dbo].[Mac_copy] VALUES ('528', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '10448');
INSERT INTO [dbo].[Mac_copy] VALUES ('529', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '12699');
INSERT INTO [dbo].[Mac_copy] VALUES ('530', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '12749');
INSERT INTO [dbo].[Mac_copy] VALUES ('531', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '12848');
INSERT INTO [dbo].[Mac_copy] VALUES ('532', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '8379');
INSERT INTO [dbo].[Mac_copy] VALUES ('533', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '8429');
INSERT INTO [dbo].[Mac_copy] VALUES ('534', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '8528');
INSERT INTO [dbo].[Mac_copy] VALUES ('535', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '9339');
INSERT INTO [dbo].[Mac_copy] VALUES ('536', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '9389');
INSERT INTO [dbo].[Mac_copy] VALUES ('537', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '9488');
INSERT INTO [dbo].[Mac_copy] VALUES ('538', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '11739');
INSERT INTO [dbo].[Mac_copy] VALUES ('539', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '11789');
INSERT INTO [dbo].[Mac_copy] VALUES ('540', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '11888');
INSERT INTO [dbo].[Mac_copy] VALUES ('541', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '9339');
INSERT INTO [dbo].[Mac_copy] VALUES ('542', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '9389');
INSERT INTO [dbo].[Mac_copy] VALUES ('543', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '9488');
INSERT INTO [dbo].[Mac_copy] VALUES ('544', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '10299');
INSERT INTO [dbo].[Mac_copy] VALUES ('545', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '10349');
INSERT INTO [dbo].[Mac_copy] VALUES ('546', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '10448');
INSERT INTO [dbo].[Mac_copy] VALUES ('547', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '12699');
INSERT INTO [dbo].[Mac_copy] VALUES ('548', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '12749');
INSERT INTO [dbo].[Mac_copy] VALUES ('549', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '12848');
INSERT INTO [dbo].[Mac_copy] VALUES ('550', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '10299');
INSERT INTO [dbo].[Mac_copy] VALUES ('551', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '10349');
INSERT INTO [dbo].[Mac_copy] VALUES ('552', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '10448');
INSERT INTO [dbo].[Mac_copy] VALUES ('553', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '11259');
INSERT INTO [dbo].[Mac_copy] VALUES ('554', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '11309');
INSERT INTO [dbo].[Mac_copy] VALUES ('555', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '11408');
INSERT INTO [dbo].[Mac_copy] VALUES ('556', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '13659');
INSERT INTO [dbo].[Mac_copy] VALUES ('557', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '13709');
INSERT INTO [dbo].[Mac_copy] VALUES ('558', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '13808');
INSERT INTO [dbo].[Mac_copy] VALUES ('559', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '11259');
INSERT INTO [dbo].[Mac_copy] VALUES ('560', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '11309');
INSERT INTO [dbo].[Mac_copy] VALUES ('561', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '11408');
INSERT INTO [dbo].[Mac_copy] VALUES ('562', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '12219');
INSERT INTO [dbo].[Mac_copy] VALUES ('563', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '12269');
INSERT INTO [dbo].[Mac_copy] VALUES ('564', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '12368');
INSERT INTO [dbo].[Mac_copy] VALUES ('565', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '14619');
INSERT INTO [dbo].[Mac_copy] VALUES ('566', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '14669');
INSERT INTO [dbo].[Mac_copy] VALUES ('567', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 56 mit 8 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '14768');
INSERT INTO [dbo].[Mac_copy] VALUES ('568', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '6219');
INSERT INTO [dbo].[Mac_copy] VALUES ('569', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '6269');
INSERT INTO [dbo].[Mac_copy] VALUES ('570', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '6368');
INSERT INTO [dbo].[Mac_copy] VALUES ('571', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '7179');
INSERT INTO [dbo].[Mac_copy] VALUES ('572', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '7229');
INSERT INTO [dbo].[Mac_copy] VALUES ('573', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '7328');
INSERT INTO [dbo].[Mac_copy] VALUES ('574', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '9579');
INSERT INTO [dbo].[Mac_copy] VALUES ('575', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '9629');
INSERT INTO [dbo].[Mac_copy] VALUES ('576', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '9728');
INSERT INTO [dbo].[Mac_copy] VALUES ('577', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '7179');
INSERT INTO [dbo].[Mac_copy] VALUES ('578', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '7229');
INSERT INTO [dbo].[Mac_copy] VALUES ('579', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '7328');
INSERT INTO [dbo].[Mac_copy] VALUES ('580', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '8139');
INSERT INTO [dbo].[Mac_copy] VALUES ('581', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '8189');
INSERT INTO [dbo].[Mac_copy] VALUES ('582', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '8288');
INSERT INTO [dbo].[Mac_copy] VALUES ('583', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '10539');
INSERT INTO [dbo].[Mac_copy] VALUES ('584', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '10589');
INSERT INTO [dbo].[Mac_copy] VALUES ('585', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '10688');
INSERT INTO [dbo].[Mac_copy] VALUES ('586', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '8139');
INSERT INTO [dbo].[Mac_copy] VALUES ('587', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '8189');
INSERT INTO [dbo].[Mac_copy] VALUES ('588', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '8288');
INSERT INTO [dbo].[Mac_copy] VALUES ('589', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '9099');
INSERT INTO [dbo].[Mac_copy] VALUES ('590', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '9149');
INSERT INTO [dbo].[Mac_copy] VALUES ('591', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '9248');
INSERT INTO [dbo].[Mac_copy] VALUES ('592', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '11499');
INSERT INTO [dbo].[Mac_copy] VALUES ('593', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '11549');
INSERT INTO [dbo].[Mac_copy] VALUES ('594', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '11648');
INSERT INTO [dbo].[Mac_copy] VALUES ('595', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '9099');
INSERT INTO [dbo].[Mac_copy] VALUES ('596', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '9149');
INSERT INTO [dbo].[Mac_copy] VALUES ('597', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '9248');
INSERT INTO [dbo].[Mac_copy] VALUES ('598', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '10059');
INSERT INTO [dbo].[Mac_copy] VALUES ('599', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '10109');
INSERT INTO [dbo].[Mac_copy] VALUES ('600', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '10208');
INSERT INTO [dbo].[Mac_copy] VALUES ('601', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '12459');
INSERT INTO [dbo].[Mac_copy] VALUES ('602', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '12509');
INSERT INTO [dbo].[Mac_copy] VALUES ('603', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '32 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '12608');
INSERT INTO [dbo].[Mac_copy] VALUES ('604', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '7179');
INSERT INTO [dbo].[Mac_copy] VALUES ('605', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '7229');
INSERT INTO [dbo].[Mac_copy] VALUES ('606', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '7328');
INSERT INTO [dbo].[Mac_copy] VALUES ('607', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '8139');
INSERT INTO [dbo].[Mac_copy] VALUES ('608', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '8189');
INSERT INTO [dbo].[Mac_copy] VALUES ('609', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '8288');
INSERT INTO [dbo].[Mac_copy] VALUES ('610', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '10539');
INSERT INTO [dbo].[Mac_copy] VALUES ('611', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '10589');
INSERT INTO [dbo].[Mac_copy] VALUES ('612', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '10688');
INSERT INTO [dbo].[Mac_copy] VALUES ('613', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '8139');
INSERT INTO [dbo].[Mac_copy] VALUES ('614', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '8189');
INSERT INTO [dbo].[Mac_copy] VALUES ('615', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '8288');
INSERT INTO [dbo].[Mac_copy] VALUES ('616', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '9099');
INSERT INTO [dbo].[Mac_copy] VALUES ('617', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '9149');
INSERT INTO [dbo].[Mac_copy] VALUES ('618', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '9248');
INSERT INTO [dbo].[Mac_copy] VALUES ('619', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '11499');
INSERT INTO [dbo].[Mac_copy] VALUES ('620', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '11549');
INSERT INTO [dbo].[Mac_copy] VALUES ('621', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '11648');
INSERT INTO [dbo].[Mac_copy] VALUES ('622', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '9099');
INSERT INTO [dbo].[Mac_copy] VALUES ('623', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '9149');
INSERT INTO [dbo].[Mac_copy] VALUES ('624', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '9248');
INSERT INTO [dbo].[Mac_copy] VALUES ('625', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '10059');
INSERT INTO [dbo].[Mac_copy] VALUES ('626', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '10109');
INSERT INTO [dbo].[Mac_copy] VALUES ('627', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '10208');
INSERT INTO [dbo].[Mac_copy] VALUES ('628', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '12459');
INSERT INTO [dbo].[Mac_copy] VALUES ('629', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '12509');
INSERT INTO [dbo].[Mac_copy] VALUES ('630', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '12608');
INSERT INTO [dbo].[Mac_copy] VALUES ('631', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '10059');
INSERT INTO [dbo].[Mac_copy] VALUES ('632', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '10109');
INSERT INTO [dbo].[Mac_copy] VALUES ('633', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '10208');
INSERT INTO [dbo].[Mac_copy] VALUES ('634', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '11019');
INSERT INTO [dbo].[Mac_copy] VALUES ('635', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '11069');
GO
INSERT INTO [dbo].[Mac_copy] VALUES ('636', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '11168');
INSERT INTO [dbo].[Mac_copy] VALUES ('637', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '13419');
INSERT INTO [dbo].[Mac_copy] VALUES ('638', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '13469');
INSERT INTO [dbo].[Mac_copy] VALUES ('639', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '64 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '13568');
INSERT INTO [dbo].[Mac_copy] VALUES ('640', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '9099');
INSERT INTO [dbo].[Mac_copy] VALUES ('641', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '9149');
INSERT INTO [dbo].[Mac_copy] VALUES ('642', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '9248');
INSERT INTO [dbo].[Mac_copy] VALUES ('643', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '10059');
INSERT INTO [dbo].[Mac_copy] VALUES ('644', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '10109');
INSERT INTO [dbo].[Mac_copy] VALUES ('645', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '10208');
INSERT INTO [dbo].[Mac_copy] VALUES ('646', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '12459');
INSERT INTO [dbo].[Mac_copy] VALUES ('647', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '12509');
INSERT INTO [dbo].[Mac_copy] VALUES ('648', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_1', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '12608');
INSERT INTO [dbo].[Mac_copy] VALUES ('649', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '10059');
INSERT INTO [dbo].[Mac_copy] VALUES ('650', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '10109');
INSERT INTO [dbo].[Mac_copy] VALUES ('651', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '10208');
INSERT INTO [dbo].[Mac_copy] VALUES ('652', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '11019');
INSERT INTO [dbo].[Mac_copy] VALUES ('653', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '11069');
INSERT INTO [dbo].[Mac_copy] VALUES ('654', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '11168');
INSERT INTO [dbo].[Mac_copy] VALUES ('655', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '13419');
INSERT INTO [dbo].[Mac_copy] VALUES ('656', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '13469');
INSERT INTO [dbo].[Mac_copy] VALUES ('657', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_2', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '13568');
INSERT INTO [dbo].[Mac_copy] VALUES ('658', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '11019');
INSERT INTO [dbo].[Mac_copy] VALUES ('659', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '11069');
INSERT INTO [dbo].[Mac_copy] VALUES ('660', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '11168');
INSERT INTO [dbo].[Mac_copy] VALUES ('661', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '11979');
INSERT INTO [dbo].[Mac_copy] VALUES ('662', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '12029');
INSERT INTO [dbo].[Mac_copy] VALUES ('663', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '12128');
INSERT INTO [dbo].[Mac_copy] VALUES ('664', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '14379');
INSERT INTO [dbo].[Mac_copy] VALUES ('665', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '14429');
INSERT INTO [dbo].[Mac_copy] VALUES ('666', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_3', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '14528');
INSERT INTO [dbo].[Mac_copy] VALUES ('667', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '11979');
INSERT INTO [dbo].[Mac_copy] VALUES ('668', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '12029');
INSERT INTO [dbo].[Mac_copy] VALUES ('669', 'iMac Pro', 'Space Grau', 'iMP', '1 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '12128');
INSERT INTO [dbo].[Mac_copy] VALUES ('670', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '12939');
INSERT INTO [dbo].[Mac_copy] VALUES ('671', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '12989');
INSERT INTO [dbo].[Mac_copy] VALUES ('672', 'iMac Pro', 'Space Grau', 'iMP', '2 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '13088');
INSERT INTO [dbo].[Mac_copy] VALUES ('673', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_1', 'iMP', 'wire', '15339');
INSERT INTO [dbo].[Mac_copy] VALUES ('674', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_2', 'iMP', 'wire', '15389');
INSERT INTO [dbo].[Mac_copy] VALUES ('675', 'iMac Pro', 'Space Grau', 'iMP', '4 TB', 'iMP', '9.7 KG', 'iMP_4', '128 GB', 'Radeon Pro Vega 64 mit 16 GB HBM2 Grafikspeicher', 'iMP', 'iM_3', 'iMP', 'wire', '15488');
INSERT INTO [dbo].[Mac_copy] VALUES ('676', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_1', '16 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '3399');
INSERT INTO [dbo].[Mac_copy] VALUES ('677', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_1', '16 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '3639');
INSERT INTO [dbo].[Mac_copy] VALUES ('678', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_1', '16 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '4119');
INSERT INTO [dbo].[Mac_copy] VALUES ('679', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_1', '32 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '3879');
INSERT INTO [dbo].[Mac_copy] VALUES ('680', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_1', '32 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '4119');
INSERT INTO [dbo].[Mac_copy] VALUES ('681', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_1', '32 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '4599');
INSERT INTO [dbo].[Mac_copy] VALUES ('682', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_1', '64 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '4839');
INSERT INTO [dbo].[Mac_copy] VALUES ('683', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_1', '64 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '5079');
INSERT INTO [dbo].[Mac_copy] VALUES ('684', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_1', '64 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '5559');
INSERT INTO [dbo].[Mac_copy] VALUES ('685', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_2', '16 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '4359');
INSERT INTO [dbo].[Mac_copy] VALUES ('686', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_2', '16 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '4599');
INSERT INTO [dbo].[Mac_copy] VALUES ('687', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_2', '16 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '5079');
INSERT INTO [dbo].[Mac_copy] VALUES ('688', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_2', '32 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '4839');
INSERT INTO [dbo].[Mac_copy] VALUES ('689', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_2', '32 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '5079');
INSERT INTO [dbo].[Mac_copy] VALUES ('690', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_2', '32 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '5559');
INSERT INTO [dbo].[Mac_copy] VALUES ('691', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_2', '64 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '5799');
INSERT INTO [dbo].[Mac_copy] VALUES ('692', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_2', '64 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '6039');
INSERT INTO [dbo].[Mac_copy] VALUES ('693', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_2', '64 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '6519');
INSERT INTO [dbo].[Mac_copy] VALUES ('694', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_3', '16 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '5799');
INSERT INTO [dbo].[Mac_copy] VALUES ('695', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_3', '16 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '6039');
INSERT INTO [dbo].[Mac_copy] VALUES ('696', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_3', '16 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '6519');
INSERT INTO [dbo].[Mac_copy] VALUES ('697', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_3', '32 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '6279');
INSERT INTO [dbo].[Mac_copy] VALUES ('698', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_3', '32 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '6519');
INSERT INTO [dbo].[Mac_copy] VALUES ('699', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_3', '32 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '6999');
INSERT INTO [dbo].[Mac_copy] VALUES ('700', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_3', '64 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '7239');
INSERT INTO [dbo].[Mac_copy] VALUES ('701', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_3', '64 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '7479');
INSERT INTO [dbo].[Mac_copy] VALUES ('702', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_3', '64 GB', 'Zwei AMD FirePro D500 GPUs mit je 3 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '7959');
INSERT INTO [dbo].[Mac_copy] VALUES ('703', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_1', '16 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '3639');
INSERT INTO [dbo].[Mac_copy] VALUES ('704', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_1', '16 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '3879');
INSERT INTO [dbo].[Mac_copy] VALUES ('705', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_1', '16 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '4359');
INSERT INTO [dbo].[Mac_copy] VALUES ('706', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_1', '32 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '4119');
INSERT INTO [dbo].[Mac_copy] VALUES ('707', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_1', '32 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '4359');
INSERT INTO [dbo].[Mac_copy] VALUES ('708', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_1', '32 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '4839');
INSERT INTO [dbo].[Mac_copy] VALUES ('709', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_1', '64 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '5079');
INSERT INTO [dbo].[Mac_copy] VALUES ('710', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_1', '64 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '5319');
INSERT INTO [dbo].[Mac_copy] VALUES ('711', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_1', '64 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '5799');
INSERT INTO [dbo].[Mac_copy] VALUES ('712', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_2', '16 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '4599');
INSERT INTO [dbo].[Mac_copy] VALUES ('713', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_2', '16 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '4839');
INSERT INTO [dbo].[Mac_copy] VALUES ('714', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_2', '16 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '5319');
INSERT INTO [dbo].[Mac_copy] VALUES ('715', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_2', '32 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '5079');
INSERT INTO [dbo].[Mac_copy] VALUES ('716', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_2', '32 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '5319');
INSERT INTO [dbo].[Mac_copy] VALUES ('717', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_2', '32 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '5799');
INSERT INTO [dbo].[Mac_copy] VALUES ('718', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_2', '64 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '6039');
INSERT INTO [dbo].[Mac_copy] VALUES ('719', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_2', '64 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '6279');
INSERT INTO [dbo].[Mac_copy] VALUES ('720', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_2', '64 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '6759');
INSERT INTO [dbo].[Mac_copy] VALUES ('721', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_3', '16 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '6039');
INSERT INTO [dbo].[Mac_copy] VALUES ('722', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_3', '16 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '6279');
INSERT INTO [dbo].[Mac_copy] VALUES ('723', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_3', '16 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '6759');
INSERT INTO [dbo].[Mac_copy] VALUES ('724', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_3', '32 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '6519');
INSERT INTO [dbo].[Mac_copy] VALUES ('725', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_3', '32 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '6759');
INSERT INTO [dbo].[Mac_copy] VALUES ('726', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_3', '32 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '7239');
INSERT INTO [dbo].[Mac_copy] VALUES ('727', 'Mac Pro', 'Schwarz', 'no', '256 GB', 'MP', '5 KG', 'MP_3', '64 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '7479');
INSERT INTO [dbo].[Mac_copy] VALUES ('728', 'Mac Pro', 'Schwarz', 'no', '512 GB', 'MP', '5 KG', 'MP_3', '64 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '7719');
INSERT INTO [dbo].[Mac_copy] VALUES ('729', 'Mac Pro', 'Schwarz', 'no', '1 TB', 'MP', '5 KG', 'MP_3', '64 GB', 'Zwei AMD FirePro D700 GPUs mit je 6 GB GDDR5 VRAM Speicher', 'MP', 'no', 'MP', 'wire', '8199');
INSERT INTO [dbo].[Mac_copy] VALUES ('730', 'Mac Mini', 'Space Grau', 'no', '128 GB', 'MM', '1.3 KG', 'MM_1', '8 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '899');
INSERT INTO [dbo].[Mac_copy] VALUES ('731', 'Mac Mini', 'Space Grau', 'no', '256 GB', 'MM', '1.3 KG', 'MM_1', '8 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1139');
INSERT INTO [dbo].[Mac_copy] VALUES ('732', 'Mac Mini', 'Space Grau', 'no', '512 GB', 'MM', '1.3 KG', 'MM_1', '8 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1379');
INSERT INTO [dbo].[Mac_copy] VALUES ('733', 'Mac Mini', 'Space Grau', 'no', '1 TB', 'MM', '1.3 KG', 'MM_1', '8 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1859');
INSERT INTO [dbo].[Mac_copy] VALUES ('734', 'Mac Mini', 'Space Grau', 'no', '2 TB', 'MM', '1.3 KG', 'MM_1', '8 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2819');
INSERT INTO [dbo].[Mac_copy] VALUES ('735', 'Mac Mini', 'Space Grau', 'no', '128 GB', 'MM', '1.3 KG', 'MM_1', '16 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1139');
INSERT INTO [dbo].[Mac_copy] VALUES ('736', 'Mac Mini', 'Space Grau', 'no', '256 GB', 'MM', '1.3 KG', 'MM_1', '16 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1379');
INSERT INTO [dbo].[Mac_copy] VALUES ('737', 'Mac Mini', 'Space Grau', 'no', '512 GB', 'MM', '1.3 KG', 'MM_1', '16 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1619');
INSERT INTO [dbo].[Mac_copy] VALUES ('738', 'Mac Mini', 'Space Grau', 'no', '1 TB', 'MM', '1.3 KG', 'MM_1', '16 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2099');
INSERT INTO [dbo].[Mac_copy] VALUES ('739', 'Mac Mini', 'Space Grau', 'no', '2 TB', 'MM', '1.3 KG', 'MM_1', '16 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '3059');
INSERT INTO [dbo].[Mac_copy] VALUES ('740', 'Mac Mini', 'Space Grau', 'no', '128 GB', 'MM', '1.3 KG', 'MM_1', '32 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1619');
INSERT INTO [dbo].[Mac_copy] VALUES ('741', 'Mac Mini', 'Space Grau', 'no', '256 GB', 'MM', '1.3 KG', 'MM_1', '32 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1859');
INSERT INTO [dbo].[Mac_copy] VALUES ('742', 'Mac Mini', 'Space Grau', 'no', '512 GB', 'MM', '1.3 KG', 'MM_1', '32 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2099');
INSERT INTO [dbo].[Mac_copy] VALUES ('743', 'Mac Mini', 'Space Grau', 'no', '1 TB', 'MM', '1.3 KG', 'MM_1', '32 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2579');
INSERT INTO [dbo].[Mac_copy] VALUES ('744', 'Mac Mini', 'Space Grau', 'no', '2 TB', 'MM', '1.3 KG', 'MM_1', '32 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '3539');
INSERT INTO [dbo].[Mac_copy] VALUES ('745', 'Mac Mini', 'Space Grau', 'no', '128 GB', 'MM', '1.3 KG', 'MM_1', '64 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2579');
INSERT INTO [dbo].[Mac_copy] VALUES ('746', 'Mac Mini', 'Space Grau', 'no', '256 GB', 'MM', '1.3 KG', 'MM_1', '64 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2819');
INSERT INTO [dbo].[Mac_copy] VALUES ('747', 'Mac Mini', 'Space Grau', 'no', '512 GB', 'MM', '1.3 KG', 'MM_1', '64 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '3059');
INSERT INTO [dbo].[Mac_copy] VALUES ('748', 'Mac Mini', 'Space Grau', 'no', '1 TB', 'MM', '1.3 KG', 'MM_1', '64 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '3539');
INSERT INTO [dbo].[Mac_copy] VALUES ('749', 'Mac Mini', 'Space Grau', 'no', '2 TB', 'MM', '1.3 KG', 'MM_1', '64 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '4499');
INSERT INTO [dbo].[Mac_copy] VALUES ('750', 'Mac Mini', 'Space Grau', 'no', '128 GB', 'MM', '1.3 KG', 'MM_2', '8 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1249');
INSERT INTO [dbo].[Mac_copy] VALUES ('751', 'Mac Mini', 'Space Grau', 'no', '256 GB', 'MM', '1.3 KG', 'MM_2', '8 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1489');
INSERT INTO [dbo].[Mac_copy] VALUES ('752', 'Mac Mini', 'Space Grau', 'no', '512 GB', 'MM', '1.3 KG', 'MM_2', '8 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1729');
INSERT INTO [dbo].[Mac_copy] VALUES ('753', 'Mac Mini', 'Space Grau', 'no', '1 TB', 'MM', '1.3 KG', 'MM_2', '8 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2209');
INSERT INTO [dbo].[Mac_copy] VALUES ('754', 'Mac Mini', 'Space Grau', 'no', '2 TB', 'MM', '1.3 KG', 'MM_2', '8 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '3169');
INSERT INTO [dbo].[Mac_copy] VALUES ('755', 'Mac Mini', 'Space Grau', 'no', '128 GB', 'MM', '1.3 KG', 'MM_2', '16 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1489');
INSERT INTO [dbo].[Mac_copy] VALUES ('756', 'Mac Mini', 'Space Grau', 'no', '256 GB', 'MM', '1.3 KG', 'MM_2', '16 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1729');
INSERT INTO [dbo].[Mac_copy] VALUES ('757', 'Mac Mini', 'Space Grau', 'no', '512 GB', 'MM', '1.3 KG', 'MM_2', '16 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1969');
INSERT INTO [dbo].[Mac_copy] VALUES ('758', 'Mac Mini', 'Space Grau', 'no', '1 TB', 'MM', '1.3 KG', 'MM_2', '16 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2449');
INSERT INTO [dbo].[Mac_copy] VALUES ('759', 'Mac Mini', 'Space Grau', 'no', '2 TB', 'MM', '1.3 KG', 'MM_2', '16 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '3409');
INSERT INTO [dbo].[Mac_copy] VALUES ('760', 'Mac Mini', 'Space Grau', 'no', '128 GB', 'MM', '1.3 KG', 'MM_2', '32 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1969');
INSERT INTO [dbo].[Mac_copy] VALUES ('761', 'Mac Mini', 'Space Grau', 'no', '256 GB', 'MM', '1.3 KG', 'MM_2', '32 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2209');
INSERT INTO [dbo].[Mac_copy] VALUES ('762', 'Mac Mini', 'Space Grau', 'no', '512 GB', 'MM', '1.3 KG', 'MM_2', '32 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2449');
INSERT INTO [dbo].[Mac_copy] VALUES ('763', 'Mac Mini', 'Space Grau', 'no', '1 TB', 'MM', '1.3 KG', 'MM_2', '32 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2929');
INSERT INTO [dbo].[Mac_copy] VALUES ('764', 'Mac Mini', 'Space Grau', 'no', '2 TB', 'MM', '1.3 KG', 'MM_2', '32 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '3889');
INSERT INTO [dbo].[Mac_copy] VALUES ('765', 'Mac Mini', 'Space Grau', 'no', '128 GB', 'MM', '1.3 KG', 'MM_2', '64 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2929');
INSERT INTO [dbo].[Mac_copy] VALUES ('766', 'Mac Mini', 'Space Grau', 'no', '256 GB', 'MM', '1.3 KG', 'MM_2', '64 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '3169');
INSERT INTO [dbo].[Mac_copy] VALUES ('767', 'Mac Mini', 'Space Grau', 'no', '512 GB', 'MM', '1.3 KG', 'MM_2', '64 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '3409');
INSERT INTO [dbo].[Mac_copy] VALUES ('768', 'Mac Mini', 'Space Grau', 'no', '1 TB', 'MM', '1.3 KG', 'MM_2', '64 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '3889');
INSERT INTO [dbo].[Mac_copy] VALUES ('769', 'Mac Mini', 'Space Grau', 'no', '2 TB', 'MM', '1.3 KG', 'MM_2', '64 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '4849');
INSERT INTO [dbo].[Mac_copy] VALUES ('770', 'Mac Mini', 'Space Grau', 'no', '256 GB', 'MM', '1.3 KG', 'MM_3', '8 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1249');
INSERT INTO [dbo].[Mac_copy] VALUES ('771', 'Mac Mini', 'Space Grau', 'no', '512 GB', 'MM', '1.3 KG', 'MM_3', '8 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1489');
INSERT INTO [dbo].[Mac_copy] VALUES ('772', 'Mac Mini', 'Space Grau', 'no', '1 TB', 'MM', '1.3 KG', 'MM_3', '8 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1969');
INSERT INTO [dbo].[Mac_copy] VALUES ('773', 'Mac Mini', 'Space Grau', 'no', '2 TB', 'MM', '1.3 KG', 'MM_3', '8 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2929');
INSERT INTO [dbo].[Mac_copy] VALUES ('774', 'Mac Mini', 'Space Grau', 'no', '256 GB', 'MM', '1.3 KG', 'MM_3', '16 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1489');
INSERT INTO [dbo].[Mac_copy] VALUES ('775', 'Mac Mini', 'Space Grau', 'no', '512 GB', 'MM', '1.3 KG', 'MM_3', '16 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1729');
INSERT INTO [dbo].[Mac_copy] VALUES ('776', 'Mac Mini', 'Space Grau', 'no', '1 TB', 'MM', '1.3 KG', 'MM_3', '16 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2209');
INSERT INTO [dbo].[Mac_copy] VALUES ('777', 'Mac Mini', 'Space Grau', 'no', '2 TB', 'MM', '1.3 KG', 'MM_3', '16 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '3169');
INSERT INTO [dbo].[Mac_copy] VALUES ('778', 'Mac Mini', 'Space Grau', 'no', '256 GB', 'MM', '1.3 KG', 'MM_3', '32 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '1969');
INSERT INTO [dbo].[Mac_copy] VALUES ('779', 'Mac Mini', 'Space Grau', 'no', '512 GB', 'MM', '1.3 KG', 'MM_3', '32 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2209');
INSERT INTO [dbo].[Mac_copy] VALUES ('780', 'Mac Mini', 'Space Grau', 'no', '1 TB', 'MM', '1.3 KG', 'MM_3', '32 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2689');
INSERT INTO [dbo].[Mac_copy] VALUES ('781', 'Mac Mini', 'Space Grau', 'no', '2 TB', 'MM', '1.3 KG', 'MM_3', '32 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '3649');
INSERT INTO [dbo].[Mac_copy] VALUES ('782', 'Mac Mini', 'Space Grau', 'no', '256 GB', 'MM', '1.3 KG', 'MM_3', '64 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '2929');
INSERT INTO [dbo].[Mac_copy] VALUES ('783', 'Mac Mini', 'Space Grau', 'no', '512 GB', 'MM', '1.3 KG', 'MM_3', '64 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '3169');
INSERT INTO [dbo].[Mac_copy] VALUES ('784', 'Mac Mini', 'Space Grau', 'no', '1 TB', 'MM', '1.3 KG', 'MM_3', '64 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '3649');
INSERT INTO [dbo].[Mac_copy] VALUES ('785', 'Mac Mini', 'Space Grau', 'no', '2 TB', 'MM', '1.3 KG', 'MM_3', '64 GB', 'Intel UHD Graphics 630', 'MM', 'no', 'MM', 'wire', '4609');
GO
COMMIT
GO

-- ----------------------------
--  Table structure for [dbo].[Addon]
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[Addon]') AND type IN ('U'))
	DROP TABLE [dbo].[Addon]
GO
CREATE TABLE [dbo].[Addon] (
	[ID] int NOT NULL,
	[Name] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DeviceType] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Price] int NULL,
	[shortName] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__Addon__3214EC2733B80313] PRIMARY KEY CLUSTERED ([ID]) 
	WITH (IGNORE_DUP_KEY = OFF)
)
GO

-- ----------------------------
--  Records of [dbo].[Addon]
-- ----------------------------
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[Addon] VALUES ('1', 'Final Cut Pro x', 'Mac', '330', 'FinalCut');
INSERT INTO [dbo].[Addon] VALUES ('2', 'Logic Pro x', 'Mac', '230', 'Logic');
GO
COMMIT
GO

